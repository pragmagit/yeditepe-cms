import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import Popover from '@material-ui/core/Popover';
import Typography from '@material-ui/core/Typography';
import { useAuth } from 'app/main/contexts/AuthContext';
import { useState } from 'react';
import { Link } from 'react-router-dom';

function UserMenu(props) {
	const { user } = useAuth();

	const [userMenu, setUserMenu] = useState(null);

	const userMenuClick = event => {
		setUserMenu(event.currentTarget);
	};

	const userMenuClose = () => {
		setUserMenu(null);
	};

	return (
		<>
			<Button className="min-h-40 min-w-40 px-0 md:px-16 py-0 md:py-6" onClick={userMenuClick}>
				<div className="md:flex flex-col mx-4 items-end">
					<Typography component="span" className="font-semibold flex">
						{user?.nameSurname}
					</Typography>
					<Typography className="text-11 font-medium capitalize" color="textSecondary">
						{user?.role}
					</Typography>
				</div>
			</Button>

			<Popover
				open={Boolean(userMenu)}
				anchorEl={userMenu}
				onClose={userMenuClose}
				anchorOrigin={{
					vertical: 'bottom',
					horizontal: 'center'
				}}
				transformOrigin={{
					vertical: 'top',
					horizontal: 'center'
				}}
				classes={{
					paper: 'py-8'
				}}
			>
				<>
					<MenuItem component={Link} to="/profile" onClick={userMenuClose} role="button">
						<ListItemIcon className="min-w-40">
							<Icon>account_circle</Icon>
						</ListItemIcon>
						<ListItemText primary="Profili Düzenle" />
					</MenuItem>
					<MenuItem component={Link} to="/logout" onClick={userMenuClose} role="button">
						<ListItemIcon className="min-w-40">
							<Icon>exit_to_app</Icon>
						</ListItemIcon>
						<ListItemText primary="Çıkış Yap" />
					</MenuItem>
				</>
			</Popover>
		</>
	);
}

export default UserMenu;
