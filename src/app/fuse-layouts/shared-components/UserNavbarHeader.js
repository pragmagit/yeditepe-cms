import AppBar from '@material-ui/core/AppBar';
import Avatar from '@material-ui/core/Avatar';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { useAuth } from 'app/main/contexts/AuthContext';
import clsx from 'clsx';
import { useSelector } from 'react-redux';
import Logo from './Logo';

const useStyles = makeStyles(theme => ({
	root: {
		'&.user': {
			'& .username, & .email': {
				transition: theme.transitions.create('opacity', {
					duration: theme.transitions.duration.shortest,
					easing: theme.transitions.easing.easeInOut
				})
			}
		}
	},
	avatar: {
		background: theme.palette.background.default,
		transition: theme.transitions.create('all', {
			duration: theme.transitions.duration.shortest,
			easing: theme.transitions.easing.easeInOut
		}),
		bottom: 0,
		'& > img': {
			borderRadius: '50%'
		}
	}
}));

function UserNavbarHeader(props) {
	const { user } = useAuth();

	const classes = useStyles();

	return (
		<AppBar
			position="static"
			color="primary"
			classes={{ root: classes.root }}
			className="user relative flex flex-col items-center justify-center pb-64 mb-32 z-0 shadow-0"
		>
			<div style={{ width: '75%' }}>
				<Logo />
			</div>
			<Typography className="username text-18 whitespace-nowrap font-semibold mb-4" color="inherit">
				{user?.nameSurname}
			</Typography>
			<Typography className="email text-13 opacity-50 whitespace-nowrap font-medium" color="inherit">
				{user?.email}
			</Typography>
			<Typography className="email text-13 opacity-50 whitespace-nowrap font-medium" color="inherit">
				({user?.role})
			</Typography>
		</AppBar>
	);
}

export default UserNavbarHeader;
