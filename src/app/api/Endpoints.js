export const Endpoints = {
    Auth: {
        Login: "auth/loginasadmin",
        GetUserDetailsForCMS: "auth/getuserdetailsForCMS",
        GetPersonnels: "auth/GetAllUsers",
        GetPersonnelById: "auth/getpersonnelbyid",
        RegisterPersonel: "auth/register",
        EditMember: "auth/EditMember",
        ChangeActivationOfUser: "auth/ChangeActivationOfUser",
        ForgotPassword: "auth/ForgotPassword"
    },
    Dashboard: {
        GetHomePageContent: "homePage/GetHomePageContent"
    },
    AirwayBill: {
        GetAirwaybills: "airwaybill/SendAirwaybillsToCMS",
        GetAirwaybillByID: "airwaybill/SendAirwaybillToCMSByAirwaybillId",
        GetAnswersByAirwaybilId: "airwaybill/GetAnswersByAirwaybillId",
        EditAnswersOfCompletedAirwaybill: "airwaybill/EditAnswersOfCompletedAirwaybill",
        AddNoteToCompletedShipment: "airwaybill/AddNoteToCompletedShipment"
    },
    Vehicles: {
        GetIncomingVehicles: "incomingvehicles/GetCompletedVehiclesForCMS",
        GetVehicleDetails: "incomingvehicles/GetVehicleDetails"
    },
    Questions: {
        GetQuestionBank: "questions/GetQuestionBank",
        EditQuestion: "questions/EditQuestion",
        AddQuestion: "questions/AddQuestion",
        CompleteQuestion: "questions/SetQuestionDetails",
        GetQuestionDetails: "questions/GetQuestionDetails",
        SetActivationOfQuestion: "questions/SetActivationOfQuestion",
        GetQuestionsByShipmentType: "questions/GetQuestionsByShipmentType",
        AddQuestionToShipmentTypes: "questions/AddQuestionToShipmentTypes",
        DeleteQuestionFromShipmentType: "questions/DeleteQuestionFromShipmentType",
        EditOrderIndexOfQuestion: "questions/EditOrderIndexOfQuestion"
    },
    CMS: {
        GetApprovalsForCMS: "CMS/GetApprovalsForCMS",
        SetApprovalConfirmation: "CMS/SetApprovalConfirmation",
        AcceptApprovalRequest: "cms/AcceptApprovalRequest",
        RejectApprovalRequest: "cms/RejectApprovalRequest",
        GetVehicleApprovalsForCMS: "cms/GetVehicleApprovalsForCMS",
        SetVehicleApprovalConfirmation: "cms/SetVehicleApprovalConfirmation",
        AcceptVehicleApprovalRequest: "cms/AcceptVehicleApprovalRequest",
        RejectVehicleApprovalRequest: "cms/RejectVehicleApprovalRequest"
    },
    CHECK_ROLE: {
        CheckNewQuestionRole: "questions/CheckIfAdminIsAuthorized"
    },
    ROLES: {
        GetRoles: "roles/getroles"
    },
    CLAIMS: {
        GetClaims: "claims/getclaims"
    },
    LOGS: {
        GetActivityLog: "activityLog/GetActivityLog"
    },
    MailFetch: {
        GetLatestSelectUpdateDate: "mailfetch/GetLatestSelectUpdateDate"
    },
    DailySummary: {
        GetDailySummary: "tracker/GetTransactionInfo"
    }
}