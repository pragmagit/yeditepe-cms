import axios from 'axios';
import { Endpoints } from './Endpoints';
import * as Sentry from "@sentry/react";

var httpClient = axios.create({
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
    }
});
httpClient.defaults.timeout = 120000;

httpClient.interceptors.response.use(
    (response) => response,
    (err) => {
        Sentry.captureException(error);
        return Promise.reject(error);
    });

class ApiCalls {
    constructor() {
        this.server_link = "https://yeditepe-api.pragmatech.com.tr/api/"; //production
        //this.server_link = "http://yeditepe.azurewebsites.net/api/"; //dev
        // this.authenticated_server_link = "Authenticated_Server_Link"
        // this.token = "not set";
    }

    setToken = (token) => {
        if (token) {
            httpClient = axios.create({
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + token
                }
            })
            httpClient.defaults.timeout = 120000;
            httpClient.interceptors.response.use((response) => response, (err) => {
                Sentry.captureException(err);
                return Promise.reject(err);
            });
        }
    }

    //auth
    loginWithEmailAndPassword = (email, password) => {
        return httpClient.post(this.server_link + Endpoints.Auth.Login, { email, password });
    }

    GetUserDetailsForCMS = () => {
        return httpClient.get(this.server_link + Endpoints.Auth.GetUserDetailsForCMS);
    }

    ForgotPassword = (data) => {
        return httpClient.post(this.server_link + Endpoints.Auth.ForgotPassword, data);
    }

    resetPassword = (payload) => {
        return httpClient.post(this.server_link + "auth/ResetPassword", payload)
    }


    //Personnel
    GetPersonnels = () => {
        return httpClient.get(this.server_link + Endpoints.Auth.GetPersonnels);
    }

    GetPersonnelById = (personnelId) => {
        return httpClient.post(this.server_link + Endpoints.Auth.GetPersonnelById, { personnelId });
    }

    EditMember = (data) => {
        return httpClient.post(this.server_link + Endpoints.Auth.EditMember, data);
    }

    RegisterPersonel = (data) => {
        return httpClient.post(this.server_link + Endpoints.Auth.RegisterPersonel, data);
    }

    ChangeActivationOfUser = (data) => {
        return httpClient.post(this.server_link + Endpoints.Auth.ChangeActivationOfUser, data);
    }

    //Dashboard
    GetDashboardContent = () => {
        return httpClient.post(this.server_link + Endpoints.Dashboard.GetHomePageContent);
    }

    //Airwaybills
    GetAirwaybills = () => {
        return httpClient.get(this.server_link + Endpoints.AirwayBill.GetAirwaybills);
    }

    GetAnswersByAirwaybilId = (airwaybillId) => {
        return httpClient.post(this.server_link + Endpoints.AirwayBill.GetAnswersByAirwaybilId, { airwaybillId });
    }

    EditAnswersOfCompletedAirwaybill = (data) => {
        return httpClient.post(this.server_link + Endpoints.AirwayBill.EditAnswersOfCompletedAirwaybill, data);
    }

    GetAirwaybillByID = (airwaybillId) => {
        return httpClient.post(this.server_link + Endpoints.AirwayBill.GetAirwaybillByID, { airwaybillId });
    }

    AddNoteToCompletedShipment = (data) => {
        return httpClient.post(this.server_link + Endpoints.AirwayBill.AddNoteToCompletedShipment, data);
    }

    //Vehicles
    GetIncomingVehicles = () => {
        return httpClient.post(this.server_link + Endpoints.Vehicles.GetIncomingVehicles);
    }

    GetVehicleDetails = (data) => {
        return httpClient.post(this.server_link + Endpoints.Vehicles.GetVehicleDetails, data);
    }

    //Questions
    GetQuestionBank = () => {
        return httpClient.post(this.server_link + Endpoints.Questions.GetQuestionBank);
    }

    AddQuestion = (data) => {
        return httpClient.post(this.server_link + Endpoints.Questions.AddQuestion, data);
    }

    CompleteQuestion = (data) => {
        return httpClient.post(this.server_link + Endpoints.Questions.CompleteQuestion, data);
    }

    EditQuestion = (data) => {
        return httpClient.post(this.server_link + Endpoints.Questions.EditQuestion, data);
    }

    GetQuestionDetails = (questionId) => {
        return httpClient.post(this.server_link + Endpoints.Questions.GetQuestionDetails, { questionId: parseInt(questionId) });
    }

    SetActivationOfQuestion = (data) => {
        return httpClient.post(this.server_link + Endpoints.Questions.SetActivationOfQuestion, data);
    }

    GetQuestionsByShipmentType = (data) => {
        return httpClient.post(this.server_link + Endpoints.Questions.GetQuestionsByShipmentType, data);
    }

    AddQuestionToShipmentTypes = (data) => {
        return httpClient.post(this.server_link + Endpoints.Questions.AddQuestionToShipmentTypes, data);
    }

    DeleteQuestionFromShipmentType = (data) => {
        return httpClient.post(this.server_link + Endpoints.Questions.DeleteQuestionFromShipmentType, data);
    }

    EditOrderIndexOfQuestion = (data) => {
        return httpClient.post(this.server_link + Endpoints.Questions.EditOrderIndexOfQuestion, data);
    }

    //CMS Approval
    GetApprovalsForCMS = (airwayBillNo) => {
        return httpClient.post(this.server_link + Endpoints.CMS.GetApprovalsForCMS, { airwayBillNo });
    }

    SetApprovalConfirmation = (data) => {
        return httpClient.post(this.server_link + Endpoints.CMS.SetApprovalConfirmation, data)
    }

    AcceptApprovalRequest = (airwayBillNo) => {
        return httpClient.post(this.server_link + Endpoints.CMS.AcceptApprovalRequest, { airwayBillNo });
    }

    RejectApprovalRequest = (airwayBillNo) => {
        return httpClient.post(this.server_link + Endpoints.CMS.RejectApprovalRequest, { airwayBillNo });
    }

    GetVehicleApprovalsForCMS = (airwayBillNo) => {
        return httpClient.post(this.server_link + Endpoints.CMS.GetVehicleApprovalsForCMS, { airwayBillNo });
    }

    SetVehicleApprovalConfirmation = (data) => {
        return httpClient.post(this.server_link + Endpoints.CMS.SetVehicleApprovalConfirmation, data);
    }

    AcceptVehicleApprovalRequest = (data) => {
        return httpClient.post(this.server_link + Endpoints.CMS.AcceptVehicleApprovalRequest, data);
    }

    RejectVehicleApprovalRequest = (data) => {
        return httpClient.post(this.server_link + Endpoints.CMS.RejectVehicleApprovalRequest, data);
    }

    //Role Check
    CheckNewQuestionRole = () => {
        return httpClient.get(this.server_link + Endpoints.CHECK_ROLE.CheckNewQuestionRole);
    }

    GetRoles = () => {
        return httpClient.get(this.server_link + Endpoints.ROLES.GetRoles);
    }

    GetClaims = () => {
        return httpClient.get(this.server_link + Endpoints.CLAIMS.GetClaims);
    }

    //Log
    GetActivityLog = () => {
        return httpClient.get(this.server_link + Endpoints.LOGS.GetActivityLog);
    }

    //MailFetch
    GetLatestSelectUpdateDate = () => {
        return httpClient.get(this.server_link + Endpoints.MailFetch.GetLatestSelectUpdateDate);
    }

    //Daily
    GetDailySummary = () => {
        return httpClient.get(this.server_link + Endpoints.DailySummary.GetDailySummary);
    }
}


export default new ApiCalls();
