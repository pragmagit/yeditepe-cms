import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function SnackbarMessage({ snackbarText, snackbarOpen, snackbarClose, snackbarSuccess }) {
    return (
        <Snackbar open={snackbarOpen} autoHideDuration={6000} onClose={snackbarClose}>
            <Alert onClose={snackbarClose} severity={snackbarSuccess}>
                {snackbarText}
            </Alert>
        </Snackbar>
    )
}