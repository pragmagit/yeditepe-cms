import React from 'react';
import { motion } from 'framer-motion';
import { ThemeProvider } from '@material-ui/core/styles';
import Icon from '@material-ui/core/Icon';
import Input from '@material-ui/core/Input';
import Paper from '@material-ui/core/Paper';
import { selectMainTheme } from 'app/store/fuse/settingsSlice';
import { useSelector } from 'react-redux';

export default function SearchBox({ placeholder, changeSearchText }) {
    const mainTheme = useSelector(selectMainTheme);

    const handleSearchTextChange = (event) => {
        changeSearchText(event.target.value);
    }

    return (
        <div className="flex flex-1 items-center justify-center px-12">
            <ThemeProvider theme={mainTheme}>
                <Paper
                    component={motion.div}
                    initial={{ y: -20, opacity: 0 }}
                    animate={{ y: 0, opacity: 1, transition: { delay: 0.2 } }}
                    className="flex items-center w-full max-w-512 px-8 py-4 rounded-16 shadow"
                >
                    <Icon color="action">search</Icon>

                    <Input
                        placeholder={placeholder}
                        className="flex flex-1 mx-8"
                        disableUnderline
                        fullWidth
                        onChange={handleSearchTextChange}
                        inputProps={{
                            'aria-label': 'Arama'
                        }}
                    />
                </Paper>
            </ThemeProvider>
        </div>
    )
}
