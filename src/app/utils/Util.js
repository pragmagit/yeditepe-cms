module.exports.formatPhoneNumber = (value, previousValue) => {
    if (!value) return value;
    const currentValue = value.replace(/[^\d]/g, '');
    const cvLength = currentValue.length;

    if (!previousValue || value.length > previousValue.length) {
        if (cvLength < 5) return currentValue;
        if (cvLength < 8) return `${currentValue.slice(0, 4)} ${currentValue.slice(4)}`;
        return `${currentValue.slice(0, 4)} ${currentValue.slice(4, 7)} ${currentValue.slice(7, 9)} ${currentValue.slice(9, 11)}`;
    }
};