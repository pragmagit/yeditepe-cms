import { createContext, useContext, useEffect, useState } from 'react';

const AppContext = createContext({});

export default AppContext;
