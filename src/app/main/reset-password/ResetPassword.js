import React, { useState } from 'react';
import { Button, TextField, Typography, Snackbar, FormControl, InputLabel, Input, InputAdornment, IconButton, makeStyles } from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import clsx from "clsx";

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
import APICall from 'app/api/APICall';
import { useParams } from 'react-router';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}


const requestStatus = {
    waiting: "warning",
    failure: "error",
    success: "success",
}

const useStyles = makeStyles((theme) => ({
    root: {
        display: "flex",
        flexWrap: "wrap"
    },
    margin: {
        margin: theme.spacing(1)
    },
    withoutLabel: {
        marginTop: theme.spacing(3)
    },
    textField: {
        width: "25ch"
    }
}));

function ResetPassword() {
    const classes = useStyles();

    const { id } = useParams();
    const [newPassword, setpassword] = useState("")
    const [newPasswordAgain, setnewpassword] = useState("")
    const [errorText, seterrorText] = useState("")
    const guid = id
    const [success, setsuccess] = useState(false)
    const [status, setStatus] = useState(requestStatus.waiting)
    const [modalVisible, setmodalVisible] = useState(false)
    const [passwordVisible, setpasswordVisible] = useState(false)
    const [passwordConfirmationVisible, setpasswordConfirmationVisible] = useState(false)

    const resetPassword = () => {
        if (newPassword.length < 7) {
            seterrorText("Şifre en az 8 karakter olmalıdır")
            setmodalVisible(true)
            return
        }
        if (newPassword !== newPasswordAgain) {
            seterrorText("Şifreler uyuşmuyor")
            setmodalVisible(true)
            return
        }
        APICall.resetPassword({ newPassword, newPasswordAgain, guid }).then((succ) => {
            setStatus(requestStatus.success)
            seterrorText("Başarılı. Şifreniz değiştirildi")
            setsuccess(true)
            setmodalVisible(true)
        }).catch((err) => {
            setStatus(requestStatus.failure)
            seterrorText("Hata, Bağlantı geçersiz")
            setmodalVisible(true)

        })
    }
    const handleClickShowPassword = () => {
        setpasswordVisible(!passwordVisible)
    };
    const handleConfirmationClickShowPassword = () => {
        setpasswordConfirmationVisible(!passwordConfirmationVisible)
    };


    const handlePasswordChange = (event) => {
        setpassword(event.target.value);
    }

    const handlePasswordConfirmationChange = (event) => {
        setnewpassword(event.target.value);
    }
    const handleClose = () => {
        setmodalVisible(false)
    }
    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };
    if (success) {
        return (
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
                <header className="App-header">
                    <img src='assets/images/logos/yeditepeLogo.png' />
                    <Typography style={{ marginTop: 20, color: "#3498db" }} variant="h5" component="h2">
                        Şifreniz başarıyla sıfırlandı. Yeni şifreniz ile oturum açabilirsiniz.
                    </Typography>
                    <Snackbar open={modalVisible} autoHideDuration={6000} onClose={handleClose}>
                        <Alert onClose={handleClose} severity={status}>
                            {errorText}
                        </Alert>
                    </Snackbar>
                </header>
            </div>
        )
    }
    return (
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
            <header style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                <img src='assets/images/logos/yeditepeLogo.png' />
                <Typography style={{ marginTop: 50, marginBottom: 20, color: "#222" }} variant="h5" component="h2">
                    Şifre Sıfırlama
                </Typography>

                <FormControl style={{ marginTop: 20 }} >
                    <InputLabel htmlFor="standard-adornment-password">
                        Şifre
                    </InputLabel>
                    <Input
                        id="standard-adornment-password"
                        type={passwordVisible ? "text" : "password"}
                        value={newPassword}
                        onChange={handlePasswordChange}
                        endAdornment={
                            <InputAdornment position="end">
                                <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={handleClickShowPassword}
                                    onMouseDown={handleMouseDownPassword}
                                >
                                    {passwordVisible ? <Visibility /> : <VisibilityOff />}
                                </IconButton>
                            </InputAdornment>
                        }
                    />
                </FormControl>

                <FormControl style={{ marginTop: 30, marginBottom: 30 }} >
                    <InputLabel htmlFor="standard-adornment-password">
                        Şifre tekrar
                    </InputLabel>
                    <Input
                        id="standard-adornment-password"
                        type={passwordConfirmationVisible ? "text" : "password"}
                        value={newPasswordAgain}
                        onChange={handlePasswordConfirmationChange}
                        endAdornment={
                            <InputAdornment position="end">
                                <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={handleConfirmationClickShowPassword}
                                    onMouseDown={handleMouseDownPassword}
                                >
                                    {passwordConfirmationVisible ? <Visibility /> : <VisibilityOff />}
                                </IconButton>
                            </InputAdornment>
                        }
                    />
                </FormControl>
                <Button onClick={resetPassword} style={{ backgroundColor: "#e74c3c", color: 'white', borderRadius: '5%' }} variant="contained" color="inherit">
                    Sıfırla
                </Button>

                <Snackbar open={modalVisible} autoHideDuration={6000} onClose={handleClose}>
                    <Alert onClose={handleClose} severity={status}>
                        {errorText}
                    </Alert>
                </Snackbar>
            </header>
        </div>
    );
}

export default ResetPassword;