import { lazy } from 'react';
import ResetPassword from './ResetPassword';

const ResetPasswordConfig = {
    settings: {
        layout: {
            config: {
                navbar: {
                    display: false
                },
                toolbar: {
                    display: false
                },
                footer: {
                    display: false
                },
                leftSidePanel: {
                    display: false
                },
                rightSidePanel: {
                    display: false
                }
            }
        }
    },
    routes: [
        {
            path: '/reset-password/:id',
            component: ResetPassword
        }
    ]
};

export default ResetPasswordConfig;
