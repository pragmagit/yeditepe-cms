import { lazy } from 'react';
import DashboardMain from './DashboardMain';

const DashboardConfig = {
    settings: {
        layout: {
            config: {
                navbar: {
                    display: true
                },
                toolbar: {
                    display: true
                },
                footer: {
                    display: false
                },
                leftSidePanel: {
                    display: false
                },
                rightSidePanel: {
                    display: false
                }
            }
        }
    },
    routes: [
        {
            path: '/dashboard',
            component: DashboardMain
        }
    ]
};

export default DashboardConfig;
