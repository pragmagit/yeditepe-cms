import React, { useEffect, useState } from 'react';
import FusePageCarded from '@fuse/core/FusePageCarded';
import FuseLoading from '@fuse/core/FuseLoading';
import { useAuth } from 'app/main/contexts/AuthContext';
import { Redirect } from 'react-router';
import DashboardHeader from './DashboardHeader';
import DashboardContent from './DashboardContent';

export default function DashboardMain() {
    const { isLoggedIn, remindProcess } = useAuth();

    if (remindProcess) {
        return <FuseLoading />
    }

    if (!isLoggedIn) {
        return <Redirect to="/" />
    }

    return (
        <div className="flex-grow overflow-x-auto">
            <FusePageCarded
                classes={{
                    content: 'flex',
                    contentCard: 'overflow-hidden',
                    header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
                }}
                header={<DashboardHeader title="Ana Sayfa" />}
                content={<DashboardContent />}
                innerScroll
            />
        </div>
    )
}
