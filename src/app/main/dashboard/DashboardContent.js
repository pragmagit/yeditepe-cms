import React, { useEffect, useState } from 'react';
import PieChart, {
    Legend,
    Series,
    Tooltip,
    Format,
    Label,
    Connector,
    Export
} from 'devextreme-react/pie-chart';
import { useQuery } from 'react-query';
import APICall from 'app/api/APICall';
import FuseLoading from '@fuse/core/FuseLoading';
import { AppBar, Button, Card, List, ListItem, Toolbar, Typography, CardContent, ListItemText } from '@material-ui/core';
import { motion } from 'framer-motion';
import { useHistory } from 'react-router';

const LOG_LIMIT = 3;

export default function DashboardContent() {
    const history = useHistory();
    const { data: dashboardData, isLoading: dashboardLoading, error: dashboardError } = useQuery('Dashboard', () => APICall.GetDashboardContent(), { cacheTime: 10000, refetchOnWindowFocus: false, retry: false });
    const { data: logData, isLoading: logLoading, error: logError } = useQuery('Log Summary', () => APICall.GetActivityLog(), { cacheTime: 10000, refetchOnWindowFocus: false, retry: false });
    const { data: dailyData, isLoading: dailLoading, error: dailyError } = useQuery('Get Daily Summary', () => APICall.GetDailySummary(), { cacheTime: 10000, refetchOnWindowFocus: false, retry: false, refetchInterval: 120000 })
    const [shipmentData, setShipmentData] = useState([]);
    const [vehicleData, setVehicleData] = useState([]);
    const [logList, setLogList] = useState([]);

    useEffect(() => {
        if (dashboardData) {
            try {
                const { pendingShipmentCount, completedShipmentCount, completedVehicleCount, vehicleAwaitingConfirmationCount, shipmentAwaitingConfirmationCount } = dashboardData?.data?.data;
                setShipmentData([{ shipmentType: 'Bekleyen Sevkiyatlar', val: pendingShipmentCount }, { shipmentType: 'Merkez Onayındaki Sevkiyatlar', val: shipmentAwaitingConfirmationCount }, { shipmentType: 'Tamamlanan Sevkiyatlar', val: completedShipmentCount }]);
                setVehicleData([{ vehicleType: 'Merkez Onayındaki Araçlar', val: vehicleAwaitingConfirmationCount }, { vehicleType: 'Tamamlanan Araçlar', val: completedVehicleCount }])
            }
            catch (err) {
                //Error handling
            }
        }
    }, [dashboardData])

    useEffect(() => {
        if (logData) {
            setLogList(logData?.data?.data?.reverse().slice(0, LOG_LIMIT));
        }
    }, [logData])


    if (dashboardLoading) {
        return <FuseLoading />
    }

    const customizeTooltip = (arg) => {
        return {
            text: `${arg.valueText} - ${(arg.percent * 100).toFixed(2)}%`
        };
    }

    const item = {
        hidden: { opacity: 0, y: 40 },
        show: { opacity: 1, y: 0 }
    };

    return (
        <div className="w-full p-20" style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row', flexWrap: 'wrap' }}>
            <Card style={{ width: 'fit-content', minWidth: '45%', height: 'fit-content' }} className="rounded-20 mb-16" elevation={5}>
                <div className="p-20 pb-0">
                    <PieChart
                        id="pie1"
                        type="donut"
                        title="Sevkiyatlar"
                        palette="Soft Pastel"
                        dataSource={shipmentData}
                    >
                        <Series argumentField="shipmentType">
                            <Label visible={true}>
                                <Connector visible={true} />
                            </Label>
                        </Series>
                        <Export enabled={true} />
                        <Legend
                            margin={0}
                            horizontalAlignment="right"
                            verticalAlignment="top"
                        />
                        <Tooltip enabled={true} customizeTooltip={customizeTooltip} />
                    </PieChart>
                </div>
            </Card>
            <Card style={{ width: 'fit-content', minWidth: '45%', height: 'fit-content' }} className="rounded-20 mb-16" elevation={5}>
                <div className="p-20 pb-0">
                    <PieChart
                        id="pie2"
                        type="donut"
                        title="Araçlar"
                        palette="Soft Pastel"
                        dataSource={vehicleData}
                    >
                        <Series argumentField="vehicleType">
                            <Label visible={true}>
                                <Connector visible={true} />
                            </Label>
                        </Series>
                        <Export enabled={true} />
                        <Legend
                            margin={0}
                            horizontalAlignment="right"
                            verticalAlignment="top"
                        />
                        <Tooltip enabled={true} customizeTooltip={customizeTooltip} />
                    </PieChart>
                </div>
            </Card>
            <Card component={motion.div} variants={item} className="w-full rounded-20 mb-16" elevation={5}>
                <AppBar position="static" elevation={0} color="inherit">
                    <Toolbar className="px-8">
                        <Typography variant="subtitle1" color="inherit" className="flex-1 px-12 font-medium">
                            Son {LOG_LIMIT} Aktivite
                        </Typography>
                        <Button color="inherit" size="small" className="font-medium" onClick={() => { history.push('/pages/logs') }}>
                            Hepsini Gör
                        </Button>
                    </Toolbar>
                </AppBar>
                <CardContent className="p-0">
                    <List>
                        {logList.length > 0 ? logList.map(log => (
                            <ListItem key={log.id} className="px-12">
                                <ListItemText
                                    className="flex-1 mx-4"
                                    primary={
                                        <div className="flex">
                                            <Typography
                                                className="font-normal whitespace-nowrap"
                                                color="primary"
                                                paragraph={false}
                                            >
                                                {log.description}
                                            </Typography>
                                        </div>
                                    }
                                    secondary={new Date(log.createdOn).toLocaleString()}
                                />
                            </ListItem>
                        )) :
                            <ListItem>
                                <ListItemText
                                    className="flex-1 mx-4"
                                    primary={
                                        <div className="flex">
                                            <Typography
                                                className="font-normal whitespace-nowrap"
                                                color="primary"
                                                paragraph={false}
                                            >
                                                {logLoading ? 'Yükleniyor...' : 'Log Bulunamadı.'}
                                            </Typography>
                                        </div>
                                    }
                                />
                            </ListItem>
                        }
                    </List>
                </CardContent>
            </Card>
            <Card component={motion.div} variants={item} className="w-full rounded-20 mb-16" elevation={5}>
                <AppBar position="static" elevation={0} color="inherit">
                    <Toolbar className="px-8">
                        <Typography variant="subtitle1" color="inherit" className="flex-1 px-12 font-medium">
                            Günlük Özet
                        </Typography>
                    </Toolbar>
                </AppBar>
                <CardContent className="p-0">
                    <List>
                        <ListItem key={1} className="px-12">
                            <ListItemText
                                className="flex-1 mx-4"
                                primary={
                                    <div className="flex">
                                        <Typography
                                            className="font-normal whitespace-nowrap"
                                            color="primary"
                                            paragraph={false}
                                        >
                                            Bugün Tamamlanan Sevkiyat Sayısı
                                        </Typography>
                                    </div>
                                }
                                secondary={dailLoading ? "Yükleniyor..." : dailyData?.data?.data?.completedShipmentsCount}
                            />
                        </ListItem>
                        <ListItem key={2} className="px-12">
                            <ListItemText
                                className="flex-1 mx-4"
                                primary={
                                    <div className="flex">
                                        <Typography
                                            className="font-normal whitespace-nowrap"
                                            color="primary"
                                            paragraph={false}
                                        >
                                            Bugün Tamamlanan Araç Sayısı
                                        </Typography>
                                    </div>
                                }
                                secondary={dailLoading ? "Yükleniyor..." : dailyData?.data?.data?.completedVehiclesCount}
                            />
                        </ListItem>
                        <ListItem key={3} className="px-12">
                            <ListItemText
                                className="flex-1 mx-4"
                                primary={
                                    <div className="flex">
                                        <Typography
                                            className="font-normal whitespace-nowrap"
                                            color="primary"
                                            paragraph={false}
                                        >
                                            Bugün Giriş Yapan Kullanıcı Sayısı
                                        </Typography>
                                    </div>
                                }
                                secondary={dailLoading ? "Yükleniyor..." : dailyData?.data?.data?.loggedInMembersCount}
                            />
                        </ListItem>
                        <ListItem key={4} className="px-12">
                            <ListItemText
                                className="flex-1 mx-4"
                                primary={
                                    <div className="flex">
                                        <Typography
                                            className="font-normal whitespace-nowrap"
                                            color="primary"
                                            paragraph={false}
                                        >
                                            Selectten Son Gelen Verinin Zamanı
                                        </Typography>
                                    </div>
                                }
                                secondary={dailLoading ? "Yükleniyor..." : new Date(dailyData?.data?.data?.selectInfoSentDate).toLocaleString()}
                            />
                        </ListItem>
                    </List>
                </CardContent>
            </Card>
        </div>
    )
}
