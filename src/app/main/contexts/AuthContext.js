import { createContext, useContext, useEffect, useState } from 'react';
import localforage from 'localforage';
import APICall from 'app/api/APICall';

const AuthContext = createContext({});

export const AuthProvider = ({ children }) => {
    const [user, setUser] = useState(null);
    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const [remindProcess, setRemindProcess] = useState(true);
    const [loginSuccess, setLoginSuccess] = useState(false);

    const login = async (email, password) => {
        try {
            const res = await APICall.loginWithEmailAndPassword(email, password);
            APICall.setToken(res?.data?.token);
            await localforage.setItem('auth_token', res?.data);
            setLoginSuccess(true);
            return { success: "Success" };
        }
        catch (err) {
            return { errorMessage: err };
        }
    }

    const logout = async () => {
        try {
            await localforage.setItem('auth_token', null);
            setIsLoggedIn(false);
            setRemindProcess(false);
            setLoginSuccess(false);
            setUser(null);
            return { success: "Success" }
        }
        catch (err) {
            return { errorMessage: err }
        }
    }

    const remindToken = async () => {
        const token = await localforage.getItem('auth_token');
        if (!token) {
            setLoginSuccess(false);
            setIsLoggedIn(false);
            setRemindProcess(false);
            setUser(null);
            return;
        }

        //Check the token is expired or not
        const currentDate = new Date();
        const tokenExpireTime = new Date(token.expiration);
        const isTokenExpired = currentDate >= tokenExpireTime;

        if (isTokenExpired) {
            await localforage.setItem('auth_token', null);
            setLoginSuccess(false);
            setIsLoggedIn(false);
            setRemindProcess(false);
            setUser(null);
            return;
        }

        await APICall.setToken(token?.token);
        setLoginSuccess(true);
    }

    const getUserDetails = async () => {
        try {
            const res = await APICall.GetUserDetailsForCMS();
            setUser(res.data.data);
            setIsLoggedIn(true);
            setRemindProcess(false);
        }
        catch (err) {
            setUser(null);
            setIsLoggedIn(false);
            setLoginSuccess(false);
            setRemindProcess(false);
        }
    }

    useEffect(() => {
        remindToken();
    }, [])

    useEffect(() => {
        if (loginSuccess) {
            getUserDetails();
        }
    }, [loginSuccess])

    return (
        <AuthContext.Provider value={{ isLoggedIn, remindProcess, user, login, logout }}>
            {children}
        </AuthContext.Provider>
    );
}

export default AuthContext;

export const useAuth = () => useContext(AuthContext);