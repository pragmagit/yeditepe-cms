import { createContext, useContext, useState } from 'react';

const WindowContext = createContext({});

export const WindowProvider = ({ children }) => {
    const [windowWidth, setWindowWidth] = useState(window?.innerWidth);

    const resizeWindowWidth = () => {
        setWindowWidth(window?.innerWidth);
    }

    window.addEventListener('resize', resizeWindowWidth);

    return (
        <WindowContext.Provider value={{ windowWidth }}>
            {children}
        </WindowContext.Provider>
    );
}

export default WindowContext;

export const useWindow = () => useContext(WindowContext);