import { createContext, useContext, useState } from 'react';

const NotificationContext = createContext({});

export const NotificationProvider = ({ children }) => {
    return (
        <NotificationContext.Provider value={{}}>
            {children}
        </NotificationContext.Provider>
    );
}

export default NotificationContext;

export const useNotification = () => useContext(NotificationContext);