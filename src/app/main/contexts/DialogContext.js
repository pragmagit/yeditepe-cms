import { createContext, useContext, useEffect, useState } from 'react';
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button } from '@material-ui/core';
import DoneOutlineIcon from '@material-ui/icons/DoneOutline';
import ClearIcon from '@material-ui/icons/Clear';

const DialogContext = createContext({});

export const DialogProvider = ({ children }) => {
    const [dialogOpen, setDialogOpen] = useState(false);
    const [dialogText, setDialogText] = useState('');
    const [dialogSuccess, setDialogSuccess] = useState(true);

    const showSuccessDialog = (text) => {
        setDialogText(text);
        setDialogSuccess(true);
        setDialogOpen(true);
    }

    const showFailureDialog = (text) => {
        setDialogText(text);
        setDialogSuccess(false);
        setDialogOpen(true);
    }

    const closeMessageDialog = () => {
        setDialogOpen(false);
    }

    return (
        <DialogContext.Provider value={{ showSuccessDialog, showFailureDialog }}>
            {children}
            <Dialog
                open={dialogOpen}
                onClose={closeMessageDialog}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title"><div style={{ display: 'flex', width: '100%', justifyContent: 'center' }}><img alt="dialog-content" style={{ width: 100, height: 100, objectFit: 'contain' }} src={dialogSuccess ? 'assets/icons/Approved.png' : 'assets/icons/Failure.png'} /></div></DialogTitle>
                <DialogContent style={{ color: dialogSuccess ? "green" : "red" }}>
                    {dialogText}
                </DialogContent>
                <DialogActions>
                    <Button onClick={closeMessageDialog} color="primary" autoFocus>
                        Kapat
                    </Button>
                </DialogActions>
            </Dialog>
        </DialogContext.Provider>
    );
}

export default DialogContext;

export const useDialog = () => useContext(DialogContext);