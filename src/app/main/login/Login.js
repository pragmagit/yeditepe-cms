import FuseLoading from '@fuse/core/FuseLoading';
import { yupResolver } from '@hookform/resolvers/yup';
import _ from '@lodash';
import { CircularProgress } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Snackbar from '@material-ui/core/Snackbar';
import { makeStyles } from '@material-ui/core/styles';
import { darken } from '@material-ui/core/styles/colorManipulator';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import MuiAlert from '@material-ui/lab/Alert';
import clsx from 'clsx';
import { motion } from 'framer-motion';
import { useEffect, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { Redirect } from 'react-router';
import * as yup from 'yup';
import { useAuth } from '../contexts/AuthContext';

//import ReCAPTCHA from 'react-google-recaptcha';
//import { useDialog } from '../contexts/DialogContext';

const useStyles = makeStyles(theme => ({
    root: {},
    leftSection: {},
    rightSection: {
        background: `linear-gradient(to right, ${theme.palette.primary.dark} 0%, ${darken(
            theme.palette.primary.dark,
            0.5
        )} 100%)`,
        color: theme.palette.primary.contrastText
    }
}));

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
    email: yup.string().email('Lütfen geçerli bir mail adresi giriniz.').required('Email boş bırakılamaz.'),
    password: yup
        .string()
        .required('Parola boş bırakılamaz.')
        .min(6, 'Parola en az 6 karakter olmalıdır.')
});

const defaultValues = {
    email: '',
    password: ''
};

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function ErrorSnackbar({ snackbarOpen, snackbarClose, title }) {
    return (
        <Snackbar open={snackbarOpen} autoHideDuration={6000} onClose={snackbarClose}>
            <Alert onClose={snackbarClose} severity="error">
                {title}
            </Alert>
        </Snackbar>
    )
}

function Login() {
    const classes = useStyles();
    const { isLoggedIn, remindProcess, login } = useAuth();
    const [loginSuccess, setLoginSuccess] = useState({});
    const [openSnackbar, setOpenSnackbar] = useState(false);
    const [snackbarText, setSnackbarText] = useState("");
    const [loginProgress, setLoginProgess] = useState(false);
    const [recaptcha, setRecaptcha] = useState(true);   //Will change after recaptcha

    const { control, formState, handleSubmit, reset } = useForm({
        mode: 'onChange',
        defaultValues,
        resolver: yupResolver(schema)
    });

    const { isValid, dirtyFields, errors } = formState;

    async function onSubmit(data) {
        const { email, password } = data;
        try {
            if (recaptcha) {
                setLoginProgess(true);
                await login(email, password).then(setLoginSuccess);
                setLoginProgess(false);
            }
            else {
                handleOpenSnackbar('Recaptcha Zorunludur');
            }
        }
        catch (err) {
            //Handle error
        }
    }

    function handleOpenSnackbar(text) {
        setSnackbarText(text);
        setOpenSnackbar(true);
    }

    function handleCloseSnackbar() {
        setOpenSnackbar(false);
    }

    useEffect(() => {
        if (loginSuccess?.errorMessage?.response?.data?.errorDescription) {
            handleOpenSnackbar(loginSuccess?.errorMessage?.response?.data?.errorDescription);
        }
    }, [loginSuccess])

    if (remindProcess) {
        return <FuseLoading />
    }

    if (isLoggedIn) {
        return <Redirect to="/dashboard" />
    }

    return (
        <div
            className={clsx(
                classes.root,
                'flex flex-col flex-auto items-center justify-center flex-shrink-0 p-16 md:p-24'
            )}
        >
            <motion.div
                initial={{ opacity: 0, scale: 0.6 }}
                animate={{ opacity: 1, scale: 1 }}
                className="flex w-full max-w-400 md:max-w-3xl rounded-20 shadow-2xl overflow-hidden"
            >
                <Card
                    className={clsx(
                        classes.leftSection,
                        'flex flex-col w-full max-w-sm items-center justify-center shadow-0'
                    )}
                    square
                >
                    <CardContent className="flex flex-col items-center justify-center w-full py-96 max-w-320">
                        <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1, transition: { delay: 0.2 } }}>
                            <div className="flex items-center mb-48 flex-column flex-wrap">
                                <img className="logo-icon w-100" src="assets/images/logos/yeditepeLogo.png" alt="logo" />
                                <Typography className="text-24 font-semibold logo-text" color="inherit">
                                    ADMIN PANEL
                                </Typography>
                            </div>
                        </motion.div>

                        <form
                            name="loginForm"
                            noValidate
                            className="flex flex-col justify-center w-full"
                            onSubmit={handleSubmit(onSubmit)}
                        >
                            <Controller
                                name="email"
                                control={control}
                                render={({ field }) => (
                                    <TextField
                                        {...field}
                                        className="mb-16"
                                        label="Email"
                                        type="email"
                                        error={!!errors.email}
                                        helperText={errors?.email?.message}
                                        variant="outlined"
                                        required
                                        fullWidth
                                    />
                                )}
                            />

                            <Controller
                                name="password"
                                control={control}
                                render={({ field }) => (
                                    <TextField
                                        {...field}
                                        className="mb-16"
                                        label="Parola."
                                        type="password"
                                        error={!!errors.password}
                                        helperText={errors?.password?.message}
                                        variant="outlined"
                                        required
                                        fullWidth
                                    />
                                )}
                            />

                            <div className="flex flex-col sm:flex-row items-center justify-center sm:justify-between" />

                            <Button
                                variant="contained"
                                color="primary"
                                className="w-full mx-auto mt-16"
                                aria-label="LOG IN"
                                disabled={_.isEmpty(dirtyFields) || !isValid || loginProgress}
                                type="submit"
                            >
                                {loginProgress ? <CircularProgress size={20} /> : null}
                                Giriş Yap.
                            </Button>
                        </form>
                    </CardContent>

                    <div className="flex flex-col items-center justify-center pb-32" />
                </Card>

                <div className={clsx(classes.rightSection, 'hidden md:flex flex-1 items-center justify-center p-64')}>
                    <div className="max-w-320">
                        <motion.div
                            initial={{ opacity: 0, y: 40 }}
                            animate={{ opacity: 1, y: 0, transition: { delay: 0.2 } }}
                        >
                            <Typography color="inherit" className="text-32 sm:text-36 font-semibold leading-tight">
                                YEDITEPE CARGO ADMIN PANEL <br />
                            </Typography>
                        </motion.div>

                        <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1, transition: { delay: 0.3 } }}>
                            <Typography variant="subtitle1" color="inherit" className="mt-32 font-medium">
                                Devam etmek için giriş yapınız.
                            </Typography>
                        </motion.div>
                    </div>
                </div>
            </motion.div>
            <ErrorSnackbar title={snackbarText} snackbarOpen={openSnackbar} snackbarClose={handleCloseSnackbar} />
        </div>
    );
}

export default Login;

/*

    <ReCAPTCHA
        sitekey="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"
        onChange={setRecaptcha}
    />
*/