import FuseLoading from '@fuse/core/FuseLoading';
import React from 'react'
import { Redirect } from 'react-router';
import { useAuth } from '../contexts/AuthContext'

export default function Logout() {
    const { isLoggedIn, remindProcess, logout } = useAuth();
    logout();

    if (remindProcess) {
        return null;
    }

    if (isLoggedIn) {
        return <Redirect to="/login" />;
    }

    return (null)
}
