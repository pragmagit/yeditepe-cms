import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import APICall from 'app/api/APICall';
import { useDialog } from 'app/main/contexts/DialogContext';
import { useHistory } from 'react-router';
import { CircularProgress } from '@material-ui/core';

export default function QuestionActiveConfirm({ open, handleClose, data, refetch }) {
    const [completeButtonDisable, setCompleteButtonDisable] = useState(false);
    const { showSuccessDialog, showFailureDialog } = useDialog();
    const history = useHistory();

    const approve = async () => {
        setCompleteButtonDisable(true);
        const requestData = {
            questionId: data?.question?.id,
            isActive: !data?.question?.isActive
        }

        try {
            APICall.SetActivationOfQuestion(requestData).then(() => {
                showSuccessDialog('Soru statüsü başarıyla değiştirildi.');
                refetch();
            }).catch(err => {
                showSuccessDialog('Soru statüsü değiştirirken bir hata meydana geldi.');
            });
            setCompleteButtonDisable(false);
            showSuccessDialog('Soru statüsü değiştirme isteğiniz iletildi.');
            handleClose();
        }
        catch (err) {
            setCompleteButtonDisable(false);
            showFailureDialog(err?.response?.data?.errorDescription);
        }
    }

    return (
        <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">{"Soru Aktifliği"}</DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    {data?.question?.questionText} sorusunun aktifliğini {data?.question?.isActive ? "kapatmak" : "açmak"} üzeresiniz. Devam etmek istiyor musunuz?
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} style={{ backgroundColor: 'red', color: 'white' }} disabled={completeButtonDisable}>
                    Hayır
                </Button>
                <Button onClick={approve} style={{ backgroundColor: 'green', color: 'white' }} disabled={completeButtonDisable}>
                    {completeButtonDisable ? <div><CircularProgress size={20} /> Lütfen Bekleyiniz...</div> : "Evet"}
                </Button>
            </DialogActions>

        </Dialog>
    )
}
