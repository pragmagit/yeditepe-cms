import React, { useEffect, useState } from 'react';
import FusePageCarded from '@fuse/core/FusePageCarded';
import QuestionHeader from './QuestionHeader';
import { useQuery } from 'react-query';
import APICall from 'app/api/APICall';
import FuseLoading from '@fuse/core/FuseLoading';
import QuestionTable from './QuestionTable';
import QuestionActiveConfirm from './QuestionActiveConfirm';

export default function QuestionList() {
    const { isLoading, error, data, refetch } = useQuery('QuestionList', () => APICall.GetQuestionBank(), { cacheTime: 10000, refetchOnWindowFocus: false, retry: false });
    const [questionList, setQuestionList] = useState([]);
    const [confirmDialogOpen, setConfirmDialogOpen] = useState(false);
    const [confirmDialogData, setConfirmDialogData] = useState(null);

    const handleOpenConfirmDialog = (data) => {
        setConfirmDialogOpen(true);
        setConfirmDialogData(data);
    }

    const handleCloseConfirmDialog = () => {
        setConfirmDialogOpen(false);
        setConfirmDialogData(null);
    }

    useEffect(() => {
        if (data) {
            setQuestionList(data?.data?.data);
        }
    }, [data])

    if (isLoading) {
        return <FuseLoading />
    }

    if (error) {
        //Error status and error message
    }

    return (
        <div className="flex-grow overflow-x-auto">
            <FusePageCarded
                classes={{
                    content: 'flex',
                    contentCard: 'overflow-hidden',
                    header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
                }}
                header={<QuestionHeader title="Tüm Sorular" />}
                content={<QuestionTable data={questionList} error={error} refetch={refetch} handleOpenConfirmDialog={handleOpenConfirmDialog} />}
                innerScroll
            />
            <QuestionActiveConfirm open={confirmDialogOpen} handleClose={handleCloseConfirmDialog} data={confirmDialogData} refetch={refetch} />
        </div>
    )
}
