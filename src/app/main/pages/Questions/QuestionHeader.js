import React from 'react';
import Typography from '@material-ui/core/Typography';
import { motion } from 'framer-motion';
import { Button } from '@material-ui/core';
import { useHistory } from 'react-router';
import SearchBox from 'app/shared-components/SearchBox';

export default function QuestionHeader({ title }) {
    const history = useHistory();

    return (
        <div className="flex flex-1 w-full items-center justify-between">
            <div className="flex items-center">
                <Typography
                    component={motion.span}
                    initial={{ x: -20 }}
                    animate={{ x: 0, transition: { delay: 0.2 } }}
                    delay={300}
                    className="text-16 md:text-24 mx-12 font-semibold"
                >
                    {title}
                </Typography>
            </div>
            <motion.div initial={{ opacity: 0, x: 20 }} animate={{ opacity: 1, x: 0, transition: { delay: 0.2 } }}>
                <Button
                    className="whitespace-nowrap"
                    variant="contained"
                    style={{ backgroundColor: 'darkgreen', color: 'white' }}
                    onClick={() => { history.push('/pages/question/new') }}
                >
                    <span className="hidden sm:flex">Yeni Soru Ekle</span>
                    <span className="flex sm:hidden">Yeni</span>
                </Button>
            </motion.div>
        </div>
    )
}
