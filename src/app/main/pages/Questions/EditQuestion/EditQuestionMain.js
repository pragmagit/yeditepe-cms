import React, { useEffect, useState } from 'react';
import FusePageCarded from '@fuse/core/FusePageCarded';
import { useQuery } from 'react-query';
import APICall from 'app/api/APICall';
import FuseLoading from '@fuse/core/FuseLoading';
import { useAuth } from 'app/main/contexts/AuthContext';
import { Redirect, useParams } from 'react-router';
import EditQuestionForm from './EditQuestionForm';
import EditQuestionHeader from './EditQuestionHeader';
import { useDialog } from 'app/main/contexts/DialogContext';

export default function EditQuestionMain() {
    const { isLoggedIn, remindProcess } = useAuth();
    const { questionId } = useParams();
    const { showFailureDialog } = useDialog();

    if (remindProcess) {
        return <FuseLoading />
    }

    if (!isLoggedIn) {
        return <Redirect to="/" />
    }

    const { data, error, isLoading } = useQuery('Get Question By Id', () => APICall.GetQuestionDetails(questionId), { cacheTime: 10000, refetchOnWindowFocus: false, retry: false });

    if (isLoading) {
        return <FuseLoading />
    }

    if (error) {
        showFailureDialog(error?.response?.data?.errorDescription);
        return <Redirect to="/pages/questions" />;
    }

    return (
        <div className="flex-grow overflow-x-auto">
            <FusePageCarded
                header={<EditQuestionHeader title="Soruyu Güncelle" />}
                content={<EditQuestionForm data={data?.data?.data} questionId={questionId} />}
                innerScroll
            />
        </div>
    )
}
