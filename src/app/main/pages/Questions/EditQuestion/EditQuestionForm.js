import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import { motion } from 'framer-motion';
import { Controller, useForm } from 'react-hook-form';
import { Button } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import { FormControlLabel, FormLabel } from '@material-ui/core';
import { Checkbox, Radio, RadioGroup } from '@material-ui/core';
import { useEffect, useState } from 'react';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import _ from '@lodash';
import { ConfirmationType, ConfirmationTypeForBoolean, ConfirmationTypeForNumber, ConfirmationTypeReverse, Datatype, DatatypeReverse, DatatypeTR, DatatypeTRReverse, PhotoRequirementType, PhotoRequirementTypeForOthers, PhotoRequirementTypeReverse, QuestionType, QuestionTypeReverse, ShipmentType, ShipmentTypeReverse } from 'app/enum/enums';
import { FormControl, Select, InputLabel, MenuItem, FormGroup } from '@material-ui/core';
import ChoiceSection from '../ChoiceSection';
import APICall from 'app/api/APICall';
import { Typography } from '@material-ui/core';
import { CircularProgress } from '@material-ui/core';
import { useHistory } from 'react-router';
import { useDialog } from 'app/main/contexts/DialogContext';
import { useWindow } from 'app/main/contexts/WindowContext';

const useStyles = makeStyles(theme => ({
    root: {},
    board: {
        cursor: 'pointer',
        transitionProperty: 'box-shadow border-color',
        transitionDuration: theme.transitions.duration.short,
        transitionTimingFunction: theme.transitions.easing.easeInOut
    },
    newBoard: {},
}));

const schema = yup.object().shape({
    questionText: yup.string().required('Boş bırakılamaz.'),
    questionTextEN: yup.string().nullable(),
    questionHelpText: yup.string().nullable()
});


export default function EditQuestionForm(props) {
    const classes = useStyles(props);
    const { data, displayMessage, questionId } = props;
    const history = useHistory();
    const { showSuccessDialog, showFailureDialog } = useDialog();
    const { windowWidth } = useWindow();
    const [selectedQuestionType, setSelectedQuestionType] = useState(null);
    const [confirmationTypeId, setConfirmationTypeId] = useState(null);
    const [photoRequirementTypeId, setPhotoRequirementTypeId] = useState(null);
    const [choiceSectionActive, setChoiceSectionActive] = useState(false);
    const [choiceList, setChoiceList] = useState([]);
    const [isRequired, setIsRequired] = useState(false);
    const [updateLoading, setUpdateLoading] = useState(false);
    const [isConfirmationTypeActive, setIsConfirmationTypeActive] = useState(false);

    if (!data.question) {
        return (
            <motion.div
                initial={{ opacity: 0 }}
                animate={{ opacity: 1, transition: { delay: 0.1 } }}
                className="flex flex-1 items-center justify-center h-full"
            >
                <Typography color="textSecondary" variant="h5">
                    Düzenlemek istediğiniz soru bulunamadı!
                </Typography>
            </motion.div>
        );
    }

    const defaultValues = {
        questionText: data?.question?.questionText,
        questionTextEN: data?.question?.questionTextEN,
        questionHelpText: data?.question?.questionHelpText
    }

    const { control, formState, handleSubmit, reset } = useForm({
        mode: 'onChange',
        defaultValues,
        resolver: yupResolver(schema)
    });

    const { isValid, dirtyFields, errors } = formState;

    const editQuestion = async (data) => {
        setUpdateLoading(true);
        try {
            if (selectedQuestionType) {
                if (photoRequirementTypeId !== null && photoRequirementTypeId !== undefined) {
                    if ((isConfirmationTypeActive && (confirmationTypeId !== null && confirmationTypeId !== undefined)) || !isConfirmationTypeActive) {
                        data.questionId = parseInt(questionId);
                        data.isRequired = isRequired;
                        data.answerTypeId = selectedQuestionType;
                        data.photoRequirementTypeId = parseInt(photoRequirementTypeId);
                        data.confirmationTypeId = isConfirmationTypeActive ? parseInt(confirmationTypeId) : null;
                        data.isSpecialQuestion = selectedQuestionType === DatatypeReverse["Multiple Choice"] || selectedQuestionType === DatatypeReverse["Single Choice"] ? true : false;
                        data.choices = data.isSpecialQuestion ? choiceList : [];
                        const res = await APICall.EditQuestion(data);
                        if (res.status === 200) {
                            showSuccessDialog('Soru başarıyla güncellendi');
                            history.push('/pages/questions');
                        }
                        else {
                            showFailureDialog(res?.errorDescription);
                        }
                    }
                    else {
                        showFailureDialog("Tüm alanları eksiksiz doldurunuz.");
                    }
                }
                else {
                    showFailureDialog("Tüm alanları eksiksiz doldurunuz.");
                }
            }
            else {
                showFailureDialog("Tüm alanları eksiksiz doldurunuz.");
            }
            setUpdateLoading(false);
        }
        catch (err) {
            showFailureDialog(err?.response?.data?.errorDescription ? err?.response?.data?.errorDescription : "Beklenmeyen Bir Hata");
            setUpdateLoading(false);
        }

    }

    const handleAnswerTypeChange = (e) => {
        setSelectedQuestionType(e.target.value);
        setConfirmationTypeId(null);
        setPhotoRequirementTypeId(null);
    }

    useEffect(() => {
        if (selectedQuestionType === DatatypeReverse["Multiple Choice"] || selectedQuestionType === DatatypeReverse["Single Choice"]) {
            setChoiceSectionActive(true);
        }
        else {
            setChoiceSectionActive(false);
        }
    }, [selectedQuestionType])

    useEffect(() => {
        if (data) {
            setIsRequired(data.question.isRequired);
            handleAnswerTypeChange({ target: { value: data?.question.answerDataTypeId } });
            setConfirmationTypeId(data.question.isConfirmationRequired);
            setPhotoRequirementTypeId(data.question.questionPhotoRequirementId);
            setChoiceList(data.specialQuestionAnswers);
            reset({
                questionText: data?.question?.questionText,
                questionTextEN: data?.question?.questionTextEN,
                questionHelpText: data?.question?.questionHelpText
            })
        }
    }, [data])

    const activeElementsForConfirmation = [DatatypeReverse["Boolean"], DatatypeReverse["Number"], DatatypeReverse["Decimal"]];

    useEffect(() => {
        if (selectedQuestionType) {
            setIsConfirmationTypeActive(activeElementsForConfirmation.includes(selectedQuestionType));
        }
    }, [selectedQuestionType])

    return (
        <div className={clsx(classes.root, 'flex flex-grow flex-shrink-0 flex-col items-center p-16 md:p-24')}>
            <div style={{ width: windowWidth > 550 ? 500 : '100%' }}>
                <form
                    name="questionForm"
                    noValidate
                    className="flex flex-col justify-center w-full"
                    onSubmit={handleSubmit(editQuestion)}
                >
                    <Controller
                        name="questionText"
                        control={control}
                        render={({ field }) => (
                            <TextField
                                {...field}
                                className="mb-16"
                                style={{ marginBottom: 16 }}
                                label="Soru"
                                type="text"
                                error={!!errors.questionText}
                                helperText={errors?.questionText?.message}
                                variant="outlined"
                                required
                                fullWidth
                            />
                        )}
                    />

                    <Controller
                        name="questionTextEN"
                        control={control}
                        render={({ field }) => (
                            <TextField
                                {...field}
                                className="mb-16"
                                style={{ marginBottom: 16 }}
                                label="Soru (İngilizce)"
                                type="text"
                                error={!!errors.questionTextEN}
                                helperText={errors?.questionTextEN?.message}
                                variant="outlined"
                                fullWidth
                            />
                        )}
                    />

                    <Controller
                        name="questionHelpText"
                        control={control}
                        render={({ field }) => (
                            <TextField
                                {...field}
                                style={{ marginBottom: 16 }}
                                className="mb-16"
                                label="Yardımcı Soru Yazısı"
                                type="text"
                                error={!!errors.questionHelpText}
                                helperText={errors?.questionHelpText?.message}
                                variant="outlined"
                                fullWidth
                            />
                        )}
                    />

                    <FormControlLabel
                        style={{ marginBottom: 16 }}
                        control={
                            <Checkbox
                                onChange={() => setIsRequired(!isRequired)}
                                checked={isRequired}
                                color="primary"
                            />
                        }
                        label="Sorunun Cevaplanması Zorunlu"
                    />

                    <FormControl variant="standard" style={{ marginBottom: 16 }}>
                        <InputLabel id="answerType-simple-select-label" shrink>Cevap Tipi</InputLabel>
                        <Select
                            value={selectedQuestionType}
                            onChange={handleAnswerTypeChange}
                            labelId="answerType-simple-select-label"
                            id="answerType-simple-select"
                            label="Cevap Tipi"
                            required
                        >
                            <MenuItem disabled value={null}>
                                <em>Seçiniz...</em>
                            </MenuItem>
                            {Object.values(DatatypeTR).map(datatype => <MenuItem value={DatatypeTRReverse[datatype]}>{datatype}</MenuItem>)}
                        </Select>
                    </FormControl>

                    {choiceSectionActive ? <ChoiceSection choiceList={choiceList} setChoiceList={setChoiceList} /> : null}

                    {isConfirmationTypeActive ?
                        <FormControl variant="standard" style={{ marginBottom: 16 }}>
                            <InputLabel id="merkezonayi-simple-select-label" shrink>Merkez Onayı</InputLabel>
                            <Select
                                value={confirmationTypeId}
                                onChange={(e) => setConfirmationTypeId(e.target.value)}
                                labelId="merkezonayi-simple-select-label"
                                id="merkezonayi-simple-select"
                                label="Merkez Onayı"
                                required
                            >
                                <MenuItem disabled value={null}>
                                    <em>Seçiniz...</em>
                                </MenuItem>
                                {Object.values(selectedQuestionType === DatatypeReverse["Boolean"] ? ConfirmationTypeForBoolean
                                    :
                                    selectedQuestionType === DatatypeReverse["Number"] || selectedQuestionType === DatatypeReverse["Decimal"] ? ConfirmationTypeForNumber
                                        :
                                        ConfirmationType).map(confirmationType => <MenuItem value={ConfirmationTypeReverse[confirmationType]}>{confirmationType}</MenuItem>)}
                            </Select>
                        </FormControl> : null}

                    {selectedQuestionType ?
                        <FormControl variant="standard" style={{ marginBottom: 16 }}>
                            <InputLabel id="photoReqType-simple-select-label" shrink>Fotoğraf Zorunluluğu</InputLabel>
                            <Select
                                value={photoRequirementTypeId}
                                onChange={(e) => setPhotoRequirementTypeId(e.target.value)}
                                labelId="photoReqType-simple-select-label"
                                id="photoReqType-simple-select"
                                label="Fotoğraf Zorunluluğu"
                                required
                            >
                                <MenuItem disabled value={null}>
                                    <em>Seçiniz...</em>
                                </MenuItem>
                                {Object.values(selectedQuestionType === DatatypeReverse["Boolean"] ? PhotoRequirementType : PhotoRequirementTypeForOthers).map(photoReqType => <MenuItem value={PhotoRequirementTypeReverse[photoReqType]}>{photoReqType}</MenuItem>)}
                            </Select>
                        </FormControl> : null}

                    <div className="flex flex-col sm:flex-row items-center justify-center sm:justify-between" />

                    <Button
                        variant="contained"
                        color="primary"
                        className="w-full mx-auto mt-16"
                        aria-label="NEW QUESTION"
                        type="submit"
                        disabled={updateLoading}
                    >
                        {updateLoading ? <div><CircularProgress size={20} /> Güncelleniyor</div> : "Soruyu Güncelle"}

                    </Button>
                </form>
            </div>
        </div>
    )
}
