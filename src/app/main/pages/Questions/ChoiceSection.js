import { makeStyles } from '@material-ui/core/styles';
import { Controller, useForm } from 'react-hook-form';
import { Button } from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import TextField from '@material-ui/core/TextField';
import _ from '@lodash';
import { useState } from 'react';

const useStyles = makeStyles((theme) => ({
    margin: {
        margin: theme.spacing(1),
    },
    extendedIcon: {
        marginRight: theme.spacing(1),
    },
}));

export default function ChoiceSection({ choiceList, setChoiceList }) {
    const [choiceText, setChoiceText] = useState("");
    const classes = useStyles();

    const handleChangeText = (e) => {
        setChoiceText(e.target.value);
    }

    const deleteChoice = (choice) => {
        setChoiceList(choiceList.filter(c => c !== choice));
    }

    const addChoice = () => {
        if (choiceText !== "") {
            setChoiceList([...choiceList, choiceText]);
        }
        setChoiceText("");
    }

    return (
        <div style={{ width: '100%', marginTop: 32, marginBottom: 32, border: '1px solid #f0f0f0', padding: 24 }}>
            <TextField
                className="mb-16"
                label="Şık Ekle"
                autoFocus
                type="text"
                variant="outlined"
                value={choiceText}
                onChange={handleChangeText}
                fullWidth
            />

            <Button
                variant="contained"
                color="primary"
                className="w-full mx-auto mt-16"
                aria-label="ADD CHOICE"
                onClick={addChoice}
            >
                Şık Ekle
            </Button>

            {choiceList.map(choice => {
                return (
                    <div style={{ width: '100%', display: 'flex', justifyContent: 'space-between', marginTop: 8, border: '1px solid #f0f0f0', padding: 8, alignItems: 'center' }}>
                        <strong style={{ width: '80%', wordWrap: 'break-word' }}>{choice}</strong>
                        <IconButton aria-label="delete" className={classes.margin}>
                            <DeleteIcon fontSize="small" onClick={() => { deleteChoice(choice) }} />
                        </IconButton>
                    </div>)
            })}
        </div>
    )
}