import { lazy } from 'react';
import EditQuestionMain from './EditQuestion/EditQuestionMain';
import NewQuestionMain from './NewQuestion/NewQuestionMain';
import QuestionsMain from './QuestionsMain';

const QuestionConfig = {
    settings: {
        layout: {
            config: {
                navbar: {
                    display: true
                },
                toolbar: {
                    display: true
                },
                footer: {
                    display: false
                },
                leftSidePanel: {
                    display: false
                },
                rightSidePanel: {
                    display: false
                }
            }
        }
    },
    routes: [
        {
            path: '/pages/questions',
            component: QuestionsMain
        },
        {
            path: '/pages/question/new',
            component: NewQuestionMain
        },
        {
            path: '/pages/question/edit/:questionId',
            component: EditQuestionMain
        }
    ]
};

export default QuestionConfig;
