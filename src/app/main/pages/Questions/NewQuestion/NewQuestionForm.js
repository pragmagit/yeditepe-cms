import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import { motion } from 'framer-motion';
import { Controller, useForm } from 'react-hook-form';
import { Button, CircularProgress } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import { FormControlLabel } from '@material-ui/core';
import { Checkbox } from '@material-ui/core';
import { useState } from 'react';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import _ from '@lodash';
import { ConfirmationType, ConfirmationTypeForBoolean, ConfirmationTypeForNumber, ConfirmationTypeReverse, Datatype, DatatypeReverse, DatatypeTR, DatatypeTRReverse, PhotoRequirementType, PhotoRequirementTypeForOthers, PhotoRequirementTypeReverse, QuestionType, QuestionTypeReverse, ShipmentType, ShipmentTypeReverse } from 'app/enum/enums';
import { FormControl, Select, InputLabel, MenuItem, FormGroup } from '@material-ui/core';
import APICall from 'app/api/APICall';
import { useHistory } from 'react-router';
import { useDialog } from 'app/main/contexts/DialogContext';
import { useWindow } from 'app/main/contexts/WindowContext';
import { useEffect } from 'react';
import ChoiceSection from '../ChoiceSection';

const useStyles = makeStyles(theme => ({
    root: {},
    board: {
        cursor: 'pointer',
        transitionProperty: 'box-shadow border-color',
        transitionDuration: theme.transitions.duration.short,
        transitionTimingFunction: theme.transitions.easing.easeInOut
    },
    newBoard: {},
}));

const schema = yup.object().shape({
    questionText: yup.string().required('Boş bırakılamaz.'),
    questionTextEN: yup.string().nullable(),
    questionHelpText: yup.string().nullable()
});


export default function NewQuestionForm(props) {
    const classes = useStyles(props);
    const history = useHistory();
    const { showSuccessDialog, showFailureDialog } = useDialog();
    const { windowWidth } = useWindow();
    const [selectedQuestionType, setSelectedQuestionType] = useState(null);
    const [choiceList, setChoiceList] = useState([]);
    const [choiceSectionActive, setChoiceSectionActive] = useState(false);
    const [addLoading, setAddLoading] = useState(false);
    const [isConfirmationTypeActive, setIsConfirmationTypeActive] = useState(false);

    const { control, formState, handleSubmit, reset, setValue } = useForm({
        mode: 'onChange',
        resolver: yupResolver(schema)
    });

    const { isValid, dirtyFields, errors } = formState;

    const addQuestion = async (data) => {
        setAddLoading(true);
        try {
            const { photoRequirementTypeId, confirmationTypeId } = data;
            if (selectedQuestionType) {
                if ((isConfirmationTypeActive && (confirmationTypeId !== null && confirmationTypeId !== undefined)) || !isConfirmationTypeActive) {
                    if (photoRequirementTypeId !== null && photoRequirementTypeId !== undefined) {
                        data.isRequired = data.isRequired ? true : false;
                        data.answerTypeId = selectedQuestionType;
                        data.confirmationTypeId = isConfirmationTypeActive ? parseInt(confirmationTypeId) : null;
                        data.isSpecialQuestion = selectedQuestionType === DatatypeReverse["Multiple Choice"] || selectedQuestionType === DatatypeReverse["Single Choice"];
                        data.choices = data.isSpecialQuestion ? choiceList : [];
                        const res = await APICall.AddQuestion(data);
                        if (res.status === 200) {
                            showSuccessDialog('Soru başarıyla eklendi');
                            history.push('/pages/questions');
                        }
                        else {
                            showFailureDialog(res?.errorDescription);
                        }
                    }
                    else {
                        showFailureDialog("Tüm alanları eksiksiz doldurunuz.");
                    }
                }
                else {
                    showFailureDialog("Tüm alanları eksiksiz doldurunuz.");
                }
            }
            else {
                showFailureDialog("Tüm alanları eksiksiz doldurunuz.");
            }
            setAddLoading(false);
        }
        catch (err) {
            showFailureDialog(err?.response?.data?.errorDescription ? err?.response?.data?.errorDescription : "Beklenmeyen Bir Hata");
            setAddLoading(false);
        }
    }

    const handleAnswerTypeChange = (e) => {
        setSelectedQuestionType(e.target.value);
        setValue('confirmationTypeId', null);
        setValue('photoRequirementTypeId', null);
    }

    const activeElementsForConfirmation = [DatatypeReverse["Boolean"], DatatypeReverse["Number"], DatatypeReverse["Decimal"]];

    useEffect(() => {
        if (selectedQuestionType) {
            setIsConfirmationTypeActive(activeElementsForConfirmation.includes(selectedQuestionType));
        }

        if (selectedQuestionType === DatatypeReverse["Multiple Choice"] || selectedQuestionType === DatatypeReverse["Single Choice"]) {
            setChoiceSectionActive(true);
        }
        else {
            setChoiceSectionActive(false);
        }
    }, [selectedQuestionType])

    return (
        <div className={clsx(classes.root, 'flex flex-grow flex-shrink-0 flex-col items-center p-16 md:p-24')}>
            <div style={{ width: windowWidth > 550 ? 500 : '100%' }}>
                <form
                    name="questionForm"
                    noValidate
                    className="flex flex-col justify-center w-full"
                    onSubmit={handleSubmit(addQuestion)}
                >
                    <Controller
                        name="questionText"
                        control={control}
                        render={({ field }) => (
                            <TextField
                                {...field}
                                className="mb-16"
                                style={{ marginBottom: 16 }}
                                label="Soru"
                                type="text"
                                error={!!errors.questionText}
                                helperText={errors?.questionText?.message}
                                variant="outlined"
                                required
                                fullWidth
                            />
                        )}
                    />

                    <Controller
                        name="questionTextEN"
                        control={control}
                        render={({ field }) => (
                            <TextField
                                {...field}
                                className="mb-16"
                                style={{ marginBottom: 16 }}
                                label="Soru (İngilizce)"
                                type="text"
                                error={!!errors.questionTextEN}
                                helperText={errors?.questionTextEN?.message}
                                variant="outlined"
                                fullWidth
                            />
                        )}
                    />

                    <Controller
                        name="questionHelpText"
                        control={control}
                        render={({ field }) => (
                            <TextField
                                {...field}
                                style={{ marginBottom: 16 }}
                                className="mb-16"
                                label="Yardımcı Soru Yazısı"
                                type="text"
                                error={!!errors.questionHelpText}
                                helperText={errors?.questionHelpText?.message}
                                variant="outlined"
                                fullWidth
                            />
                        )}
                    />

                    <Controller
                        name="isRequired"
                        control={control}
                        render={({ field }) => (
                            <FormControlLabel
                                style={{ marginBottom: 16 }}
                                control={
                                    <Checkbox
                                        {...field}
                                        color="primary"
                                    />
                                }
                                label="Sorunun Cevaplanması Zorunlu"
                            />
                        )}
                    />

                    <Controller
                        name="answerTypeId"
                        control={control}
                        render={({ field }) => (
                            <FormControl variant="outlined" style={{ marginBottom: 16 }}>
                                <InputLabel id="answerType-select-outlined-label">Cevap Tipi</InputLabel>
                                <Select
                                    {...field}
                                    value={selectedQuestionType}
                                    onChange={handleAnswerTypeChange}
                                    labelId="answerType-select-outlined-label"
                                    id="answerType-select-outlined"
                                    label="Cevap Tipi"
                                    required
                                >
                                    <MenuItem disabled value={null}>
                                        <em>Seçiniz...</em>
                                    </MenuItem>
                                    {Object.values(DatatypeTR).map(datatype => <MenuItem value={DatatypeTRReverse[datatype]}>{datatype}</MenuItem>)}
                                </Select>
                            </FormControl>
                        )}
                    />

                    {choiceSectionActive ? <ChoiceSection choiceList={choiceList} setChoiceList={setChoiceList} /> : null}

                    {isConfirmationTypeActive ?
                        <Controller
                            name="confirmationTypeId"
                            control={control}
                            render={({ field }) => (
                                <FormControl variant="outlined" style={{ marginBottom: 16 }}>
                                    <InputLabel id="merkezonayi-select-outlined-label">Merkez Onayı</InputLabel>
                                    <Select
                                        {...field}
                                        labelId="merkezonayi-select-outlined-label"
                                        id="merkezonayi-select-outlined"
                                        label="Merkez Onayı"
                                        required
                                    >
                                        <MenuItem disabled value={null}>
                                            <em>Seçiniz...</em>
                                        </MenuItem>
                                        {Object.values(selectedQuestionType === DatatypeReverse["Boolean"] ? ConfirmationTypeForBoolean
                                            :
                                            selectedQuestionType === DatatypeReverse["Number"] || selectedQuestionType === DatatypeReverse["Decimal"] ? ConfirmationTypeForNumber
                                                :
                                                ConfirmationType).map(confirmationType => <MenuItem value={ConfirmationTypeReverse[confirmationType]}>{confirmationType}</MenuItem>)}
                                    </Select>
                                </FormControl>
                            )}
                        /> : null}

                    {selectedQuestionType ?
                        <Controller
                            name="photoRequirementTypeId"
                            control={control}
                            render={({ field }) => (
                                <FormControl variant="outlined" style={{ marginBottom: 16 }}>
                                    <InputLabel id="photoReqType-select-outlined-label">Fotoğraf Zorunluluğu</InputLabel>
                                    <Select
                                        {...field}
                                        labelId="photoReqType-select-outlined-label"
                                        id="photoReqType-select-outlined"
                                        label="Fotoğraf Zorunluluğu"
                                        required
                                    >
                                        <MenuItem disabled value={null}>
                                            <em>Seçiniz...</em>
                                        </MenuItem>
                                        {Object.values(selectedQuestionType === DatatypeReverse["Boolean"] ? PhotoRequirementType : PhotoRequirementTypeForOthers).map(photoReqType => <MenuItem value={PhotoRequirementTypeReverse[photoReqType]}>{photoReqType}</MenuItem>)}
                                    </Select>
                                </FormControl>
                            )}
                        /> : null}


                    <div className="flex flex-col sm:flex-row items-center justify-center sm:justify-between" />

                    <Button
                        variant="contained"
                        color="primary"
                        className="w-full mx-auto mt-16"
                        aria-label="NEW QUESTION"
                        disabled={_.isEmpty(dirtyFields) || !isValid || addLoading}
                        type="submit"
                    >
                        {addLoading ? <div><CircularProgress size={20} /> Yükleniyor</div> : "Soru Ekle"}
                    </Button>
                </form>
            </div>
        </div>
    )
}
