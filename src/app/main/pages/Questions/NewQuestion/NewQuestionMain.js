import React, { useEffect, useState } from 'react';
import FusePageCarded from '@fuse/core/FusePageCarded';
import FuseLoading from '@fuse/core/FuseLoading';
import { useAuth } from 'app/main/contexts/AuthContext';
import { Redirect } from 'react-router';
import NewQuestionForm from './NewQuestionForm';
import NewQuestionHeader from './NewQuestionHeader';
import { useQuery } from 'react-query';
import APICall from 'app/api/APICall';
import { useDialog } from 'app/main/contexts/DialogContext';

export default function NewQuestionMain() {
    const { isLoggedIn, remindProcess } = useAuth();
    const { showFailureDialog } = useDialog();

    if (remindProcess) {
        return <FuseLoading />
    }

    if (!isLoggedIn) {
        return <Redirect to="/" />
    }

    const { data, error, isLoading } = useQuery('Check permission for new question', () => APICall.CheckNewQuestionRole(), { cacheTime: 10000, refetchOnWindowFocus: false, retry: false });

    if (isLoading) {
        return <FuseLoading />
    }

    if (error) {
        showFailureDialog(error?.response?.data?.errorDescription);
        return <Redirect to="/pages/questions" />;
    }

    return (
        <div className="flex-grow overflow-x-auto">
            <FusePageCarded
                classes={{
                    content: 'flex',
                    contentCard: 'overflow-hidden',
                    header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
                }}
                header={<NewQuestionHeader title="Yeni Soru Ekle" />}
                content={<NewQuestionForm />}
                innerScroll
            />
        </div>
    )
}
