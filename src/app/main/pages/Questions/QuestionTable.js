import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import Typography from '@material-ui/core/Typography';
import { motion } from 'framer-motion';
import _ from 'lodash';
import { useHistory } from 'react-router';
import { WeightUnit, Datatype, ConfirmationType, PhotoRequirementType, DatatypeTR, ShipmentType, ShipmentTypeReverse } from '../../../enum/enums';
import DataGrid, { Column, FilterRow, HeaderFilter, Scrolling, Paging } from 'devextreme-react/data-grid';
import { Button } from 'devextreme-react/button';
import { useAuth } from 'app/main/contexts/AuthContext';
import APICall from 'app/api/APICall';
import { useDialog } from 'app/main/contexts/DialogContext';

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

export default function QuestionTable({ data, error, refetch, handleOpenConfirmDialog }) {
    const classes = useStyles();
    const history = useHistory();
    const { showSuccessDialog, showFailureDialog } = useDialog();

    if (data.length === 0 || error) {
        return (
            <motion.div
                initial={{ opacity: 0 }}
                animate={{ opacity: 1, transition: { delay: 0.1 } }}
                className="flex flex-1 items-center justify-center h-full"
            >
                <Typography color="textSecondary" variant="h5">
                    {error?.response?.data?.errorDescription ? error?.response?.data?.errorDescription : "Soru bulunamadı!"}
                </Typography>
            </motion.div>
        );
    }

    const calculateIsRequiredCell = (data) => {
        return data.question.isRequired ? "Zorunlu" : "Zorunlu Değil";
    }

    const calculateIsSpecialQuestionCell = (data) => {
        return data.question.isSpecialQuestion ? "Özel Soru" : "Özel Soru Değil";
    }

    const calculateAnswerTypeCell = (data) => {
        return DatatypeTR[data.question.answerDataTypeId];
    }

    const calculatePhotoReqCell = (data) => {
        return PhotoRequirementType[data.question.questionPhotoRequirementId];
    }

    const calculateConfirationReqCell = (data) => {
        return ConfirmationType[data.question.isConfirmationRequired];
    }

    const calculateShipmentCell = (data) => {
        return data.shipmentTypes.map(type => ShipmentType[type]);
    }

    const renderEditButton = (cellData) => {
        return (
            <div>
                <Button
                    text="Düzenle"
                    type="default"
                    stylingMode="text"
                    onClick={() => history.push('/pages/question/edit/' + cellData.data.question.id)}
                />
            </div>
        )
    }

    const handleChangeQuestionStatus = async (data) => {
    }

    const renderActiveButton = (cellData) => {
        return (
            <div>
                <Button
                    text={cellData.data.question.isActive ? "Aktif" : "Aktif Değil"}
                    type="default"
                    stylingMode="text"
                    style={{ color: cellData.data.question.isActive ? 'green' : 'red' }}
                    onClick={() => { handleOpenConfirmDialog(cellData.data) }}
                />
            </div>
        )
    }

    const shipmentTypeFilter = Object.values(ShipmentType).map(type => { return { text: type, value: ['shipmentTypes', 'contains', type] } })

    return (
        <div className="w-full flex flex-col">
            <FuseScrollbars className="flex-grow overflow-x-auto">
                <DataGrid
                    height="100%"
                    keyExpr="question.id"
                    dataSource={data}
                    showBorders={true}
                    selection={{ mode: 'single' }}
                    hoverStateEnabled={true}
                    noDataText="Soru Bulunamadı"
                    paging={false}
                    allowColumnResizing={true}
                    style={{ padding: 10 }}
                >
                    <Scrolling showScrollbar="always" mode="virtual" rowRenderingMode="virtual" />
                    <Paging defaultPageSize={20} />
                    <FilterRow visible={true} applyFilter='auto' />
                    <HeaderFilter visible={true} />
                    <Column
                        dataField="question.questionText"
                        caption="Soru"
                        dataType="string"
                        width={140}
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="question.questionTextEN"
                        caption="Soru (İngilizce)"
                        dataType="string"
                        width={140}
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="question.questionHelpText"
                        caption="Soru Yardımcı Yazı"
                        dataType="string"
                        width={180}
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="question.isRequired"
                        caption="Soru Cevaplama Zorunluluğu"
                        dataType="string"
                        calculateCellValue={calculateIsRequiredCell}
                        width={140}
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="question.isSpecialQuestion"
                        caption="Özel Soru"
                        dataType="string"
                        calculateCellValue={calculateIsSpecialQuestionCell}
                        width={140}
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="question.answerDataTypeId"
                        caption="Cevap Tipi"
                        dataType="string"
                        calculateCellValue={calculateAnswerTypeCell}
                        width={140}
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="question.questionPhotoRequirementId"
                        caption="Fotoğraf Gerekliliği"
                        dataType="string"
                        calculateCellValue={calculatePhotoReqCell}
                        width={180}
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="question.isConfirmationRequired"
                        caption="Teyit"
                        dataType="string"
                        calculateCellValue={calculateConfirationReqCell}
                        width={140}
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="memberEmail"
                        caption="Soruyu Oluşturan"
                        dataType="string"
                        width={140}
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="question.createdOn"
                        caption="Oluşturma Tarihi"
                        dataType="datetime"
                        format="dd/MM/yyyy HH:mm"
                        width={140}
                        cssClass="justify-table"
                    >
                        <HeaderFilter groupInterval="day" />
                    </Column>
                    <Column
                        dataField="shipmentTypes"
                        caption="Gönderim Türü"
                        dataType="string"
                        calculateCellValue={calculateShipmentCell}
                        width={140}
                        cssClass="justify-table"
                    >
                        <HeaderFilter dataSource={shipmentTypeFilter} />
                    </Column>
                    <Column
                        cellRender={renderActiveButton}
                        width={140}
                        cssClass="justify-table"
                    />
                    <Column
                        cellRender={renderEditButton}
                        width={140}
                        cssClass="justify-table"
                    />
                </DataGrid>
            </FuseScrollbars>
        </div>
    )
}