import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import APICall from 'app/api/APICall';
import { useDialog } from 'app/main/contexts/DialogContext';
import { QuestionType, ShipmentType } from 'app/enum/enums';
import { CircularProgress } from '@material-ui/core';

export default function ShipmentConfirmDialog({ open, handleClose, deleteData, refetch }) {
    const { showSuccessDialog, showFailureDialog } = useDialog();
    const [deleteButtonDisable, setDeleteButtonDisable] = useState(false);

    const rejectDeletion = () => {
        handleClose();
    }

    const acceptDeletion = async () => {
        setDeleteButtonDisable(true);
        try {
            if (deleteData) {
                const requestData = {
                    shipmentTypeId: deleteData?.shipmentTypeId,
                    isVehicleQuestion: deleteData?.isVehicleQuestion,
                    questionId: parseInt(deleteData?.question?.id)
                }
                await APICall.DeleteQuestionFromShipmentType(requestData);
            }
            await refetch();
            setDeleteButtonDisable(false);
            showSuccessDialog("Soru, ürün tipinden başarıyla kaldırıldı.");
            handleClose();
        }
        catch (err) {
            setDeleteButtonDisable(false);
            showFailureDialog(err?.response?.data?.errorDescription);
        }
    }

    return (
        <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">{"Soruyu kaldırmak istediğinize emin misiniz?"}</DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    {deleteData ? `\"${deleteData?.question?.questionText}\" sorusunu \"${QuestionType[deleteData?.isVehicleQuestion ? 1 : 0] + "/" + ShipmentType[deleteData?.shipmentTypeId]}\" ürün tipinden kaldırmak istediğinize emin misiniz?` : null}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={rejectDeletion} color="secondary" disabled={deleteButtonDisable}>
                    İptal Et
                </Button>
                <Button onClick={acceptDeletion} style={{ backgroundColor: 'red', color: 'white' }} autoFocus disabled={deleteButtonDisable}>
                    {deleteButtonDisable ? <CircularProgress size={20} /> : null} Sil
                </Button>
            </DialogActions>
        </Dialog>
    )
}
