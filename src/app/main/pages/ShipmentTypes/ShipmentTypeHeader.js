import React from 'react';
import Typography from '@material-ui/core/Typography';
import { motion } from 'framer-motion';
import { Button, FormControl, InputLabel, MenuItem, Select } from '@material-ui/core';
import { useHistory } from 'react-router';
import { QuestionType, QuestionTypeReverse, ShipmentType, ShipmentTypeReverse } from 'app/enum/enums';
import { useWindow } from 'app/main/contexts/WindowContext';

export default function ShipmentTypeHeader({ isVehicleQuestion, shipmentTypeId, changeIsVehicleQuestion, changeShipmentTypeId }) {
    const history = useHistory();
    const { windowWidth } = useWindow();

    return (
        <div className="flex flex-1 w-full items-center justify-between">
            <div style={{ minWidth: 150 }}>
                <FormControl variant="standard" style={{ marginRight: 10, minWidth: windowWidth > 600 ? 150 : windowWidth > 480 ? 100 : 70 }}>
                    <InputLabel id="QuestionType-simple-select-label" shrink>Soru Tipi</InputLabel>
                    <Select
                        value={isVehicleQuestion}
                        onChange={e => changeIsVehicleQuestion(e.target.value)}
                        labelId="QuestionType-simple-select-label"
                        id="QuestionType-simple-select"
                        label="Soru Tipi"
                        required
                    >
                        <MenuItem disabled value={undefined}>
                            <em>Seçiniz...</em>
                        </MenuItem>
                        {Object.values(QuestionType).map(questionType => <MenuItem value={QuestionTypeReverse[questionType]}>{questionType}</MenuItem>)}
                    </Select>
                </FormControl>
                <FormControl variant="standard" style={{ marginBottom: 0, minWidth: windowWidth > 600 ? 150 : windowWidth > 480 ? 100 : 70 }}>
                    <InputLabel id="ShipmentType-simple-select-label" shrink>Ürün Tipi</InputLabel>
                    <Select
                        value={shipmentTypeId}
                        onChange={e => changeShipmentTypeId(e.target.value)}
                        labelId="ShipmentType-simple-select-label"
                        id="ShipmentType-simple-select"
                        label="Ürün Tipi"
                        required
                    >
                        <MenuItem disabled value={undefined}>
                            <em>Seçiniz...</em>
                        </MenuItem>
                        {Object.values(ShipmentType).map(shipmentType => <MenuItem value={ShipmentTypeReverse[shipmentType]}>{shipmentType}</MenuItem>)}
                    </Select>
                </FormControl>
            </div>
            <div className="flex items-center">
                <Typography
                    component={motion.span}
                    initial={{ x: -20 }}
                    animate={{ x: 0, transition: { delay: 0.2 } }}
                    style={windowWidth > 600 ? null : { width: '100%', wordWrap: 'break-word', fontSize: windowWidth > 480 ? 12 : 9 }}
                    delay={300}
                    className="text-16 md:text-24 mx-12 font-semibold"
                >
                    {isVehicleQuestion !== null && shipmentTypeId !== null ? (QuestionType[isVehicleQuestion] + "/" + ShipmentType[shipmentTypeId]) : "Lütfen Soru Tipi ve Ürün Tipi Seçiniz"}
                </Typography>
            </div>
            {
                isVehicleQuestion !== null && shipmentTypeId !== null ?
                    <motion.div initial={{ opacity: 0, x: 20 }} animate={{ opacity: 1, x: 0, transition: { delay: 0.2 } }}>
                        <Button
                            className="whitespace-nowrap"
                            variant="contained"
                            style={{ backgroundColor: 'darkgreen', color: 'white' }}
                            onClick={() => { history.push('/pages/new-shipment-question/' + isVehicleQuestion + '/' + shipmentTypeId) }}
                        >
                            <span className="hidden sm:flex">Yeni Soru Ekle</span>
                            <span className="flex sm:hidden">Yeni</span>
                        </Button>
                    </motion.div> : null
            }
        </div >
    )
}
