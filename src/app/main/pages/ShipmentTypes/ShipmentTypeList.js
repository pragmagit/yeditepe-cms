import React, { useEffect, useState } from 'react';
import FusePageCarded from '@fuse/core/FusePageCarded';
import ShipmentTypeHeader from './ShipmentTypeHeader';
import { useQuery } from 'react-query';
import APICall from 'app/api/APICall';
import FuseLoading from '@fuse/core/FuseLoading';
import ShipmentTypeTable from './ShipmentTypeTable';
import { QuestionTypeReverse } from 'app/enum/enums';
import ShipmentConfirmDialog from './ShipmentConfirmDialog';

export default function ShipmentTypeList() {
    const [confirmDialogOpen, setConfirmDialogOpen] = useState(false);
    const [dialogDeleteData, setDialogDeleteData] = useState(null);
    const [shipmentTypeList, setShipmentTypeList] = useState([]);
    const [isVehicleQuestion, setIsIncomingVehicleQuestion] = useState(null);
    const [shipmentTypeId, setShipmentTypeId] = useState(null);
    const [queryCriteria, setQueryCriteria] = useState(false);
    const { isLoading, error, data, refetch } = useQuery(['shipmentTypeList', queryCriteria],
        () => APICall.GetQuestionsByShipmentType({ isVehicleQuestion: isVehicleQuestion === QuestionTypeReverse["Araç Kabul"] ? true : false, shipmentTypeId }),
        { cacheTime: 10000, refetchOnWindowFocus: false, retry: false, enabled: queryCriteria ? true : false });

    const handleOpenConfirmDialog = (data) => {
        setDialogDeleteData(data);
        setConfirmDialogOpen(true);
    }

    const handleCloseConfirmDialog = () => {
        setConfirmDialogOpen(false);
    }

    const changeIsVehicleQuestion = (data) => {
        setIsIncomingVehicleQuestion(data);
    }

    const changeShipmentTypeId = (data) => {
        setShipmentTypeId(data);
    }

    useEffect(() => {
        setQueryCriteria(isVehicleQuestion !== null && shipmentTypeId !== null ? isVehicleQuestion + shipmentTypeId : false)
    }, [isVehicleQuestion, shipmentTypeId])

    useEffect(() => {
        if (data) {
            setShipmentTypeList(data?.data?.data);
        }
    }, [data])

    return (
        <div className="flex-grow overflow-x-auto">
            <FusePageCarded
                classes={{
                    content: 'flex',
                    contentCard: 'overflow-hidden',
                    header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
                }}
                header={<ShipmentTypeHeader isVehicleQuestion={isVehicleQuestion} shipmentTypeId={shipmentTypeId} changeIsVehicleQuestion={changeIsVehicleQuestion} changeShipmentTypeId={changeShipmentTypeId} />}
                content={<ShipmentTypeTable data={shipmentTypeList} error={error} refetch={refetch} isLoading={isLoading} shipmentTypeId={shipmentTypeId} isVehicleQuestion={isVehicleQuestion} handleOpenConfirmDialog={handleOpenConfirmDialog} />}
                innerScroll
            />
            <ShipmentConfirmDialog open={confirmDialogOpen} handleClose={handleCloseConfirmDialog} refetch={refetch} deleteData={dialogDeleteData} />
        </div>
    )
}
