import NewShipmentQuestionMain from './NewShipmentQuestion/NewShipmentQuestionMain';
import ShipmentTypeMain from './ShipmentTypeMain';

const ShipmentTypeConfig = {
    settings: {
        layout: {
            config: {
                navbar: {
                    display: true
                },
                toolbar: {
                    display: true
                },
                footer: {
                    display: false
                },
                leftSidePanel: {
                    display: false
                },
                rightSidePanel: {
                    display: false
                }
            }
        }
    },
    routes: [
        {
            path: '/pages/shipment-types',
            component: ShipmentTypeMain
        },
        {
            path: '/pages/new-shipment-question/:isVehicleQuestion/:shipmentTypeId',
            component: NewShipmentQuestionMain
        }
    ]
};

export default ShipmentTypeConfig;
