import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import { motion } from 'framer-motion';
import { Controller, useForm } from 'react-hook-form';
import { Button, CircularProgress, Typography } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import { FormControlLabel } from '@material-ui/core';
import { Checkbox } from '@material-ui/core';
import { useEffect, useState } from 'react';
import _ from '@lodash';
import { DatatypeReverse, QuestionType, QuestionTypeReverse, ShipmentType, ShipmentTypeReverse } from 'app/enum/enums';
import { FormControl, Select, InputLabel, MenuItem, FormGroup, FormLabel } from '@material-ui/core';
import APICall from 'app/api/APICall';
import { useHistory } from 'react-router';
import { useDialog } from 'app/main/contexts/DialogContext';
import { useWindow } from 'app/main/contexts/WindowContext';
import QuestionSelect from './QuestionSelect';
import { useQuery } from 'react-query';
import ShipmentTypeIndexTable from './ShipmentTypeIndexTable';

const useStyles = makeStyles(theme => ({
    root: {},
    board: {
        cursor: 'pointer',
        transitionProperty: 'box-shadow border-color',
        transitionDuration: theme.transitions.duration.short,
        transitionTimingFunction: theme.transitions.easing.easeInOut
    },
    newBoard: {},
}));

export default function NewShipmentQuestionForm(props) {
    const classes = useStyles(props);
    const { isVehicleQuestion, shipmentTypeId, data } = props;
    const history = useHistory();
    const { showSuccessDialog, showFailureDialog } = useDialog();
    const { windowWidth } = useWindow();
    const [buttonDisable, setButtonDisable] = useState(false);
    const [rangeSectionActive, setRangeSectionActive] = useState(false);
    const [range1, setRange1] = useState(null);
    const [range2, setRange2] = useState(null);
    const [orderIndex, setOrderIndex] = useState(null);
    const [answerRangeActive, setAnswerRangeActive] = useState(false);
    const [selectedQuestion, setSelectedQuestion] = useState(null);
    const { isLoading: shipmentSumLoading, data: shipmentSumData } = useQuery('shipment type summary', () => APICall.GetQuestionsByShipmentType({ isVehicleQuestion: parseInt(isVehicleQuestion) === QuestionTypeReverse["Araç Kabul"] ? true : false, shipmentTypeId: parseInt(shipmentTypeId) }), { cacheTime: 10000, refetchOnWindowFocus: false, retry: false });

    const { handleSubmit } = useForm();

    useEffect(() => {
        if (selectedQuestion) {
            setRangeSectionActive(selectedQuestion?.question?.answerDataTypeId === DatatypeReverse["Number"] || selectedQuestion?.question?.answerDataTypeId === DatatypeReverse["Decimal"]);
        }
        setAnswerRangeActive(false);
        setRange1(null);
        setRange2(null);
    }, [selectedQuestion]);

    useEffect(() => {
    }, [])

    if (data.length === 0) {
        return (
            <motion.div
                initial={{ opacity: 0 }}
                animate={{ opacity: 1, transition: { delay: 0.1 } }}
                className="flex flex-1 items-center justify-center h-full"
            >
                <Typography color="textSecondary" variant="h5">
                    Eklemek istediğiniz soru bulunamadı!
                </Typography>
            </motion.div>
        );
    }

    const addQuestion = async (data) => {
        setButtonDisable(true);
        try {
            if (selectedQuestion) {
                if (orderIndex) {
                    if ((rangeSectionActive && answerRangeActive && range1 && range2) || (rangeSectionActive && !answerRangeActive) || !rangeSectionActive) {
                        const requestData = {
                            questionId: parseInt(selectedQuestion?.question?.id),
                            range1: rangeSectionActive && answerRangeActive ? range1 : null,
                            range2: rangeSectionActive && answerRangeActive ? range2 : null,
                            orderIndex,
                            shipmentTypeId: parseInt(shipmentTypeId),
                            isVehicleQuestion: parseInt(isVehicleQuestion) === QuestionTypeReverse["Araç Kabul"] ? true : false
                        }
                        const res = await APICall.AddQuestionToShipmentTypes(requestData);
                        if (res.status === 200) {
                            setButtonDisable(false);
                            showSuccessDialog('İşlem başarıyla tamamlandı.');
                            history.push('/pages/shipment-types');
                        }
                        else {
                            showFailureDialog(res?.errorDescription);
                        }
                    }
                    else {
                        showFailureDialog('Lütfen aralıkları giriniz.');
                    }
                }
                else {
                    showFailureDialog('Lütfen sorunun sırasını yazınız.');
                }
            }
            else {
                showFailureDialog('Lütfen bir soru seçiniz.');
            }
            setButtonDisable(false);
        }
        catch (err) {
            showFailureDialog(err?.response?.data?.errorDescription ? err?.response?.data?.errorDescription : "Beklenmeyen Bir Hata");
            setButtonDisable(false);
        }
    }

    return (
        <div style={{ width: '100%', display: 'flex', flexWrap: 'wrap', justifyContent: 'space-between', padding: 30 }}>
            <div style={{ width: windowWidth > 550 ? '45%' : '100%' }}>
                <strong>İşlem için bir soru seçiniz</strong>
                <QuestionSelect data={data} tableWidth={windowWidth > 550 ? 500 : '100%'} setSelectedQuestion={setSelectedQuestion} />
                <strong>{ShipmentType[shipmentTypeId] + "/" + QuestionType[isVehicleQuestion] + " İçin Mevcut Sorular"}</strong>
                <ShipmentTypeIndexTable data={shipmentSumData?.data?.data} isLoading={shipmentSumLoading} />
            </div>
            <div style={{ width: windowWidth > 550 ? '45%' : '100%' }}>
                {selectedQuestion ?
                    <form
                        name="questionCompleteForm"
                        noValidate
                        className="flex flex-col justify-center w-full"
                        onSubmit={handleSubmit(addQuestion)}
                    >
                        <TextField
                            className="mb-16"
                            label="Sorunun Sırası"
                            type="number"
                            value={orderIndex}
                            onChange={e => setOrderIndex(parseInt(e.target.value))}
                            min={1}
                            variant="outlined"
                            fullWidth
                        />

                        {rangeSectionActive ?
                            <FormControlLabel
                                style={{ marginBottom: 16 }}
                                control={
                                    <Checkbox
                                        value={answerRangeActive}
                                        onChange={(e) => { setAnswerRangeActive(!answerRangeActive) }}
                                        checked={answerRangeActive}
                                        color="primary"
                                    />
                                }
                                label="Cevap Aralığı"
                            /> : null}
                        {rangeSectionActive && answerRangeActive ?
                            <div>
                                <TextField
                                    className="mb-16"
                                    label="Min"
                                    type="number"
                                    value={range1}
                                    onChange={e => setRange1(parseFloat(e.target.value))}
                                    min={0}
                                    variant="outlined"
                                    fullWidth
                                />
                                <TextField
                                    className="mb-16"
                                    label="Max"
                                    type="number"
                                    value={range2}
                                    onChange={e => setRange2(parseFloat(e.target.value))}
                                    min={0}
                                    variant="outlined"
                                    fullWidth
                                />
                            </div> : null}

                        <div className="flex flex-col sm:flex-row items-center justify-center sm:justify-between" />

                        <Button
                            variant="contained"
                            color="primary"
                            className="w-full mx-auto mt-16"
                            style={{ marginBottom: 16 }}
                            aria-label="ADD SHIPMENT QUESTION"
                            disable={buttonDisable}
                            type="submit"
                        >
                            {buttonDisable ? <div><CircularProgress size={20} /> Lütfen Bekleyiniz</div> : "Soruyu Ekle"}
                        </Button>
                    </form>
                    :
                    <div>Lütfen bir soru seçiniz.</div>}
            </div>
        </div>
    )
}
