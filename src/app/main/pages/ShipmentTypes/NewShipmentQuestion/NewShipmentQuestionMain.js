import React, { useEffect, useState } from 'react';
import FusePageCarded from '@fuse/core/FusePageCarded';
import FuseLoading from '@fuse/core/FuseLoading';
import { useAuth } from 'app/main/contexts/AuthContext';
import { Redirect, useParams } from 'react-router';
import NewShipmentQuestionForm from './NewShipmentQuestionForm';
import NewShipmentQuestionHeader from './NewShipmentQuestionHeader';
import { useQuery } from 'react-query';
import APICall from 'app/api/APICall';
import { useDialog } from 'app/main/contexts/DialogContext';

export default function NewShipmentQuestionMain() {
    const { isLoggedIn, remindProcess } = useAuth();
    const { isVehicleQuestion, shipmentTypeId } = useParams();
    const { showFailureDialog } = useDialog();

    if (remindProcess) {
        return <FuseLoading />
    }

    if (!isLoggedIn) {
        return <Redirect to="/" />
    }

    const { data, isLoading, error, refetch } = useQuery('get questions for new shipment question', () => APICall.GetQuestionBank(), { cacheTime: 10000, refetchOnWindowFocus: false, retry: false });

    const [availableQuestionList, setAvailableQuestionList] = useState([]);

    useEffect(() => {
        if (data) {
            setAvailableQuestionList(data?.data?.data?.filter(question => !question.shipmentTypes.includes(parseInt(shipmentTypeId))));
        }
    }, [data])

    if (isLoading) {
        return <FuseLoading />
    }

    return (
        <div className="flex-grow overflow-x-auto">
            <FusePageCarded
                classes={{
                    content: 'flex',
                    contentCard: 'overflow-hidden',
                    header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
                }}
                header={<NewShipmentQuestionHeader isVehicleQuestion={isVehicleQuestion} shipmentTypeId={shipmentTypeId} />}
                content={<NewShipmentQuestionForm data={availableQuestionList} isVehicleQuestion={isVehicleQuestion} shipmentTypeId={shipmentTypeId} />}
                innerScroll
            />
        </div>
    )
}
