import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import Typography from '@material-ui/core/Typography';
import { motion } from 'framer-motion';
import _ from 'lodash';
import { useHistory } from 'react-router';
import { WeightUnit, Datatype, ConfirmationType, PhotoRequirementType, DatatypeTR, ShipmentType, ShipmentTypeReverse } from '../../../../enum/enums';
import DataGrid, { Column, FilterRow, HeaderFilter, Editing, Scrolling, Paging } from 'devextreme-react/data-grid';
import { Button } from 'devextreme-react/button';
import { useAuth } from 'app/main/contexts/AuthContext';
import APICall from 'app/api/APICall';
import { useDialog } from 'app/main/contexts/DialogContext';
import FuseLoading from '@fuse/core/FuseLoading';

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

export default function ShipmentTypeIndexTable({ data, error, refetch, isLoading }) {
    const classes = useStyles();
    const history = useHistory();
    const { showSuccessDialog, showFailureDialog } = useDialog();

    if (isLoading) {
        return <FuseLoading />
    }

    if (data.length === 0 || error) {
        return (
            <motion.div
                initial={{ opacity: 0 }}
                animate={{ opacity: 1, transition: { delay: 0.1 } }}
                className="flex flex-1 items-center justify-center h-full"
            >
                <Typography color="textSecondary" variant="h5">
                    {error?.response?.data?.errorDescription ? error?.response?.data?.errorDescription : "Listeyi görmek için soru tipi ve ürün tipini seçiniz."}
                </Typography>
            </motion.div>
        );
    }

    const calculateIsRequiredCell = (data) => {
        return data.question.isRequired ? "Zorunlu" : "Zorunlu Değil";
    }

    const calculateIsSpecialQuestionCell = (data) => {
        return data.question.isSpecialQuestion ? "Özel Soru" : "Özel Soru Değil";
    }

    const calculateAnswerTypeCell = (data) => {
        return DatatypeTR[data.question.answerDataTypeId];
    }

    return (
        <div className="w-full flex flex-col">
            <FuseScrollbars className="flex-grow overflow-x-auto">
                <DataGrid
                    height={300}
                    keyExpr="question.id"
                    dataSource={data}
                    showBorders={true}
                    selection={{ mode: 'single' }}
                    hoverStateEnabled={true}
                    noDataText="Soru Bulunamadı"
                    paging={false}
                    allowColumnResizing={true}
                    style={{ padding: 10 }}
                >
                    <Scrolling showScrollbar="always" mode="virtual" rowRenderingMode="virtual" />
                    <Paging defaultPageSize={20} />
                    <FilterRow visible={true} applyFilter='auto' />
                    <HeaderFilter visible={true} />
                    <Column
                        dataField="orderIndex"
                        caption="Sorunun İndeksi"
                        dataType="number"
                        width={140}
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="question.questionText"
                        caption="Soru"
                        dataType="string"
                        width={140}
                        allowEditing={false}
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="question.questionTextEN"
                        caption="Soru (İngilizce)"
                        dataType="string"
                        width={140}
                        allowEditing={false}
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="question.questionHelpText"
                        caption="Soru Yardımcı Yazı"
                        dataType="string"
                        width={180}
                        allowEditing={false}
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="question.isRequired"
                        caption="Soru Cevaplama Zorunluluğu"
                        dataType="string"
                        calculateCellValue={calculateIsRequiredCell}
                        width={140}
                        allowEditing={false}
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="question.answerDataTypeId"
                        caption="Cevap Tipi"
                        dataType="string"
                        calculateCellValue={calculateAnswerTypeCell}
                        width={140}
                        allowEditing={false}
                        cssClass="justify-table"
                    />
                </DataGrid>
            </FuseScrollbars>
        </div>
    )
}