import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import Typography from '@material-ui/core/Typography';
import { motion } from 'framer-motion';
import _ from 'lodash';
import { useHistory } from 'react-router';
import { ConfirmationType, DatatypeTR, PhotoRequirementType } from '../../../../enum/enums';
import DataGrid, { Column, FilterRow, HeaderFilter, Scrolling, Paging } from 'devextreme-react/data-grid';
import { useWindow } from 'app/main/contexts/WindowContext';

export default function QuestionSelect({ data, tableWidth, error, setSelectedQuestion }) {
    if (data.length === 0 || error) {
        return (
            <motion.div
                initial={{ opacity: 0 }}
                animate={{ opacity: 1, transition: { delay: 0.1 } }}
                className="flex flex-1 items-center justify-center h-full"
            >
                <Typography color="textSecondary" variant="h5">
                    {error?.response?.data?.errorDescription ? error?.response?.data?.errorDescription : "Soru bulunamadı!"}
                </Typography>
            </motion.div>
        );
    }

    const calculateIsRequiredCell = (data) => {
        return data.question.isRequired ? "Zorunlu" : "Zorunlu Değil";
    }

    const calculateIsSpecialQuestionCell = (data) => {
        return data.question.isSpecialQuestion ? "Özel Soru" : "Özel Soru Değil";
    }

    const calculatePhotoReqCell = (data) => {
        return PhotoRequirementType[data.question.questionPhotoRequirementId];
    }

    const calculateConfirationReqCell = (data) => {
        return ConfirmationType[data.question.isConfirmationRequired];
    }

    const changeSelectedQuestion = ({ selectedRowsData }) => {
        const data = selectedRowsData[0];
        setSelectedQuestion(data);
    }

    const calculateAnswerTypeCell = (data) => {
        return DatatypeTR[data.question.answerDataTypeId];
    }

    return (
        <div className="w-full flex flex-col mb-16">
            <DataGrid
                height={300}
                keyExpr="question.id"
                dataSource={data}
                showBorders={true}
                selection={{ mode: 'single' }}
                hoverStateEnabled={true}
                noDataText="Soru Bulunamadı"
                paging={false}
                allowColumnResizing={true}
                onSelectionChanged={changeSelectedQuestion}
            >
                <Scrolling showScrollbar="always" mode="virtual" rowRenderingMode="virtual" />
                <Paging defaultPageSize={20} />
                <FilterRow visible={true} applyFilter='auto' />
                <HeaderFilter visible={true} />
                <Column
                    dataField="question.questionText"
                    caption="Soru"
                    dataType="string"
                    minWidth={140}
                    cssClass="justify-table"
                />
                <Column
                    dataField="question.questionTextEN"
                    caption="Soru (İngilizce)"
                    dataType="string"
                    minWidth={140}
                    cssClass="justify-table"
                />
                <Column
                    dataField="question.questionHelpText"
                    caption="Soru Yardımcı Yazı"
                    dataType="string"
                    minWidth={180}
                    cssClass="justify-table"
                />
                <Column
                    dataField="question.answerDataTypeId"
                    caption="Cevap Tipi"
                    dataType="string"
                    calculateCellValue={calculateAnswerTypeCell}
                    width={140}
                    cssClass="justify-table"
                />
                <Column
                    dataField="question.isRequired"
                    caption="Soru Cevaplama Zorunluluğu"
                    dataType="string"
                    calculateCellValue={calculateIsRequiredCell}
                    minWidth={140}
                    cssClass="justify-table"
                />
                <Column
                    dataField="question.isSpecialQuestion"
                    caption="Özel Soru"
                    dataType="string"
                    calculateCellValue={calculateIsSpecialQuestionCell}
                    minWidth={140}
                    cssClass="justify-table"
                />
                <Column
                    dataField="question.questionPhotoRequirementId"
                    caption="Fotoğraf Gerekliliği"
                    dataType="string"
                    calculateCellValue={calculatePhotoReqCell}
                    minWidth={180}
                    cssClass="justify-table"
                />
                <Column
                    dataField="question.isConfirmationRequired"
                    caption="Teyit"
                    dataType="string"
                    calculateCellValue={calculateConfirationReqCell}
                    minWidth={140}
                    cssClass="justify-table"
                />
            </DataGrid>
        </div>
    )
}
