import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import Typography from '@material-ui/core/Typography';
import { motion } from 'framer-motion';
import _ from 'lodash';
import { useHistory } from 'react-router';
import { WeightUnit, Datatype, ConfirmationType, PhotoRequirementType, DatatypeTR, ShipmentType, ShipmentTypeReverse } from '../../../enum/enums';
import DataGrid, { Column, FilterRow, HeaderFilter, Editing, Scrolling, Paging } from 'devextreme-react/data-grid';
import { Button } from 'devextreme-react/button';
import { useAuth } from 'app/main/contexts/AuthContext';
import APICall from 'app/api/APICall';
import { useDialog } from 'app/main/contexts/DialogContext';
import FuseLoading from '@fuse/core/FuseLoading';
import { useWindow } from 'app/main/contexts/WindowContext';

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

export default function ShipmentTypeTable({ data, error, refetch, isLoading, shipmentTypeId, isVehicleQuestion, handleOpenConfirmDialog }) {
    const classes = useStyles();
    const history = useHistory();
    const { showSuccessDialog, showFailureDialog } = useDialog();
    const { windowWidth } = useWindow();

    if (isLoading) {
        return <FuseLoading />
    }

    if (data?.length === 0 || error) {
        return (
            <motion.div
                initial={{ opacity: 0 }}
                animate={{ opacity: 1, transition: { delay: 0.1 } }}
                className="flex flex-1 items-center justify-center h-full"
            >
                <Typography color="textSecondary" variant="h5" style={{ fontSize: windowWidth > 650 ? 20 : 15 }}>
                    {error?.response?.data?.errorDescription ? error?.response?.data?.errorDescription : "Listeyi görmek için soru tipi ve ürün tipini seçiniz."}
                </Typography>
            </motion.div>
        );
    }

    const calculateIsRequiredCell = (data) => {
        return data?.question?.isRequired ? "Zorunlu" : "Zorunlu Değil";
    }

    const calculateIsSpecialQuestionCell = (data) => {
        return data?.question?.isSpecialQuestion ? "Özel Soru" : "Özel Soru Değil";
    }

    const calculateAnswerTypeCell = (data) => {
        return DatatypeTR[data?.question?.answerDataTypeId];
    }

    const calculatePhotoReqCell = (data) => {
        return PhotoRequirementType[data?.question?.questionPhotoRequirementId];
    }

    const calculateConfirationReqCell = (data) => {
        return ConfirmationType[data?.question?.isConfirmationRequired];
    }

    const renderDeleteButton = (cellData) => {
        return (
            <div>
                <Button
                    text="Sil"
                    type="default"
                    style={{ color: 'red' }}
                    stylingMode="text"
                    onClick={() => { handleOpenConfirmDialog({ shipmentTypeId: parseInt(shipmentTypeId), isVehicleQuestion: isVehicleQuestion === 0 ? false : true, question: cellData?.data?.question }) }}
                />
            </div>
        )
    }

    const handleOrderIndexChange = async (newData, value, currentData) => {
        try {
            if (!isNaN(parseInt(value))) {
                newData.orderIndex = value;
                const requestData = {
                    shipmentTypeId: parseInt(shipmentTypeId),
                    isVehicleQuestion: isVehicleQuestion === 0 ? false : true,
                    questionId: currentData?.question?.id,
                    orderIndex: parseInt(value)
                }
                APICall.EditOrderIndexOfQuestion(requestData).then((response) => {
                    showSuccessDialog("Sorunun sırası başarıyla güncellendi.");
                    refetch();
                }).catch((err) => {
                    showFailureDialog(err?.response?.data?.errorDescription);
                });
            }
            else {
                showFailureDialog("Lütfen bir sayı giriniz.");
            }
        }
        catch (err) {
            showFailureDialog(err?.response?.data?.errorDescription ? err?.response?.data?.errorDescription : "Beklenmedik Hata");
        }
    }

    return (
        <div className="w-full flex flex-col">
            <FuseScrollbars className="flex-grow overflow-x-auto">
                <DataGrid
                    height="100%"
                    keyExpr="question.id"
                    dataSource={data}
                    showBorders={true}
                    selection={{ mode: 'single' }}
                    hoverStateEnabled={true}
                    noDataText="Soru Bulunamadı"
                    paging={false}
                    allowColumnResizing={true}
                    style={{ padding: 10 }}
                >
                    <Scrolling showScrollbar="always" mode="virtual" rowRenderingMode="virtual" />
                    <Paging defaultPageSize={20} />
                    <Editing
                        mode="cell"
                        allowUpdating={true}
                    />
                    <FilterRow visible={true} applyFilter='auto' />
                    <HeaderFilter visible={true} />
                    <Column
                        dataField="orderIndex"
                        caption="Sorunun İndeksi"
                        dataType="number"
                        width={140}
                        setCellValue={handleOrderIndexChange}
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="question.questionText"
                        caption="Soru"
                        dataType="string"
                        width={140}
                        allowEditing={false}
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="question.questionTextEN"
                        caption="Soru (İngilizce)"
                        dataType="string"
                        width={140}
                        allowEditing={false}
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="question.questionHelpText"
                        caption="Soru Yardımcı Yazı"
                        dataType="string"
                        width={180}
                        allowEditing={false}
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="question.isRequired"
                        caption="Soru Cevaplama Zorunluluğu"
                        dataType="string"
                        calculateCellValue={calculateIsRequiredCell}
                        width={140}
                        allowEditing={false}
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="question.isSpecialQuestion"
                        caption="Özel Soru"
                        dataType="string"
                        calculateCellValue={calculateIsSpecialQuestionCell}
                        width={140}
                        allowEditing={false}
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="question.answerDataTypeId"
                        caption="Cevap Tipi"
                        dataType="string"
                        calculateCellValue={calculateAnswerTypeCell}
                        width={140}
                        allowEditing={false}
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="question.questionPhotoRequirementId"
                        caption="Fotoğraf Gerekliliği"
                        dataType="string"
                        calculateCellValue={calculatePhotoReqCell}
                        width={180}
                        allowEditing={false}
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="question.isConfirmationRequired"
                        caption="Teyit"
                        dataType="string"
                        calculateCellValue={calculateConfirationReqCell}
                        width={140}
                        allowEditing={false}
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="range1"
                        caption="Min"
                        dataType="string"
                        minWidth={140}
                        allowEditing={false}
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="range2"
                        caption="Max"
                        dataType="string"
                        minWidth={180}
                        allowEditing={false}
                        cssClass="justify-table"
                    />
                    <Column
                        minWidth={140}
                        cellRender={renderDeleteButton}
                        cssClass="justify-table"
                    />
                </DataGrid>
            </FuseScrollbars>
        </div>
    )
}