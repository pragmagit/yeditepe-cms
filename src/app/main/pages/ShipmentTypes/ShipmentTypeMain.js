import FuseLoading from '@fuse/core/FuseLoading';
import { useAuth } from 'app/main/contexts/AuthContext';
import React from 'react';
import { Redirect } from 'react-router';
import ShipmentTypeList from './ShipmentTypeList';

export default function ShipmentTypeMain() {
    const { isLoggedIn, remindProcess } = useAuth();

    if (remindProcess) {
        return <FuseLoading />
    }

    if (!isLoggedIn) {
        return <Redirect to="/" />
    }

    return (
        <ShipmentTypeList />
    )
}
