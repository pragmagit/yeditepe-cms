import { lazy } from 'react';

const LogsConfig = {
    settings: {
        layout: {
            config: {
                navbar: {
                    display: true
                },
                toolbar: {
                    display: true
                },
                footer: {
                    display: false
                },
                leftSidePanel: {
                    display: false
                },
                rightSidePanel: {
                    display: false
                }
            }
        }
    },
    routes: [
        {
            path: '/pages/logs',
            component: lazy(() => import('./LogsMain'))
        }
    ]
};

export default LogsConfig;
