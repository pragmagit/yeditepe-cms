import React, { useEffect, useState } from 'react';
import FusePageCarded from '@fuse/core/FusePageCarded';
import { useQuery } from 'react-query';
import APICall from 'app/api/APICall';
import FuseLoading from '@fuse/core/FuseLoading';
import LogsHeader from './LogsHeader';
import LogsTable from './LogsTable';

export default function LogsList(props) {
    const { isLoading, error, data } = useQuery('Log List', () => APICall.GetActivityLog(), { cacheTime: 10000, refetchOnWindowFocus: false, retry: false });
    const [logList, setLogList] = useState([]);

    useEffect(() => {
        if (data) {
            setLogList(data?.data?.data);
        }
    }, [data])

    if (isLoading) {
        return <FuseLoading />
    }

    return (
        <div className="flex-grow overflow-x-auto">
            <FusePageCarded
                classes={{
                    content: 'flex',
                    contentCard: 'overflow-hidden',
                    header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
                }}
                header={<LogsHeader title="LOGLAR" />}
                content={<LogsTable data={logList} />}
                innerScroll
            />
        </div>
    )
}
