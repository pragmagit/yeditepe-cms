import FuseLoading from '@fuse/core/FuseLoading';
import { useAuth } from 'app/main/contexts/AuthContext';
import React from 'react';
import { Redirect, useParams } from 'react-router';
import LogsList from './LogsList';

export default function LogsMain() {
    const { isLoggedIn, remindProcess } = useAuth();

    if (remindProcess) {
        return <FuseLoading />
    }

    if (!isLoggedIn) {
        return <Redirect to="/" />
    }

    return (
        <LogsList />
    )
}
