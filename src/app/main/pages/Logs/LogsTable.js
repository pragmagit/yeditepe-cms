import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import Typography from '@material-ui/core/Typography';
import { motion } from 'framer-motion';
import _ from 'lodash';
import { useHistory } from 'react-router';
import DataGrid, { Column, FilterRow, HeaderFilter, Scrolling, Paging } from 'devextreme-react/data-grid';
import { Button } from 'devextreme-react/button';

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

export default function LogsTable({ data }) {
    const classes = useStyles();
    const history = useHistory();

    if (data.length === 0) {
        return (
            <motion.div
                initial={{ opacity: 0 }}
                animate={{ opacity: 1, transition: { delay: 0.1 } }}
                className="flex flex-1 items-center justify-center h-full"
            >
                <Typography color="textSecondary" variant="h5">
                    Log bulunamadı!
                </Typography>
            </motion.div>
        );
    }

    const calculateIncomingVehicleStatus = (data) => {
        return IncomingVehicleStatusTR[data.incomingVehicleStatusId];
    }

    return (
        <div className="w-full flex flex-col">
            <FuseScrollbars className="flex-grow overflow-x-auto">
                <DataGrid
                    height="100%"
                    keyExpr="id"
                    dataSource={data}
                    showBorders={true}
                    paging={false}
                    columnAutoWidth={true}
                    selection={{ mode: 'single' }}
                    hoverStateEnabled={true}
                    allowColumnResizing={true}
                    noDataText="Log Bulunamadı"
                    style={{ padding: 10 }}
                >
                    <Scrolling showScrollbar="always" mode="virtual" rowRenderingMode="virtual" />
                    <Paging defaultPageSize={20} />
                    <HeaderFilter visible={true} />
                    <Column
                        dataField="activityType.name"
                        caption="Aktivite Tipi"
                        dataType="string"
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="description"
                        caption="Log"
                        dataType="string"
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="createdOn"
                        caption="Tarih"
                        dataType="datetime"
                        cssClass="justify-table"
                    >
                        <HeaderFilter groupInterval="day" />
                    </Column>
                    <Column
                        dataField="createdFrom"
                        caption="IP"
                        dataType="string"
                        cssClass="justify-table"
                    />
                </DataGrid>
            </FuseScrollbars>
        </div>
    )
}