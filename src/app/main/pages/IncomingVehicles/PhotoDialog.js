import React from 'react';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import { DialogContent } from '@material-ui/core';

export default function PhotoDialog({ image, open, handleClose }) {
    return (
        <Dialog onClose={handleClose} aria-labelledby="answers-dialog-title" open={open} maxWidth="sm" fullWidth>
            <DialogTitle color="primary" id="answers-dialog-title">Soru Fotoğrafı</DialogTitle>
            <DialogContent dividers>
                <img alt="answer" style={{ width: '100%' }} src={image} />
            </DialogContent>
        </Dialog>
    )
}