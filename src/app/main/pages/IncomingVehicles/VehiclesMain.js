import FuseLoading from '@fuse/core/FuseLoading';
import { IncomingVehicleStatusReverse } from 'app/enum/enums';
import { useAuth } from 'app/main/contexts/AuthContext';
import React from 'react';
import { Redirect, useParams } from 'react-router';
import VehiclesList from './VehiclesList';

export default function VehiclesMain() {
    const { isLoggedIn, remindProcess } = useAuth();
    const { incomingVehicleStatus } = useParams();

    if (remindProcess) {
        return <FuseLoading />
    }

    if (!isLoggedIn) {
        return <Redirect to="/" />
    }

    return (
        <VehiclesList incomingVehicleStatus={incomingVehicleStatus} incomingVehicleStatusId={IncomingVehicleStatusReverse[incomingVehicleStatus]} />
    )
}
