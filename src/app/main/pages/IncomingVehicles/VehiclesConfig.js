import { lazy } from 'react';

const VehiclesConfig = {
    settings: {
        layout: {
            config: {
                navbar: {
                    display: true
                },
                toolbar: {
                    display: true
                },
                footer: {
                    display: false
                },
                leftSidePanel: {
                    display: false
                },
                rightSidePanel: {
                    display: false
                }
            }
        }
    },
    routes: [
        {
            path: '/pages/incoming-vehicles/:incomingVehicleStatus',
            component: lazy(() => import('./VehiclesMain'))
        },
        {
            path: '/pages/vehicle-approval/:airwaybillNo/:incomingVehicleId',
            component: lazy(() => import('./VehicleApproval/VehicleApprovalMain'))
        }
    ]
};

export default VehiclesConfig;
