import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import Typography from '@material-ui/core/Typography';
import { motion } from 'framer-motion';
import _ from 'lodash';
import { useHistory } from 'react-router';
import { Button } from 'devextreme-react/button';
import DataGrid, { Column, FilterRow, HeaderFilter, Scrolling, Paging } from 'devextreme-react/data-grid';
import DataSource from 'devextreme/data/data_source';
import ArrayStore from 'devextreme/data/array_store';
import { useQuery } from 'react-query';
import APICall from 'app/api/APICall';
import FuseLoading from '@fuse/core/FuseLoading';
import { IncomingVehicleStatusReverse, WeightUnit } from '../../../enum/enums';

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

export default function VehicleMasterDetail({ data }) {
    const { isLoading: vehicleDetailLoading, error: vehicleDetailError, data: vehicleDetailData } = useQuery(['Incoming Vehicle Detail List', data.data.id], () => APICall.GetVehicleDetails({ incomingVehicleId: data.data.id }), { cacheTime: 500000, refetchOnWindowFocus: false, retry: false });
    const [vehicleDetailList, setVehicleDetailList] = useState([]);
    const { id: incomingVehicleId, vehiclePlateNumber, incomingVehicleStatusId } = data.data; //Coming from Master Detail Component
    const history = useHistory();

    const renderImage = (cellData) => {
        return (<div><img style={{ width: 50, height: 50, objectFit: 'contain' }} src={cellData.data.cargoCompany.photoURL}></img></div>);
    }

    const calculateWeightCell = (data) => {
        return data.weight + " " + WeightUnit[data.weightUnitId];
    }

    const renderApprovalButton = (cellData) => {
        return (
            <div>
                <Button
                    text="İncele"
                    type="default"
                    stylingMode="text"
                    onClick={() => history.push('/pages/vehicle-approval/' + cellData?.data?.airwaybillNo + "/" + incomingVehicleId)}
                />
            </div>
        )
    }

    const renderPDFLink = (cellData) => {
        return cellData?.data?.pdfUrl ? <a href={cellData.data.pdfUrl}>PDF'i İndir</a> : "Pdf Mevcut Değil";
    }

    const calculatePDFLink = (data) => {
        return data.pdfUrl;
    }

    useEffect(() => {
        if (vehicleDetailData) {
            setVehicleDetailList(vehicleDetailData?.data?.data ?? []);
        }
    }, [vehicleDetailData])

    if (vehicleDetailLoading) {
        return <FuseLoading />
    }

    return (
        <React.Fragment>
            <div className="master-detail-caption">
                {`${vehiclePlateNumber} Plakalı Araç ile Eşleşen Sevkiyatlar`}
            </div>
            <DataGrid
                dataSource={vehicleDetailList}
                noDataText={"Eşleşen Sevkiyat Bulunamadı."}
                showBorders={true}
                columnAutoWidth={true}
                selection={{ mode: 'single' }}
            >
                <Scrolling showScrollbar="always" />
                <Column dataField="id" cssClass="justify-table" />
                <Column
                    width={70}
                    caption="Firma"
                    cellRender={renderImage}
                    cssClass="justify-table"
                />
                <Column dataField="airwaybillNo" cssClass="justify-table" />
                <Column
                    dataField="origin"
                    caption="Kalkış Yeri"
                    dataType="string"
                    cssClass="justify-table"
                />
                <Column
                    dataField="destination"
                    caption="Varış Yeri"
                    dataType="string"
                    cssClass="justify-table"
                />
                <Column
                    dataField="takeOffTime"
                    caption="Kalkış Zamanı"
                    dataType="datetime"
                    format="dd/MM/yyyy HH:mm"
                    cssClass="justify-table"
                />
                <Column
                    dataField="importerName"
                    caption="İthalatçı"
                    dataType="string"
                    cssClass="justify-table"
                />
                <Column
                    dataField="exporterName"
                    caption="İhracatçı"
                    dataType="string"
                    cssClass="justify-table"
                />
                <Column
                    dataField="shipmentType.name"
                    caption="Gönderi Türü"
                    dataType="string"
                    cssClass="justify-table"
                />
                <Column
                    dataField="piece"
                    caption="Kap Adeti"
                    dataType="number"
                    cssClass="justify-table"
                ></Column>
                <Column
                    dataField="weight"
                    caption="Ağırlık"
                    dataType="number"
                    calculateDisplayValue={calculateWeightCell}
                    cssClass="justify-table"
                />
                {incomingVehicleStatusId == IncomingVehicleStatusReverse["AwaitingApproval"] ?
                    <Column
                        minWidth={140}
                        cellRender={renderApprovalButton}
                        cssClass="justify-table"
                    /> : null}
                <Column
                    minWidth={120}
                    caption="PDF"
                    cellRender={renderPDFLink}
                    calculateCellValue={calculatePDFLink}
                    cssClass="justify-table"
                />
            </DataGrid>
        </React.Fragment>
    )
}