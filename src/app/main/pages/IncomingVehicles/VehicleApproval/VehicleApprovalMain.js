import FuseLoading from '@fuse/core/FuseLoading';
import { useAuth } from 'app/main/contexts/AuthContext';
import React from 'react';
import { Redirect, useParams } from 'react-router';
import VehicleApprovalList from './VehicleApprovalList';

export default function VehicleApprovalMain() {
    const { isLoggedIn, remindProcess } = useAuth();
    const { airwaybillNo, incomingVehicleId } = useParams();

    if (remindProcess) {
        return <FuseLoading />
    }

    if (!isLoggedIn) {
        return <Redirect to="/" />
    }

    return (
        <VehicleApprovalList airwaybillNo={airwaybillNo} incomingVehicleId={parseInt(incomingVehicleId)} />
    )
}
