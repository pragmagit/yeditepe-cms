import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import Typography from '@material-ui/core/Typography';
import { motion } from 'framer-motion';
import { Button } from '@material-ui/core';
import _ from 'lodash';
import { useHistory } from 'react-router';
import APICall from 'app/api/APICall';

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

const answerConverter = {
    "true": "Evet",
    "false": "Hayır"
}

const confirmConverter = {
    "true": "Onaylandı",
    "false": "Reddedildi"
}

export default function VehicleApprovalTable({ data, refetch, handleOpenPhotoDialog, handleOpenSnackbar }) {
    const classes = useStyles();
    const history = useHistory();

    const updateQuestionConfirmation = async (ApprovalId, confirmation) => {
        APICall.SetVehicleApprovalConfirmation({ ApprovalId, confirmation }).then(() => {
            handleOpenSnackbar('Onay durumu ' + confirmConverter[confirmation?.toString()] + ' olarak değiştirildi.', 'info');
            refetch();
        }).catch(() => { handleOpenSnackbar('Bir hata meydana geldi. Lütfen tekrar deneyiniz.', 'danger') })
    }

    if (data.length === 0) {
        return (
            <motion.div
                initial={{ opacity: 0 }}
                animate={{ opacity: 1, transition: { delay: 0.1 } }}
                className="flex flex-1 items-center justify-center h-full"
            >
                <Typography color="textSecondary" variant="h5">
                    Araç cevapları bulunamadı!
                </Typography>
            </motion.div>
        );
    }

    return (
        <div className="w-full flex flex-col">
            <FuseScrollbars className="flex-grow overflow-x-auto">
                <Table stickyHeader className={classes.table} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell align="left">SORU</TableCell>
                            <TableCell align="left">CEVAP</TableCell>
                            <TableCell align="left">Onay Durumu</TableCell>
                            <TableCell align="center">Fotoğraf</TableCell>
                            <TableCell align="left" />
                            <TableCell align="left" />
                            <TableCell align="left" />
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            _.orderBy(data)
                                .map(vehicleApprovalQuestion => (
                                    <TableRow hover className="h-72" key={vehicleApprovalQuestion.id}>
                                        <TableCell align="left">
                                            {vehicleApprovalQuestion?.question?.questionText}{vehicleApprovalQuestion?.question?.questionTextEN ? "/" + vehicleApprovalQuestion?.question?.questionTextEN : null}
                                            <p style={{ color: '#a0a0a0' }}>{vehicleApprovalQuestion?.question?.questionHelpText}</p>
                                        </TableCell>
                                        <TableCell align="left">{answerConverter[vehicleApprovalQuestion?.answer] ? answerConverter[vehicleApprovalQuestion?.answer] : vehicleApprovalQuestion?.answer}</TableCell>
                                        <TableCell align="left"><strong style={{ color: vehicleApprovalQuestion?.isConfirmed ? 'green' : vehicleApprovalQuestion?.isConfirmed !== null ? 'red' : 'gray' }}>{vehicleApprovalQuestion?.isConfirmed !== null ? confirmConverter[vehicleApprovalQuestion?.isConfirmed] : "Onay Bekliyor"}</strong></TableCell>
                                        <TableCell align="center">
                                            {vehicleApprovalQuestion.answerPhoto ?
                                                <Button
                                                    color="secondary"
                                                    component="label"
                                                    onClick={() => { handleOpenPhotoDialog(vehicleApprovalQuestion.answerPhoto) }}
                                                >
                                                    Görüntüle
                                                </Button> : "Fotoğraf Yok"}
                                        </TableCell>
                                        <TableCell size="small" align="right">
                                            <Button
                                                variant="contained"
                                                component="label"
                                                onClick={() => { updateQuestionConfirmation(vehicleApprovalQuestion.id, true) }}
                                            >
                                                Onayla
                                            </Button>
                                        </TableCell>
                                        <TableCell align="left">
                                            <Button
                                                variant="contained"
                                                component="label"
                                                onClick={() => { updateQuestionConfirmation(vehicleApprovalQuestion.id, false) }}
                                            >
                                                Reddet
                                            </Button>
                                        </TableCell>
                                    </TableRow>
                                ))
                        }
                    </TableBody>
                </Table>
            </FuseScrollbars>
        </div>
    )
}