import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import APICall from 'app/api/APICall';
import { useDialog } from 'app/main/contexts/DialogContext';
import { useHistory } from 'react-router';
import { CircularProgress } from '@material-ui/core';

export default function VehicleApprovalConfirmation({ open, handleClose, data }) {
    const [completeButtonDisable, setCompleteButtonDisable] = useState(false);
    const { showSuccessDialog, showFailureDialog } = useDialog();
    const history = useHistory();

    const approve = async () => {
        setCompleteButtonDisable(true);
        try {
            await APICall.AcceptVehicleApprovalRequest({ airwaybillNo: data?.airwaybillNo, incomingVehicleId: data?.incomingVehicleId });
            setCompleteButtonDisable(false);
            showSuccessDialog('Araç Kabul Onaylandı.');
            history.push('/pages/incoming-vehicles/awaitingApproval');
        }
        catch (err) {
            showFailureDialog(err?.response?.data?.errorDescription ? err?.response?.data?.errorDescription : "Beklenmeyen Bir Hata");
            setCompleteButtonDisable(false);
        }
    }

    const reject = async () => {
        setCompleteButtonDisable(true);
        try {
            await APICall.RejectVehicleApprovalRequest({ airwaybillNo: data?.airwaybillNo, incomingVehicleId: data?.incomingVehicleId });
            setCompleteButtonDisable(false);
            showSuccessDialog('Araç Kabul Reddedildi.');
            history.push('/pages/incoming-vehicles/awaitingApproval');
        }
        catch (err) {
            showFailureDialog(err?.response?.data?.errorDescription ? err?.response?.data?.errorDescription : "Beklenmeyen Bir Hata");
            setCompleteButtonDisable(false);
        }
    }

    return (
        <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">{"Araç Tamamlama Onayı"}</DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    {data?.isApproved ? "Araç ONAY alacak. Tamamlamak istiyor musunuz?" : "Araç RED alacak. Tamamlamak istiyor musunuz?"}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} style={{ backgroundColor: 'red', color: 'white' }} disabled={completeButtonDisable}>
                    İptal Et
                </Button>
                <Button onClick={data?.isApproved ? approve : reject} style={{ backgroundColor: 'green', color: 'white' }} disabled={completeButtonDisable}>
                    {completeButtonDisable ? <div><CircularProgress size={20} /> Lütfen Bekleyiniz</div> : "Tamamla"}
                </Button>
            </DialogActions>
        </Dialog>
    )
}
