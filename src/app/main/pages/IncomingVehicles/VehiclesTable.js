import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import Typography from '@material-ui/core/Typography';
import { motion } from 'framer-motion';
import _ from 'lodash';
import { useHistory } from 'react-router';
import DataGrid, { Column, FilterRow, HeaderFilter, MasterDetail, Export, Scrolling, Paging } from 'devextreme-react/data-grid';
import { Button } from 'devextreme-react/button';
import { IncomingVehicleStatus, IncomingVehicleStatusReverse, IncomingVehicleStatusTR } from 'app/enum/enums';
import VehicleMasterDetail from './VehicleMasterDetail';
import { Workbook } from 'exceljs';
import saveAs from 'file-saver';
import { exportDataGrid } from 'devextreme/excel_exporter';

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

export default function VehiclesTable({ vehicleData, incomingVehicleStatusId }) {
    const classes = useStyles();
    const history = useHistory();

    if (vehicleData?.length === 0) {
        return (
            <motion.div
                initial={{ opacity: 0 }}
                animate={{ opacity: 1, transition: { delay: 0.1 } }}
                className="flex flex-1 items-center justify-center h-full"
            >
                <Typography color="textSecondary" variant="h5">
                    Araç bulunamadı!
                </Typography>
            </motion.div>
        );
    }

    const calculateIncomingVehicleStatus = (data) => {
        return IncomingVehicleStatusTR[data.incomingVehicleStatusId];
    }

    const calculatePDFLink = (data) => {
        return data?.pdfUrl;
    }

    const renderPDFLink = (cellData) => {
        return cellData?.data?.pdfUrl ? <a href={cellData?.data?.pdfUrl}>PDF'i İndir</a> : "PDF Mevcut Değil"
    }

    const onExporting = (e) => {
        const workbook = new Workbook();
        const worksheet = workbook.addWorksheet('Main sheet');
        exportDataGrid({
            component: e.component,
            worksheet: worksheet,
            autoFilterEnabled: true,
            customizeCell: function (options) {
                const excelCell = options;
                excelCell.font = { name: 'Arial', size: 12 };
                excelCell.alignment = { horizontal: 'left' };
            }
        }).then(function () {
            workbook.xlsx.writeBuffer()
                .then(function (buffer) {
                    saveAs(new Blob([buffer], { type: 'application/octet-stream' }), `Araçlar.xlsx`);
                });
        });
        e.cancel = true;
    }

    return (
        <div className="w-full flex flex-col">
            <FuseScrollbars className="flex-grow overflow-x-auto">
                <DataGrid
                    height="100%"
                    keyExpr="id"
                    dataSource={vehicleData}
                    showBorders={true}
                    paging={false}
                    columnAutoWidth={true}
                    onExporting={onExporting}
                    selection={{ mode: 'single' }}
                    hoverStateEnabled={true}
                    allowColumnResizing={true}
                    noDataText="Araç Bulunamadı"
                    style={{ padding: 10 }}
                >
                    <Scrolling showScrollbar="always" mode="virtual" rowRenderingMode="virtual" />
                    <Paging defaultPageSize={50} />
                    <FilterRow visible={true} applyFilter='auto' />
                    <HeaderFilter visible={true} />
                    <Column
                        width={70}
                        dataField="id"
                        caption="ID"
                        dataType="string"
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="vehiclePlateNumber"
                        caption="Araç Plakası"
                        dataType="string"
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="sealNumber"
                        caption="Mühür No"
                        dataType="string"
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="memberNameSurname"
                        caption="Kabulü Yapan Kişi"
                        dataType="string"
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="date"
                        caption="Kabul Zamanı"
                        dataType="datetime"
                        format="dd/MM/yyyy HH:mm"
                        cssClass="justify-table"
                    >
                        <HeaderFilter groupInterval="day" />
                    </Column>
                    <Column
                        dataField="incomingVehicleStatusId"
                        caption="Statüsü"
                        dataType="string"
                        calculateCellValue={calculateIncomingVehicleStatus}
                        cssClass="justify-table"
                    />
                    {incomingVehicleStatusId === IncomingVehicleStatusReverse["Completed"] ?
                        <Column
                            minWidth={140}
                            caption="PDF"
                            cellRender={renderPDFLink}
                            calculateCellValue={calculatePDFLink}
                            cssClass="justify-table"
                        /> : null}
                    <MasterDetail
                        enabled={true}
                        component={VehicleMasterDetail}
                    />
                    <Export enabled={true} />
                </DataGrid>
            </FuseScrollbars>
        </div>
    )
}