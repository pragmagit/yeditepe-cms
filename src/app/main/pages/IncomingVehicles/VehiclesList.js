import React, { useEffect, useState } from 'react';
import FusePageCarded from '@fuse/core/FusePageCarded';
import { useQuery } from 'react-query';
import APICall from 'app/api/APICall';
import FuseLoading from '@fuse/core/FuseLoading';
import VehiclesHeader from './VehiclesHeader';
import VehiclesTable from './VehiclesTable';
import { IncomingVehicleStatusTR } from 'app/enum/enums';

export default function VehiclesList({ incomingVehicleStatus, incomingVehicleStatusId }) {
    const { isLoading: vehicleLoading, error: vehicleError, data: vehicleData } = useQuery('Incoming Vehicle List', () => APICall.GetIncomingVehicles(), { cacheTime: 10000, refetchOnWindowFocus: false, retry: false });
    const [vehiclesList, setVehiclesList] = useState([]);

    useEffect(() => {
        if (vehicleData) {
            console.log(incomingVehicleStatusId);
            setVehiclesList(vehicleData?.data?.data?.filter(vehicle => vehicle.incomingVehicleStatusId === incomingVehicleStatusId));
        }
    }, [vehicleData, incomingVehicleStatusId])

    if (vehicleLoading) {
        return <FuseLoading />
    }

    return (
        <div className="flex-grow overflow-x-auto">
            <FusePageCarded
                classes={{
                    content: 'flex',
                    contentCard: 'overflow-hidden',
                    header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
                }}
                header={<VehiclesHeader title={(incomingVehicleStatusId === 5 ? "Tamamlanan" : "Onay Bekleyen") + " Araçlar"} />}
                content={<VehiclesTable vehicleData={vehiclesList} incomingVehicleStatusId={incomingVehicleStatusId} />}
                innerScroll
            />
        </div>
    )
}
