import React, { useEffect, useState } from 'react';
import FusePageCarded from '@fuse/core/FusePageCarded';
import { useQuery } from 'react-query';
import APICall from 'app/api/APICall';
import FuseLoading from '@fuse/core/FuseLoading';
import { useAuth } from 'app/main/contexts/AuthContext';
import { Redirect, useParams } from 'react-router';
import { useDialog } from 'app/main/contexts/DialogContext';
import EditStaffHeader from './EditStaffHeader';
import EditStaffForm from './EditStaffForm';

export default function EditStaffMain() {
    const { isLoggedIn, remindProcess } = useAuth();
    const { staffId } = useParams();
    const { showFailureDialog } = useDialog();

    if (remindProcess) {
        return <FuseLoading />
    }

    if (!isLoggedIn) {
        return <Redirect to="/" />
    }

    const { data, error, isLoading } = useQuery('Get Personnel By Id', () => APICall.GetPersonnelById(parseInt(staffId)), { cacheTime: 10000, refetchOnWindowFocus: false, retry: false });
    const { isLoading: isRolesLoading, error: roleError, data: roleData } = useQuery('Role List', () => APICall.GetRoles(), { cacheTime: 10000, refetchOnWindowFocus: false, retry: false });
    const { isLoading: isClaimsLoading, error: claimError, data: claimData } = useQuery('Claim List', () => APICall.GetClaims(), { cacheTime: 10000, refetchOnWindowFocus: false, retry: false });

    if (isLoading || isRolesLoading || isClaimsLoading) {
        return <FuseLoading />
    }

    if (error || roleError || claimError) {
        showFailureDialog(error?.response?.data?.errorDescription);
        return <Redirect to="/pages/staff" />;
    }

    return (
        <div className="flex-grow overflow-x-auto">
            <FusePageCarded
                classes={{
                    content: 'flex',
                    contentCard: 'overflow-hidden',
                    header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
                }}
                header={<EditStaffHeader title="Personel Güncelle" />}
                content={<EditStaffForm data={data?.data?.data} staffId={staffId} error={error} roleData={roleData?.data?.data} claimData={claimData?.data?.data} />}
                innerScroll
            />
        </div>
    )
}
