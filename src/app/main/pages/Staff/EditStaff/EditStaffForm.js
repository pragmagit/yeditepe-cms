import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import { motion } from 'framer-motion';
import { Controller, useForm } from 'react-hook-form';
import { Button, Checkbox, CircularProgress, FormControlLabel } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import _ from '@lodash';
import APICall from 'app/api/APICall';
import { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { useDialog } from 'app/main/contexts/DialogContext';
import { FormControl, Select, InputLabel, MenuItem, FormGroup } from '@material-ui/core';
import { useWindow } from 'app/main/contexts/WindowContext';
import { formatPhoneNumber } from 'app/utils/Util';

const useStyles = makeStyles(theme => ({
    root: {},
    board: {
        cursor: 'pointer',
        transitionProperty: 'box-shadow border-color',
        transitionDuration: theme.transitions.duration.short,
        transitionTimingFunction: theme.transitions.easing.easeInOut
    },
    newBoard: {},
}));

const schema = yup.object().shape({
    nameSurname: yup.string().required('Boş bırakılamaz.'),
    email: yup.string().email('Emaili doğru formatta giriniz.').required('Boş bırakılamaz.')
});

export default function EditStaffForm(props) {
    const classes = useStyles(props);
    const { data, staffId, error, roleData, roleError, claimData } = props;
    const history = useHistory();
    const { showSuccessDialog, showFailureDialog } = useDialog();
    const { windowWidth } = useWindow();
    const [disabledButton, setDisabledButton] = useState(false);
    const [selectedRoleId, setSelectedRoleId] = useState(null);
    const [phoneNumber, setPhoneNumber] = useState('');
    const [userClaimList, setUserClaimList] = useState(data.operationClaims ? data.operationClaims.map(claim => claim.id) : []);

    const defaultValues = {
        nameSurname: data.nameSurname,
        email: data.email
    }

    const { control, formState, handleSubmit, reset } = useForm({
        mode: 'onChange',
        defaultValues,
        resolver: yupResolver(schema)
    });

    const { isValid, dirtyFields, errors } = formState;

    if (data?.length < 0 || error || roleError) {
        return (
            <motion.div
                initial={{ opacity: 0 }}
                animate={{ opacity: 1, transition: { delay: 0.1 } }}
                className="flex flex-1 items-center justify-center h-full"
            >
                <Typography color="textSecondary" variant="h5">
                    {error?.response?.data?.errorDescription ? error?.response?.data?.errorDescription : "Personel bulunamadı!"}
                </Typography>
            </motion.div>
        )
    }

    const editPersonnel = async (data) => {
        setDisabledButton(true);
        try {
            if (selectedRoleId) {
                data.id = parseInt(staffId);
                data.phoneNumber = phoneNumber;
                data.roleId = selectedRoleId;
                data.claimIds = selectedRoleId === searchRole("Yeditepe Admin") ? userClaimList : [];
                await APICall.EditMember(data);
                setDisabledButton(false);
                showSuccessDialog("Personel Başarıyla Güncellendi");
                history.push('/pages/staff');
            }
            else {
                showFailureDialog("Bir rol seçiniz");
                setDisabledButton(false);
            }
        }
        catch (err) {
            showFailureDialog(err?.response?.data?.errorDescription ? err?.response?.data?.errorDescription : "Beklenmeyen Hata");
            setDisabledButton(false);
        }
    }

    function searchRole(nameKey) {
        for (var i = 0; i < roleData.length; i++) {
            if (roleData[i].name === nameKey) {
                return roleData[i].id;
            }
        }
    }

    const handlePhoneChange = ({ target: { value } }) => {
        setPhoneNumber(prevNumber => formatPhoneNumber(value, prevNumber));
    }

    const handleChangeCheckbox = (event, claim) => {
        const claimId = parseInt(event.target.value);
        const isChecked = userClaimList.includes(claimId);
        if (isChecked) {
            setUserClaimList(userClaimList.filter(id => id !== claimId));
        }
        else {
            setUserClaimList([...userClaimList, claimId]);
        }
    }

    useEffect(() => {
        if (data && roleData) {
            setPhoneNumber(formatPhoneNumber(data.phoneNumber));
            setSelectedRoleId(searchRole(data.role));
            setUserClaimList(data.operationClaims ? data.operationClaims.map(claim => claim.id) : []);
            reset({
                nameSurname: data.nameSurname,
                email: data.email
            })
        }
    }, [data, roleData])

    return (
        <div className={clsx(classes.root, 'flex flex-grow flex-shrink-0 flex-col items-center p-16 md:p-24')}>
            <div style={{ width: windowWidth > 550 ? 500 : '100%' }}>
                <form
                    name="personnelForm"
                    noValidate
                    className="flex flex-col justify-center w-full"
                    onSubmit={handleSubmit(editPersonnel)}
                >
                    <Controller
                        name="nameSurname"
                        control={control}
                        render={({ field }) => (
                            <TextField
                                {...field}
                                className="mb-16"
                                style={{ marginBottom: 16 }}
                                label="İsim Soyisim"
                                type="text"
                                variant="outlined"
                                error={!!errors.nameSurname}
                                helperText={errors?.nameSurname?.message}
                                required
                                fullWidth
                            />
                        )}
                    />

                    <Controller
                        name="email"
                        control={control}
                        render={({ field }) => (
                            <TextField
                                {...field}
                                className="mb-16"
                                style={{ marginBottom: 16 }}
                                label="Email"
                                type="text"
                                variant="outlined"
                                error={!!errors.email}
                                helperText={errors?.email?.message}
                                required
                                fullWidth
                            />
                        )}
                    />

                    <TextField
                        className="mb-16"
                        style={{ marginBottom: 16 }}
                        label="Telefon Numarası"
                        type="phone"
                        placeholder="0xxx xxx xx xx"
                        variant="outlined"
                        value={phoneNumber}
                        onChange={handlePhoneChange}
                        required
                        fullWidth
                    />

                    <FormControl variant="standard" style={{ marginBottom: 16 }}>
                        <InputLabel id="roleId-simple-select-label" shrink>Rol</InputLabel>
                        <Select
                            value={selectedRoleId}
                            onChange={(e) => setSelectedRoleId(e.target.value)}
                            labelId="roleId-simple-select-label"
                            id="roleId-simple-select"
                            label="Rol"
                            required
                        >
                            <MenuItem disabled value={null}>
                                <em>Seçiniz...</em>
                            </MenuItem>
                            {roleData?.map(role => <MenuItem value={role.id}>{role.name}</MenuItem>)}
                        </Select>
                    </FormControl>

                    {selectedRoleId === searchRole("Yeditepe Admin") ?
                        <FormControl component="fieldset" style={{ width: '100%', marginBottom: 16 }}>
                            <InputLabel id="roleId-simple-select-label" shrink style={{ marginBottom: 16 }}>Yetkiler</InputLabel>
                            <FormGroup style={{
                                display: 'flex', flexDirection: 'row', flexWrap: 'wrap', marginTop: 16
                            }}>
                                {claimData?.map(claim =>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                value={claim.id}
                                                onChange={e => handleChangeCheckbox(e, claim)}
                                                checked={userClaimList.includes(parseInt(claim.id))}
                                                name={claim.name}
                                                color="primary"
                                            />
                                        }
                                        label={claim.name}
                                        name={claim.name}
                                        style={{ width: '45%' }}
                                    />)}
                            </FormGroup>
                        </FormControl> : null}

                    <div className="flex flex-col sm:flex-row items-center justify-center sm:justify-between" />

                    <Button
                        variant="contained"
                        color="primary"
                        disabled={!isValid || disabledButton}
                        className="w-full mx-auto mt-16"
                        aria-label="EDIT PERSONNEL"
                        type="submit"
                    >
                        {disabledButton ? <div><CircularProgress size={20} /> Güncelleniyor</div> : "Düzenle"}
                    </Button>
                </form>
            </div>
        </div>
    )
}
