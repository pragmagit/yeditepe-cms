import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import Typography from '@material-ui/core/Typography';
import { motion } from 'framer-motion';
import _ from 'lodash';
import { useHistory } from 'react-router';
import DataGrid, { Column, FilterRow, HeaderFilter, Export, Scrolling, Paging } from 'devextreme-react/data-grid';
import { Button } from 'devextreme-react/button';
import { useAuth } from 'app/main/contexts/AuthContext';
import { Workbook } from 'exceljs';
import saveAs from 'file-saver';
import { exportDataGrid } from 'devextreme/excel_exporter';
import APICall from 'app/api/APICall';
import { useDialog } from 'app/main/contexts/DialogContext';

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

export default function StaffTable({ data, error, refetch }) {
    const classes = useStyles();
    const history = useHistory();
    const { showSuccessDialog, showFailureDialog } = useDialog();

    if (data?.length === 0 || error) {
        return (
            <motion.div
                initial={{ opacity: 0 }}
                animate={{ opacity: 1, transition: { delay: 0.1 } }}
                className="flex flex-1 items-center justify-center h-full"
            >
                <Typography color="textSecondary" variant="h5">
                    {error?.response?.data?.errorDescription ? error?.response?.data?.errorDescription : "Personel bulunamadı!"}
                </Typography>
            </motion.div>
        );
    }

    const renderEditButton = (cellData) => {
        return (
            <div>
                <Button
                    text="Düzenle"
                    type="default"
                    stylingMode="text"
                    onClick={() => history.push('/pages/edit-staff/' + cellData.data.id)}
                />
            </div>
        )
    }

    const renderBanButton = (cellData) => {
        return (<div>
            <Button
                text={cellData?.data?.isActive ? "Hesabı devre dışı bırak" : "Hesabın Engelini Kaldır"}
                type="default"
                style={{ color: cellData?.data?.isActive ? 'red' : 'green' }}
                stylingMode="text"
                onClick={() => {
                    APICall.ChangeActivationOfUser({ memberId: cellData?.data?.id }).then(response => {
                        showSuccessDialog(cellData?.data?.isActive ? "Kullanıcı başarıyla devre dışı bırakıldı." : "Kullanıcı başarılıyla aktif edildi.");
                        refetch();
                    }).catch();
                }}
            />
        </div>
        )
    }

    const onExporting = (e) => {
        const workbook = new Workbook();
        const worksheet = workbook.addWorksheet('Main sheet');
        exportDataGrid({
            component: e.component,
            worksheet: worksheet,
            autoFilterEnabled: true,
            customizeCell: function (options) {
                const excelCell = options;
                excelCell.font = { name: 'Arial', size: 12 };
                excelCell.alignment = { horizontal: 'left' };
            }
        }).then(function () {
            workbook.xlsx.writeBuffer()
                .then(function (buffer) {
                    saveAs(new Blob([buffer], { type: 'application/octet-stream' }), `Personeller.xlsx`);
                });
        });
        e.cancel = true;
    }

    return (
        <div className="w-full flex flex-col">
            <FuseScrollbars className="flex-grow overflow-x-auto">
                <DataGrid
                    height="100%"
                    keyExpr="id"
                    dataSource={data}
                    showBorders={true}
                    noDataText="Personel Bulunamadı"
                    onExporting={onExporting}
                    selection={{ mode: 'single' }}
                    hoverStateEnabled={true}
                    allowColumnResizing={true}
                    paging={false}
                    style={{ padding: 10 }}
                >
                    <Scrolling showScrollbar="always" mode="virtual" rowRenderingMode="virtual" />
                    <Paging defaultPageSize={20} />
                    <FilterRow visible={true} applyFilter='auto' />
                    <HeaderFilter visible={true} />
                    <Column
                        dataField="id"
                        caption="ID"
                        dataType="number"
                        cssClass="justify-table"
                        minWidth={120}
                    />
                    <Column
                        dataField="nameSurname"
                        caption="İsim Soyisim"
                        dataType="string"
                        cssClass="justify-table"
                        minWidth={120}
                    />
                    <Column
                        dataField="email"
                        caption="Email"
                        dataType="string"
                        cssClass="justify-table"
                        minWidth={120}
                    />
                    <Column
                        dataField="phoneNumber"
                        caption="Telefon Numarası"
                        dataType="string"
                        cssClass="justify-table"
                        minWidth={120}
                    />
                    <Column
                        dataField="role"
                        caption="Rol"
                        dataType="string"
                        cssClass="justify-table"
                        minWidth={120}
                    />
                    <Column
                        cellRender={renderEditButton}
                        cssClass="justify-table"
                        minWidth={120}
                    />
                    <Column
                        cellRender={renderBanButton}
                        cssClass="justify-table"
                        minWidth={200}
                    />
                    <Export enabled={true} />
                </DataGrid>
            </FuseScrollbars>
        </div>
    )
}