import FuseLoading from '@fuse/core/FuseLoading';
import { useAuth } from 'app/main/contexts/AuthContext';
import React from 'react';
import { Redirect } from 'react-router';
import StaffList from './StaffList';

export default function StaffMain() {
    const { isLoggedIn, remindProcess } = useAuth();

    if (remindProcess) {
        return <FuseLoading />
    }

    if (!isLoggedIn) {
        return <Redirect to="/" />
    }

    return (
        <StaffList />
    )
}
