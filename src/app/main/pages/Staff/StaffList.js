import React, { useEffect, useState } from 'react';
import FusePageCarded from '@fuse/core/FusePageCarded';
import { useQuery } from 'react-query';
import APICall from 'app/api/APICall';
import FuseLoading from '@fuse/core/FuseLoading';
import StaffTable from './StaffTable';
import StaffHeader from './StaffHeader';
import StaffAddDialog from './StaffAddDialog';

export default function StaffList() {
    const { isLoading, error, data, refetch } = useQuery('Person List', () => APICall.GetPersonnels(), { cacheTime: 10000, refetchOnWindowFocus: false, retry: false });
    const { isLoading: isRolesLoading, error: roleError, data: roleData } = useQuery('Role List', () => APICall.GetRoles(), { cacheTime: 10000, refetchOnWindowFocus: false, retry: false });
    const { isLoading: isClaimsLoading, error: claimError, data: claimData } = useQuery('Claim List', () => APICall.GetClaims(), { cacheTime: 10000, refetchOnWindowFocus: false, retry: false });
    const [openAddStaffDialog, setOpenAddStaffDialog] = useState(false);

    const handleOpenDialog = () => {
        setOpenAddStaffDialog(true);
    }

    const handleCloseDialog = () => {
        setOpenAddStaffDialog(false);
    }

    if (isLoading || isRolesLoading || isClaimsLoading) {
        return <FuseLoading />
    }

    if (error) {
        //Error status and error message
    }

    return (
        <div className="flex-grow overflow-x-auto">
            <FusePageCarded
                classes={{
                    content: 'flex',
                    contentCard: 'overflow-hidden',
                    header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
                }}
                header={<StaffHeader title="Personeller" handleOpenDialog={handleOpenDialog} />}
                content={<StaffTable data={data?.data?.data} error={error} roleData={roleData?.data?.data} refetch={refetch} />}
                innerScroll
            />
            <StaffAddDialog open={openAddStaffDialog} handleClose={handleCloseDialog} refetch={refetch} roleData={roleData?.data?.data} claimData={claimData?.data?.data} />
        </div>
    )
}
