import { lazy } from 'react';
import StaffMain from './StaffMain';
import EditStaffMain from './EditStaff/EditStaffMain';

const StaffConfig = {
    settings: {
        layout: {
            config: {
                navbar: {
                    display: true
                },
                toolbar: {
                    display: true
                },
                footer: {
                    display: false
                },
                leftSidePanel: {
                    display: false
                },
                rightSidePanel: {
                    display: false
                }
            }
        }
    },
    routes: [
        {
            path: '/pages/staff',
            component: StaffMain
        },
        {
            path: '/pages/edit-staff/:staffId',
            component: EditStaffMain
        }
    ]
};

export default StaffConfig;
