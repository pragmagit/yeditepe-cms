import { Controller, useForm } from 'react-hook-form';
import { Button, Checkbox, CircularProgress } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import _ from '@lodash';
import APICall from 'app/api/APICall';
import { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { useDialog } from 'app/main/contexts/DialogContext';
import { FormControl, Select, InputLabel, MenuItem, FormGroup, Dialog, DialogTitle, DialogContent, FormControlLabel } from '@material-ui/core';
import { formatPhoneNumber } from '../../../utils/Util';

const schema = yup.object().shape({
    username: yup.string().required('Boş bırakılamaz.'),
    password: yup.string().min(6, 'En az 7 haneli bir şifre giriniz.').required('Boş bırakılamaz.'),
    email: yup.string().email('Emaili doğru formatta giriniz.').required('Boş bırakılamaz.')
});

export default function StaffAddDialog({ open, handleClose, refetch, roleData, claimData }) {
    const { showSuccessDialog, showFailureDialog } = useDialog();
    const [disabledButton, setDisabledButton] = useState(false);
    const [selectedRoleId, setSelectedRoleId] = useState(null);
    const [phoneNumber, setPhoneNumber] = useState('');
    const [userClaimList, setUserClaimList] = useState([]);

    const defaultValues = {
        username: '',
        password: '',
        email: ''
    }

    const { control, formState, handleSubmit, reset } = useForm({
        mode: 'onChange',
        defaultValues,
        resolver: yupResolver(schema)
    });

    const { isValid, dirtyFields, errors } = formState;

    const addPersonnel = async (data) => {
        setDisabledButton(true);
        try {
            if (phoneNumber && phoneNumber.length > 13) {
                if (selectedRoleId) {
                    data.phoneNumber = phoneNumber;
                    data.roleId = selectedRoleId;
                    data.operationClaimIds = selectedRoleId === searchRole("Yeditepe Admin") ? userClaimList : [];
                    await APICall.RegisterPersonel(data);
                    setDisabledButton(false);
                    showSuccessDialog("Personel Başarıyla Eklendi");
                    refetch();
                    handleClose();
                }
                else {
                    showFailureDialog("Bir rol seçiniz");
                    setDisabledButton(false);
                }
            }
            else {
                showFailureDialog("Telefon numarasını doğru giriniz.");
                setDisabledButton(false);
            }
        }
        catch (err) {
            showFailureDialog(err?.response?.data?.errorDescription ? err?.response?.data?.errorDescription : "Beklenmeyen Hata");
            setDisabledButton(false);
        }
    }

    const handlePhoneChange = ({ target: { value } }) => {
        setPhoneNumber(prevNumber => formatPhoneNumber(value, prevNumber));
    }

    function searchRole(nameKey) {
        for (var i = 0; i < roleData?.length; i++) {
            if (roleData[i].name === nameKey) {
                return roleData[i].id;
            }
        }
    }

    const handleChangeCheckbox = (event, claim) => {
        const claimId = parseInt(event.target.value);
        const isChecked = userClaimList.includes(claimId);
        if (isChecked) {
            setUserClaimList(userClaimList.filter(id => id !== claimId));
        }
        else {
            setUserClaimList([...userClaimList, claimId]);
        }
    }

    return (
        <Dialog onClose={handleClose} aria-labelledby="answers-dialog-title" open={open} maxWidth="sm" fullWidth>
            <DialogTitle color="primary" id="answers-dialog-title">Yeni Personel Kaydı</DialogTitle>
            <DialogContent dividers>
                <form
                    name="personnelForm"
                    noValidate
                    className="flex flex-col justify-center w-full"
                    onSubmit={handleSubmit(addPersonnel)}
                >
                    <Controller
                        name="username"
                        control={control}
                        render={({ field }) => (
                            <TextField
                                {...field}
                                className="mb-16"
                                style={{ marginBottom: 16 }}
                                label="İsim Soyisim"
                                type="text"
                                variant="outlined"
                                error={!!errors.username}
                                helperText={errors?.username?.message}
                                required
                                fullWidth
                            />
                        )}
                    />

                    <Controller
                        name="password"
                        control={control}
                        render={({ field }) => (
                            <TextField
                                {...field}
                                className="mb-16"
                                style={{ marginBottom: 16 }}
                                label="Parola"
                                type="password"
                                variant="outlined"
                                error={!!errors.password}
                                helperText={errors?.password?.message}
                                required
                                fullWidth
                            />
                        )}
                    />

                    <Controller
                        name="email"
                        control={control}
                        render={({ field }) => (
                            <TextField
                                {...field}
                                className="mb-16"
                                style={{ marginBottom: 16 }}
                                label="Email"
                                type="text"
                                variant="outlined"
                                error={!!errors.email}
                                helperText={errors?.email?.message}
                                required
                                fullWidth
                            />
                        )}
                    />

                    <TextField
                        className="mb-16"
                        style={{ marginBottom: 16 }}
                        label="Telefon Numarası"
                        type="phone"
                        placeholder="0xxx xxx xx xx"
                        variant="outlined"
                        value={phoneNumber}
                        onChange={handlePhoneChange}
                        required
                        fullWidth
                    />

                    <FormControl variant="standard" style={{ marginBottom: 16 }}>
                        <InputLabel id="roleId-simple-select-label" shrink>Rol</InputLabel>
                        <Select
                            value={selectedRoleId}
                            onChange={(e) => setSelectedRoleId(e.target.value)}
                            labelId="roleId-simple-select-label"
                            id="roleId-simple-select"
                            label="Rol"
                            required
                        >
                            <MenuItem disabled value={null}>
                                <em>Rol Seçiniz...</em>
                            </MenuItem>
                            {roleData?.map(role => <MenuItem value={role.id}>{role.name}</MenuItem>)}
                        </Select>
                    </FormControl>

                    {selectedRoleId === searchRole("Yeditepe Admin") ?
                        <FormControl component="fieldset" style={{ width: '100%', marginBottom: 16 }}>
                            <InputLabel id="roleId-simple-select-label" shrink style={{ marginBottom: 16 }}>Yetkiler</InputLabel>
                            <FormGroup style={{
                                display: 'flex', flexDirection: 'row', flexWrap: 'wrap', marginTop: 16
                            }}>
                                {claimData?.map(claim =>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                value={claim.id}
                                                onChange={e => handleChangeCheckbox(e, claim)}
                                                checked={userClaimList.includes(parseInt(claim.id))}
                                                name={claim.name}
                                                color="primary"
                                            />
                                        }
                                        label={claim.name}
                                        name={claim.name}
                                        style={{ width: '45%' }}
                                    />)}
                            </FormGroup>
                        </FormControl> : null}

                    <div className="flex flex-col sm:flex-row items-center justify-center sm:justify-between" />

                    <Button
                        variant="contained"
                        color="primary"
                        disabled={_.isEmpty(dirtyFields) || !isValid || disabledButton}
                        className="w-full mx-auto mt-16"
                        aria-label="NEW PERSONNEL"
                        type="submit"
                    >
                        {disabledButton ? <div><CircularProgress size={20} /> Bekleyiniz</div> : "Personel Ekle"}
                    </Button>

                    <Button
                        variant="contained"
                        style={{ backgroundColor: 'red', color: 'white' }}
                        className="w-full mx-auto mt-16"
                        aria-label="CANCEL"
                        onClick={handleClose}
                    >
                        Vazgeç
                    </Button>
                </form>
            </DialogContent>
        </Dialog>
    )
}
