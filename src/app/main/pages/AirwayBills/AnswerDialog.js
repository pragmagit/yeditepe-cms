import React, { useState, useEffect } from 'react';
import TextField from '@material-ui/core/TextField';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import { Button, DialogActions, DialogContent } from '@material-ui/core';
import APICall from 'app/api/APICall';
import { DatatypeReverse } from 'app/enum/enums';
import FuseLoading from '@fuse/core/FuseLoading';
import { useQuery } from 'react-query';
import { useWindow } from 'app/main/contexts/WindowContext';
import moment from 'moment';
import PhotoDialog from './PhotoDialog';

const answerConverter = {
    'true': 'Evet',
    'false': 'Hayır'
}

export default function AnswerDialog({ selectedAirwaybillData, open, close }) {
    const { windowWidth } = useWindow();
    const [queryCriteria, setQueryCriteria] = useState(false);
    const [openPhotoDialog, setOpenPhotoDialog] = useState(false);
    const [dialogImage, setDialogImage] = useState(null);

    const handleOpenPhotoDialog = (data) => {
        setDialogImage(data);
        setOpenPhotoDialog(true);
    }

    const handleClosePhotoDialog = () => {
        setOpenPhotoDialog(false);
    }
    const [answerList, setAnswerList] = useState([]);
    const { isLoading, error, data, refetch } = useQuery(['Completed Airwaybill Answers Read Only', queryCriteria],
        () => APICall.GetAnswersByAirwaybilId(parseInt(selectedAirwaybillData?.airwaybillId)),
        { cacheTime: 10000, refetchOnWindowFocus: false, retry: false, enabled: queryCriteria ? true : false });

    useEffect(() => {
        setQueryCriteria(selectedAirwaybillData?.airwaybillId ? selectedAirwaybillData?.airwaybillId : false);
    }, [selectedAirwaybillData?.airwaybillId])

    return (
        <Dialog onClose={close} aria-labelledby="answers-dialog-title" open={open} maxWidth="sm" fullWidth disableBackdropClick={true}>
            <DialogTitle color="primary" id="answers-dialog-title">Konşimento: {selectedAirwaybillData?.airwaybillNo}</DialogTitle>
            <DialogContent dividers>
                <div style={{ width: '100%', display: 'flex', flexWrap: 'wrap', justifyContent: 'space-between' }}>
                    {data ? data?.data?.data?.map(d =>
                        <div style={{ width: '100%' }} >
                            <h4>{d?.question?.questionText}</h4>
                            <h6 style={{ color: '#a0a0a0' }}>{d?.question?.questionHelpText}</h6>
                            {d?.question?.answerDataTypeId === DatatypeReverse["Date"] ?
                                <TextField
                                    className="mb-16"
                                    defaultValue={moment(new Date(d.answer)).format('YYYY-MM-DDThh:mm')}
                                    style={{ marginBottom: 16 }}
                                    type="datetime-local"
                                    variant="outlined"
                                    fullWidth
                                    disabled
                                /> :
                                d?.question?.answerDataTypeId === DatatypeReverse["Photo"] ?
                                    null :
                                    <TextField
                                        className="mb-16"
                                        defaultValue={d?.question?.answerDataTypeId === DatatypeReverse["Boolean"]
                                            || d?.question?.answerDataTypeId === DatatypeReverse["Piece"]
                                            || d?.question?.answerDataTypeId === DatatypeReverse["Destination"] ? answerConverter[d.answer] : d.answer}
                                        style={{ marginBottom: 16 }}
                                        variant="outlined"
                                        fullWidth
                                        disabled
                                    />}
                            {d?.answerPhoto ?
                                <img alt="answer" style={{ width: 100, height: 100, objectFit: 'contain', marginBottom: 50 }} src={d?.answerPhoto} onClick={() => { handleOpenPhotoDialog(d?.answerPhoto) }} /> : null}
                        </div>) : <FuseLoading />}
                </div>
            </DialogContent>
            <DialogActions>
                <Button onClick={close} color="primary">
                    Kapat
                </Button>
            </DialogActions>
            <PhotoDialog image={dialogImage} open={openPhotoDialog} handleClose={handleClosePhotoDialog} />
        </Dialog>
    )
}