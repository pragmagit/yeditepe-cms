import FuseLoading from '@fuse/core/FuseLoading';
import { Button, Checkbox, CircularProgress, FormControl, FormControlLabel, FormGroup, MenuItem, Select, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import APICall from 'app/api/APICall';
import { DatatypeReverse } from 'app/enum/enums';
import { useDialog } from 'app/main/contexts/DialogContext';
import { useWindow } from 'app/main/contexts/WindowContext';
import { motion } from 'framer-motion';
import moment from 'moment';
import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router';
import AirwayBillDetail from '../AirwayBillDetail';
import PhotoDialog from '../PhotoDialog';

const useStyles = makeStyles(theme => ({
    root: {},
    board: {
        cursor: 'pointer',
        transitionProperty: 'box-shadow border-color',
        transitionDuration: theme.transitions.duration.short,
        transitionTimingFunction: theme.transitions.easing.easeInOut
    },
    newBoard: {},
}));

const answerConverter = {
    'true': 'Evet',
    'false': 'Hayır'
}

const answerConverterReverse = {
    'Evet': 'true',
    'Hayır': 'false'
}

export default function EditAirwaybillForm(props) {
    const classes = useStyles(props);
    const { windowWidth } = useWindow();
    const { data, airwaybillDetailData, refetch, airwaybillId } = props;
    const history = useHistory();
    const { showSuccessDialog, showFailureDialog } = useDialog();
    const [openPhotoDialog, setOpenPhotoDialog] = useState(false);
    const [dialogImage, setDialogImage] = useState(null);
    const [disabledButton, setDisableButton] = useState(false);
    const [changedAnswers, setChangedAnswers] = useState([]);
    var timer = null;

    const handleOpenPhotoDialog = (data) => {
        setDialogImage(data);
        setOpenPhotoDialog(true);
    }

    const handleClosePhotoDialog = () => {
        setOpenPhotoDialog(false);
    }

    const { control, formState, handleSubmit, reset } = useForm();

    useEffect(() => {
        if (data.length > 0) {
            setChangedAnswers(data?.map(d => {
                return {
                    questionId: d.question.id,
                    value: d.answer,
                    photo: d.answerPhoto,
                    dataTypeId: d.question.answerDataTypeId
                }
            }))
        }
        else {
            setChangedAnswers([]);
        }
    }, [data])

    if (data?.length === 0) {
        return (
            <motion.div
                initial={{ opacity: 0 }}
                animate={{ opacity: 1, transition: { delay: 0.1 } }}
                className="flex flex-1 items-center justify-center h-full"
            >
                <Typography color="textSecondary" variant="h5">
                    Düzenlemek istediğiniz soru bulunamadı!
                </Typography>
            </motion.div>
        )
    }

    const handleChangeCheckbox = (e, data) => {
        const value = e.target.value;
        var newValueObj = changedAnswers.find(answer => answer.questionId === data.question.id);
        var valueArray = newValueObj.value.split(', ');
        if (valueArray.includes(value)) {
            valueArray = valueArray.filter(val => val !== value);
        }
        else {
            valueArray.push(value);
        }
        valueArray = valueArray.filter(val => val !== "");
        newValueObj.value = valueArray.join(', ');
        setChangedAnswers(changedAnswers.map(answer => answer.questionId !== data.question.id ? answer : newValueObj));
    }

    const addChangedValue = (e, data) => {
        clearTimeout(timer);    //Clear previous array operations

        const value = e.target.value;
        const newValue = {
            questionId: data.question.id,
            photo: data.answerPhoto,
            dataTypeId: data.question.answerDataTypeId,
            value
        }

        //Change answer array when user stopped typing
        timer = setTimeout(() => {
            setChangedAnswers(changedAnswers.map(answer => answer.questionId !== newValue.questionId ? answer : newValue));
        }, 200)
    }

    const editAirwayBill = async (formResult) => {
        setDisableButton(true);

        //Validation
        const questionList = data?.map(d => d.question);
        const isValid = changedAnswers.every(answer => {
            const question = questionList.find(question => question.id === answer.questionId);
            if (question.isRequired && answer.value === "") {
                showFailureDialog("Lütfen " + question.questionText + " kısmını doldurunuz.");
                return false;
            }
            return true;
        });

        if (isValid) {
            const requestData = {
                airwaybillId: parseInt(airwaybillId),
                answers: changedAnswers
            }

            try {

                const { data: airwaybill } = await APICall.GetAirwaybillByID(parseInt(airwaybillId));

                //Check if existing airwaybill has a PDF
                if (airwaybill?.data?.pdfUrl) {
                    await APICall.EditAnswersOfCompletedAirwaybill(requestData);
                    showSuccessDialog('Sevkiyat başarıyla güncellendi. PDF yeniden oluşturuldu.');
                    setDisableButton(false);
                    history.push('/pages/airwaybills/completed');
                }
                else {
                    showFailureDialog('Bu sevkiyata ait bir PDF şuan oluşturuluyor. Lütfen bekleyiniz.');
                    setDisableButton(false);
                }
            }
            catch (err) {
                showFailureDialog(err?.response?.data?.errorDescription ? err?.response?.data?.errorDescription : "Beklenmeyen Bir Hata");
                setDisableButton(false);
            }
        }
        setDisableButton(false);
    }

    if (changedAnswers.length === 0) {
        return <FuseLoading />
    }

    return (
        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
            <div style={{ width: '90%' }}>
                <AirwayBillDetail data={airwaybillDetailData} />
            </div>
            <div style={{ width: '90%' }}>
                <form
                    name="answerForm"
                    noValidate
                    style={{ width: '100%', display: 'flex', flexWrap: 'wrap', justifyContent: 'space-between' }}
                    onSubmit={handleSubmit(editAirwayBill)}
                >
                    {data?.map(d =>
                        <div style={{ width: windowWidth > 700 ? '45%' : '100%' }} >
                            <h4>{d?.question?.questionText}</h4>
                            <h6 style={{ color: '#a0a0a0' }}>{d?.question?.questionHelpText}</h6>
                            {d?.question?.answerDataTypeId === DatatypeReverse["Boolean"]
                                || d?.question?.answerDataTypeId === DatatypeReverse["Piece"]
                                || d?.question?.answerDataTypeId === DatatypeReverse["Destination"] ?
                                <FormControl variant="standard" style={{ width: '100%', marginBottom: 16 }}>
                                    <Select
                                        defaultValue={d?.answer}
                                        onChange={e => addChangedValue(e, d)}
                                        label={d?.question?.questionText}
                                        required
                                    >
                                        <MenuItem disabled value={null}>
                                            <em>Seçiniz...</em>
                                        </MenuItem>
                                        {Object.values(answerConverter).map(answer => <MenuItem value={answerConverterReverse[answer]}>{answer}</MenuItem>)}
                                    </Select>
                                </FormControl> :
                                d?.question?.answerDataTypeId === DatatypeReverse["Single Choice"] ?
                                    <FormControl variant="standard" style={{ width: '100%', marginBottom: 16 }}>
                                        <Select
                                            defaultValue={d?.answer}
                                            onChange={e => addChangedValue(e, d)}
                                            label={d?.question?.questionText}
                                            required
                                        >
                                            <MenuItem disabled value={null}>
                                                <em>Seçiniz...</em>
                                            </MenuItem>
                                            {d?.specialQuestionAnswers.map(answer => <MenuItem value={answer}>{answer}</MenuItem>)}
                                        </Select>
                                    </FormControl> :
                                    d?.question?.answerDataTypeId === DatatypeReverse["Multiple Choice"] ?
                                        <FormControl component="fieldset" style={{ width: '100%', marginBottom: 16 }}>
                                            <FormGroup style={{
                                                display: 'flex', flexDirection: 'row', justifyContent: 'space-between', flexWrap: 'wrap'
                                            }}>
                                                {d?.specialQuestionAnswers.map(answer =>
                                                    <FormControlLabel
                                                        control={
                                                            <Checkbox
                                                                value={answer}
                                                                onChange={e => handleChangeCheckbox(e, d)}
                                                                checked={changedAnswers.find(answer => answer?.questionId === d?.question?.id)?.value?.includes(answer)}
                                                                name={answer}
                                                                color="primary"
                                                            />
                                                        }
                                                        label={answer}
                                                        name={answer}
                                                    />)}
                                            </FormGroup>
                                        </FormControl> :
                                        d?.question?.answerDataTypeId === DatatypeReverse["Date"] ?
                                            <TextField
                                                className="mb-16"
                                                defaultValue={moment(new Date(d.answer)).format('YYYY-MM-DDThh:mm')}
                                                onChange={e => addChangedValue(e, d)}
                                                style={{ marginBottom: 16 }}
                                                type="datetime-local"
                                                variant="outlined"
                                                required={d?.question?.isRequired}
                                                fullWidth
                                            /> :
                                            d?.question?.answerDataTypeId === DatatypeReverse["Photo"] ?
                                                null
                                                : <TextField
                                                    className="mb-16"
                                                    defaultValue={d.answer}
                                                    onChange={e => addChangedValue(e, d)}
                                                    style={{ marginBottom: 16 }}
                                                    type={d?.question?.answerDataTypeId === DatatypeReverse["Number"] || d?.question?.answerDataTypeId === DatatypeReverse["Decimal"] ? "number" : "text"}
                                                    disabled={d?.question?.answerDataTypeId === DatatypeReverse["Notes"]}
                                                    variant="outlined"
                                                    required={d?.question?.isRequired}
                                                    fullWidth
                                                />}
                            {d?.answerPhoto ?
                                <img alt="answer" style={{ width: 50, height: 50, objectFit: 'contain', marginBottom: 16 }} src={d?.answerPhoto} onClick={() => { handleOpenPhotoDialog(d?.answerPhoto) }} /> : null}
                        </div>)}

                    <div className="flex flex-col sm:flex-row items-center justify-center sm:justify-between" />

                    <Button
                        variant="contained"
                        color="primary"
                        disabled={disabledButton}
                        style={{ width: '60%' }}
                        className="mx-auto m-16"
                        aria-label="COMPLETED AIRWAYBILL"
                        type="submit"
                    >
                        {disabledButton ? <div><CircularProgress size={20} /> PDF Oluşturuluyor...</div> : "Kaydet"}
                    </Button>
                </form>
            </div>
            <PhotoDialog image={dialogImage} open={openPhotoDialog} handleClose={handleClosePhotoDialog} />
        </div>
    )
}
