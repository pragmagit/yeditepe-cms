import FuseLoading from '@fuse/core/FuseLoading';
import FusePageCarded from '@fuse/core/FusePageCarded';
import APICall from 'app/api/APICall';
import { useDialog } from 'app/main/contexts/DialogContext';
import React from 'react';
import { useQuery } from 'react-query';
import { Redirect } from 'react-router';
import EditAirwaybillForm from './EditAirwaybillForm';
import EditAirwaybillHeader from './EditAirwaybillHeader';

export default function EditAirwaybillList({ airwaybillId }) {
    const { data, error, isLoading, refetch } = useQuery('Get Answers of AirwayBill', () => APICall.GetAnswersByAirwaybilId(parseInt(airwaybillId)), { cacheTime: 10000, refetchOnWindowFocus: false, retry: false });
    const { data: airwaybillDetailData, error: detailError, isLoading: detailLoading } = useQuery('Get Airwaybill Detail', () => APICall.GetAirwaybillByID(parseInt(airwaybillId)), { cacheTime: 10000, refetchOnWindowFocus: false, retry: false });
    const { showFailureDialog } = useDialog();

    if (isLoading || detailLoading) {
        return <FuseLoading />
    }

    if (error || detailError) {
        showFailureDialog(error?.response?.data?.errorDescription ? error?.response?.data?.errorDescription : "Beklenmedik Hata");
        return <Redirect to="/pages/airwaybills/completed" />
    }

    return (
        <div className="flex-grow overflow-x-auto">
            <FusePageCarded
                header={<EditAirwaybillHeader title={"Tamamlanan Sevkiyat Düzenleme"} />}
                content={<EditAirwaybillForm data={data?.data?.data} refetch={refetch} airwaybillId={airwaybillId} airwaybillDetailData={airwaybillDetailData?.data?.data} />}
                innerScroll
            />
        </div>
    )
}
