import FuseLoading from '@fuse/core/FuseLoading';
import { useAuth } from 'app/main/contexts/AuthContext';
import React from 'react';
import { Redirect, useParams } from 'react-router';
import EditAirwaybillList from './EditAirwaybillList';

export default function EditAirwaybillMain() {
    const { isLoggedIn, remindProcess } = useAuth();
    const { airwaybillId } = useParams();

    if (remindProcess) {
        return <FuseLoading />
    }

    if (!isLoggedIn) {
        return <Redirect to="/" />
    }

    return (
        <EditAirwaybillList airwaybillId={airwaybillId} />
    )
}
