import FuseLoading from '@fuse/core/FuseLoading';
import { AirwaybillStatus } from 'app/enum/enums';
import { useAuth } from 'app/main/contexts/AuthContext';
import React from 'react';
import { Redirect, useParams } from 'react-router';
import AirwayBillsList from './AirwayBillsList';

export default function AirwayBillsMain() {
    const { isLoggedIn, remindProcess } = useAuth();
    const { airwayBillStatus } = useParams();

    if (remindProcess) {
        return <FuseLoading />
    }

    if (!isLoggedIn) {
        return <Redirect to="/" />
    }

    return (
        <AirwayBillsList airwayBillStatus={AirwaybillStatus[airwayBillStatus]} />
    )
}
