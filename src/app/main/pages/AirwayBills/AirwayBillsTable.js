import { useState } from 'react';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { Button } from 'devextreme-react/button';
import DataGrid, { Column, FilterRow, HeaderFilter, Export, Scrolling, Paging } from 'devextreme-react/data-grid';
import { motion } from 'framer-motion';
import React from 'react';
import { useHistory } from 'react-router';
import { AirwaybillStatus, WeightUnit } from '../../../enum/enums';
import { Workbook } from 'exceljs';
import saveAs from 'file-saver';
import { exportDataGrid } from 'devextreme/excel_exporter';
import AnswerDialog from './AnswerDialog';
import { useWindow } from 'app/main/contexts/WindowContext';

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

export default function AirwayBillsTable({ data, airwayBillStatus, openNoteDialog, headerTitle }) {
    const classes = useStyles();
    const history = useHistory();
    const { windowWidth } = useWindow();
    const [selectedAirwaybillData, setSelectedAirwaybillData] = useState(null);
    const [answerDialogOpen, setAnswerDialogOpen] = useState(false);

    const handleOpenAnswerDialog = (data) => {
        setAnswerDialogOpen(true);
        setSelectedAirwaybillData(data);
    }

    const handleCloseAnswerDialog = () => {
        setAnswerDialogOpen(false);
    }

    if (data.length === 0) {
        return (
            <motion.div
                initial={{ opacity: 0 }}
                animate={{ opacity: 1, transition: { delay: 0.1 } }}
                className="flex flex-1 items-center justify-center h-full"
            >
                <Typography color="textSecondary" variant="h5">
                    Konşimento bulunamadı!
                </Typography>
            </motion.div>
        );
    }

    const renderImage = (cellData) => {
        return (<div><img style={{ width: 50, height: 50, objectFit: 'contain' }} alt={cellData.data.cargoCompany.name} src={cellData.data.cargoCompany.photoURL}></img></div>);
    }

    const calculateWeightCell = (data) => {
        return data.weight + " " + WeightUnit[data.weightUnitId];
    }

    const calculateCompanyName = (data) => {
        return data.cargoCompany.name;
    }

    const calculatePDFLink = (data) => {
        return data.pdfUrl;
    }

    const renderPDFLink = (cellData) => {
        return cellData?.data?.pdfUrl ? <a href={cellData.data.pdfUrl}>PDF'i İndir</a> : "Pdf Oluşturuluyor...";
    }

    const renderShowButton = (cellData) => {
        return (
            <div>
                <Button
                    text="Göster"
                    type="default"
                    stylingMode="text"
                    onClick={() => handleOpenAnswerDialog({ airwaybillId: cellData.data.id, airwaybillNo: cellData.data.airwaybillNo })}
                />
            </div>)
    }

    const renderEditButton = (cellData) => {
        return (
            <div>
                <Button
                    text="Düzenle"
                    type="default"
                    stylingMode="text"
                    onClick={() => history.push('/pages/airwaybill/edit/' + cellData.data.id)}
                />
            </div>)
    }

    const renderNoteButton = (cellData) => {
        return (
            <div>
                <Button
                    text="Not Ekle"
                    type="default"
                    stylingMode="text"
                    onClick={() => { openNoteDialog(cellData.data) }}
                />
            </div>
        )
    }

    const renderApprovalButton = (cellData) => {
        return (
            <div>
                <Button
                    text="İncele"
                    type="default"
                    stylingMode="text"
                    onClick={() => history.push('/pages/airwaybill/approval/' + cellData.data.airwaybillNo)}
                />
            </div>
        )
    }

    const onExporting = (e) => {
        const workbook = new Workbook();
        const worksheet = workbook.addWorksheet('Main sheet');
        exportDataGrid({
            component: e.component,
            worksheet: worksheet,
            autoFilterEnabled: true,
            customizeCell: function (options) {
                const excelCell = options;
                excelCell.font = { name: 'Arial', size: 12 };
                excelCell.alignment = { horizontal: 'left' };
            }
        }).then(function () {
            workbook.xlsx.writeBuffer()
                .then(function (buffer) {
                    saveAs(new Blob([buffer], { type: 'application/octet-stream' }), `${headerTitle}.xlsx`);
                });
        });
        e.cancel = true;
    }

    const weightFilter = [{
        text: '100 Kilogramdan Az',
        value: ['weight', '<', 100]
    }, {
        text: '100 - 1000 Kilogram Arası',
        value: [
            ['weight', '>=', 100],
            ['weight', '<', 1000]
        ]
    }, {
        text: '1000 - 10000 Kilogram Arası',
        value: [
            ['weight', '>=', 1000],
            ['weight', '<', 10000]
        ]
    }, {
        text: '10000 Kilogram Üzeri',
        value: [
            ['weight', '>=', 10000]
        ]
    }];

    return (
        <div className="w-full flex flex-col">
            <FuseScrollbars className="flex-grow overflow-x-auto">
                <DataGrid
                    height="100%"
                    keyExpr="id"
                    dataSource={data}
                    onExporting={onExporting}
                    showBorders={true}
                    paging={false}
                    columnAutoWidth={true}
                    selection={{ mode: 'single' }}
                    hoverStateEnabled={true}
                    allowColumnResizing={true}
                    noDataText="Sevkiyat Bulunamadı"
                    style={{ padding: 10 }}
                >
                    <Scrolling showScrollbar="always" mode="virtual" rowRenderingMode="virtual" />
                    <Paging defaultPageSize={20} />
                    <FilterRow visible={true} applyFilter='auto' />
                    <HeaderFilter visible={true} />
                    <Column
                        width={70}
                        caption="Firma"
                        cellRender={renderImage}
                        calculateCellValue={calculateCompanyName}
                        dataField="Picture"
                        cssClass="justify-table"
                        fixed={windowWidth > 500 ? true : false}
                    />
                    <Column
                        dataField="airwaybillNo"
                        caption="Konşimento No"
                        dataType="string"
                        cssClass="justify-table"
                        fixed={windowWidth > 500 ? true : false}
                    />
                    <Column
                        dataField="parentAirwayBill"
                        caption="Ana Konşimento"
                        dataType="string"
                        cssClass="justify-table"
                        fixed={windowWidth > 500 ? true : false}
                    />
                    <Column
                        dataField="origin"
                        caption="Kalkış Yeri"
                        dataType="string"
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="destination"
                        caption="Varış Yeri"
                        dataType="string"
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="takeOffTime"
                        caption="Kalkış Zamanı"
                        dataType="datetime"
                        format="dd/MM/yyyy HH:mm"
                        cssClass="justify-table"
                    >
                        <HeaderFilter groupInterval="day" />
                    </Column>
                    {airwayBillStatus != AirwaybillStatus.AWAITING ?
                        <Column
                            dataField="controlDate"
                            caption="Kontrol Zamanı"
                            dataType="datetime"
                            format="dd/MM/yyyy HH:mm"
                            cssClass="justify-table"
                        >
                            <HeaderFilter groupInterval="day" />
                        </Column> : null}
                    <Column
                        dataField="importerName"
                        caption="İthalatçı"
                        dataType="string"
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="exporterName"
                        caption="İhracatçı"
                        dataType="string"
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="shipmentType.name"
                        caption="Gönderi Türü"
                        dataType="string"
                        cssClass="justify-table"
                    />
                    <Column
                        dataField="piece"
                        caption="Kap Adeti"
                        dataType="number"
                        cssClass="justify-table"
                    ></Column>
                    <Column
                        dataField="weight"
                        caption="Ağırlık"
                        dataType="number"
                        calculateDisplayValue={calculateWeightCell}
                        cssClass="justify-table"
                    >
                        <HeaderFilter dataSource={weightFilter} />
                    </Column>
                    {airwayBillStatus !== AirwaybillStatus.AWAITING ?
                        <Column
                            dataField="memberNameSurname"
                            caption="Operatör"
                            dataType="string"
                            cssClass="justify-table"
                        /> : null}
                    {airwayBillStatus == AirwaybillStatus.COMPLETED ?
                        <Column
                            minWidth={120}
                            caption="PDF"
                            cellRender={renderPDFLink}
                            calculateCellValue={calculatePDFLink}
                            cssClass="justify-table"
                        /> : null}
                    {airwayBillStatus == AirwaybillStatus.COMPLETED ?
                        <Column
                            minWidth={120}
                            cellRender={renderShowButton}
                            cssClass="justify-table"
                        /> : null}
                    {airwayBillStatus == AirwaybillStatus.COMPLETED ?
                        <Column
                            minWidth={120}
                            cellRender={renderEditButton}
                            cssClass="justify-table"
                        /> : null}
                    {airwayBillStatus == AirwaybillStatus.COMPLETED ?
                        <Column
                            minWidth={120}
                            cellRender={renderNoteButton}
                            cssClass="justify-table"
                        /> : null}
                    {airwayBillStatus == AirwaybillStatus.AWAITING_APPROVAL ?
                        <Column
                            minWidth={120}
                            cellRender={renderApprovalButton}
                            cssClass="justify-table"
                        /> : null}
                    <Export enabled={true} />
                </DataGrid>
                <AnswerDialog open={answerDialogOpen} close={handleCloseAnswerDialog} selectedAirwaybillData={selectedAirwaybillData} />
            </FuseScrollbars>

        </div>
    )
}