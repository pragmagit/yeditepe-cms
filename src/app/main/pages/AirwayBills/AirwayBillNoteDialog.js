import { Controller, useForm } from 'react-hook-form';
import { Button, CircularProgress } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import _ from '@lodash';
import APICall from 'app/api/APICall';
import { useEffect, useState } from 'react';
import { useDialog } from 'app/main/contexts/DialogContext';
import { Dialog, DialogTitle, DialogContent } from '@material-ui/core';

const schema = yup.object().shape({
    note: yup.string().required('Boş bırakılamaz.')
});

export default function AirwayBillNoteDialog({ open, handleClose, selectedAirwayBillData, refetch }) {
    const { showSuccessDialog, showFailureDialog } = useDialog();
    const [disabledButton, setDisabledButton] = useState(false);

    const defaultValues = {
        note: ''
    }

    const { control, formState, handleSubmit, reset } = useForm({
        mode: 'onChange',
        defaultValues,
        resolver: yupResolver(schema)
    });

    const { isValid, dirtyFields, errors } = formState;

    const addNote = async (data) => {
        setDisabledButton(true);
        try {
            data.airwaybillNo = selectedAirwayBillData.airwaybillNo;
            APICall.AddNoteToCompletedShipment(data).then(() => {
                showSuccessDialog("Not ekleme işleminiz başarılı. PDF yeniden oluşturuldu.");
                refetch();
            }).catch(err => {
                showFailureDialog(err?.response?.data?.errorDescription ? err?.response?.data?.errorDescription : "Not eklerken beklenmedik bir hata oluştu.");
            });
            setDisabledButton(false);
            showSuccessDialog("Not ekleme isteğiniz başarıyla gönderildi. Not eklendikten sonra PDF yeniden oluşturulacak.");
            handleClose();
        }
        catch (err) {
            setDisabledButton(false);
            showFailureDialog(err?.response?.data?.errorDescription);
        }
    }

    useEffect(() => {
        reset();
        setDisabledButton(false);
    }, [selectedAirwayBillData])

    return (
        <Dialog onClose={handleClose} aria-labelledby="answers-dialog-title" open={open} maxWidth="sm" fullWidth>
            <DialogTitle color="primary" id="answers-dialog-title">{selectedAirwayBillData?.airwaybillNo} No'lu Konşimento için Not Ekle</DialogTitle>
            <DialogContent dividers>
                <form
                    name="newNoteForm"
                    noValidate
                    className="flex flex-col justify-center w-full"
                    onSubmit={handleSubmit(addNote)}
                >
                    <Controller
                        name="note"
                        control={control}
                        render={({ field }) => (
                            <TextField
                                {...field}
                                className="mb-16"
                                style={{ marginBottom: 16 }}
                                label="Notunuz"
                                type="text"
                                variant="outlined"
                                error={!!errors.note}
                                helperText={errors?.note?.message}
                                required
                                fullWidth
                            />
                        )}
                    />

                    <Button
                        variant="contained"
                        color="primary"
                        disabled={_.isEmpty(dirtyFields) || !isValid || disabledButton}
                        className="w-full mx-auto mt-16"
                        aria-label="NEW NOTE"
                        type="submit"
                    >
                        {disabledButton ? <div><CircularProgress size={20} /> Bekleyiniz</div> : "Notu Ekle"}
                    </Button>

                    <Button
                        variant="contained"
                        style={{ backgroundColor: 'red', color: 'white' }}
                        className="w-full mx-auto mt-16"
                        aria-label="CANCEL"
                        onClick={handleClose}
                    >
                        Vazgeç
                    </Button>
                </form>
            </DialogContent>
        </Dialog>
    )
}
