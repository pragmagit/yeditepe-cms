import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import Typography from '@material-ui/core/Typography';
import { motion } from 'framer-motion';
import _ from 'lodash';
import { WeightUnit } from '../../../enum/enums';
import DataGrid, { Column, Scrolling, Paging } from 'devextreme-react/data-grid';

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

export default function AirwayBillDetail({ data }) {
    if (data.length === 0) {
        return (
            <motion.div
                initial={{ opacity: 0 }}
                animate={{ opacity: 1, transition: { delay: 0.1 } }}
                className="flex flex-1 items-center justify-center h-full"
            >
                <Typography color="textSecondary" variant="h5">
                    Konşimento bulunamadı!
                </Typography>
            </motion.div>
        );
    }

    const renderImage = (cellData) => {
        return (<div><img style={{ width: 50, height: 50, objectFit: 'contain' }} src={cellData.data.cargoCompany.photoURL}></img></div>);
    }

    const calculateWeightCell = (data) => {
        return data.weight + " " + WeightUnit[data.weightUnitId];
    };

    return (
        <div className="w-full flex flex-col">
            <FuseScrollbars className="flex-grow overflow-x-auto">
                <DataGrid
                    keyExpr="id"
                    dataSource={[data]}
                    showBorders={true}
                    paging={false}
                    allowColumnResizing={true}
                    noDataText="Sevkiyat Bulunamadı"
                    style={{ padding: 10 }}
                >
                    <Scrolling showScrollbar="always" mode="virtual" rowRenderingMode="virtual" />
                    <Paging defaultPageSize={20} />
                    <Column
                        width={70}
                        caption="Firma"
                        cellRender={renderImage}
                        cssClass="justify-table"
                    />
                    <Column
                        width={140}
                        dataField="airwaybillNo"
                        caption="Konşimento No"
                        dataType="string"
                        cssClass="justify-table"
                    />
                    <Column
                        width={140}
                        dataField="origin"
                        caption="Kalkış Yeri"
                        dataType="string"
                        cssClass="justify-table"
                    />
                    <Column
                        width={140}
                        dataField="destination"
                        caption="Varış Yeri"
                        dataType="string"
                        cssClass="justify-table"
                    />
                    <Column
                        minWidth={180}
                        dataField="takeOffTime"
                        caption="Kalkış Zamanı"
                        dataType="datetime"
                        format="dd/MM/yyyy HH:mm"
                        cssClass="justify-table"
                    />
                    <Column
                        minWidth={180}
                        dataField="controlDate"
                        caption="Kontrol Zamanı"
                        dataType="datetime"
                        format="dd/MM/yyyy HH:mm"
                        cssClass="justify-table"
                    />
                    <Column
                        width={180}
                        dataField="importerName"
                        caption="İthalatçı"
                        dataType="string"
                        cssClass="justify-table"
                    />
                    <Column
                        width={180}
                        dataField="exporterName"
                        caption="İhracatçı"
                        dataType="string"
                        cssClass="justify-table"
                    />
                    <Column
                        width={180}
                        dataField="shipmentType.name"
                        caption="Gönderi Türü"
                        dataType="string"
                        cssClass="justify-table"
                    />
                    <Column
                        width={160}
                        dataField="weight"
                        caption="Ağırlık"
                        dataType="number"
                        calculateDisplayValue={calculateWeightCell}
                        cssClass="justify-table"
                    />
                </DataGrid>
            </FuseScrollbars>
        </div>
    )
}