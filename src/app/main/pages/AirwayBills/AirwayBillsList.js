import React, { useEffect, useState } from 'react';
import FusePageCarded from '@fuse/core/FusePageCarded';
import AirwayBillsHeader from './AirwayBillsHeader';
import { useQuery } from 'react-query';
import APICall from 'app/api/APICall';
import FuseLoading from '@fuse/core/FuseLoading';
import AirwayBillsTable from './AirwayBillsTable';
import { AirwaybillStatus } from 'app/enum/enums';
import AirwayBillNoteDialog from './AirwayBillNoteDialog';

export default function AirwayBillsList({ airwayBillStatus }) {
    const { isLoading, error, data, refetch } = useQuery('AirwayBillsList', () => APICall.GetAirwaybills(), { cacheTime: 10000, refetchOnWindowFocus: false, retry: false });
    const [airwaybillList, setAirwaybillList] = useState([]);
    const [headerTitle, setHeaderTitle] = useState("");
    const [selectedAirwayBillData, setSelectedAirwayBillData] = useState(null);
    const [noteDialogOpen, setNoteDialogOpen] = useState(false);

    const openNoteDialog = (data) => {
        setSelectedAirwayBillData(data);
        setNoteDialogOpen(true);
    }

    const closeNoteDialog = () => {
        setNoteDialogOpen(false);
    }

    useEffect(() => {
        if (data) {
            setAirwaybillList(data?.data?.data?.filter(airwaybill => (airwaybill.airwayBillStatusId === airwayBillStatus)));
        }
        setHeaderTitle(airwayBillStatus === AirwaybillStatus.COMPLETED ? "Tamamlanan" :
            airwayBillStatus === AirwaybillStatus.AWAITING ? "Bekleyen" :
                airwayBillStatus === AirwaybillStatus.AWAITING_APPROVAL ? "Onay Bekleyen" : "")
    }, [data, airwayBillStatus])

    if (isLoading) {
        return <FuseLoading />
    }

    return (
        <div className="flex-grow overflow-x-auto">
            <FusePageCarded
                classes={{
                    content: 'flex',
                    contentCard: 'overflow-hidden',
                    header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
                }}
                header={<AirwayBillsHeader title={headerTitle + " Sevkiyatlar (Toplam: " + airwaybillList.length + " adet)"} />}
                content={<AirwayBillsTable data={airwaybillList} headerTitle={headerTitle} airwayBillStatus={airwayBillStatus} openNoteDialog={openNoteDialog} />}
                innerScroll
            />
            <AirwayBillNoteDialog open={noteDialogOpen} handleClose={closeNoteDialog} selectedAirwayBillData={selectedAirwayBillData} refetch={refetch} />
        </div>
    )
}
