import { lazy } from 'react';

const AirwayBillConfig = {
    settings: {
        layout: {
            config: {
                navbar: {
                    display: true
                },
                toolbar: {
                    display: true
                },
                footer: {
                    display: false
                },
                leftSidePanel: {
                    display: false
                },
                rightSidePanel: {
                    display: false
                }
            }
        }
    },
    routes: [
        {
            path: '/pages/airwaybills/:airwayBillStatus',
            component: lazy(() => import('./AirwayBillsMain'))
        },
        {
            path: '/pages/airwaybill/approval/:airwayBillNo',
            component: lazy(() => import('./BillAnswers/BillAnswersMain'))
        },
        {
            path: '/pages/airwaybill/edit/:airwaybillId',
            component: lazy(() => import('./EditAirwaybill/EditAirwaybillMain'))
        }
    ]
};

export default AirwayBillConfig;
