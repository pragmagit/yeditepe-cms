import React, { useEffect, useState } from 'react';
import Typography from '@material-ui/core/Typography';
import { motion } from 'framer-motion';
import { Button } from '@material-ui/core';
import APICall from 'app/api/APICall';
import { CircularProgress } from '@material-ui/core';
import { useDialog } from 'app/main/contexts/DialogContext';
import { useHistory } from 'react-router';

export default function BillAnswerHeader({ title, airwayBillNo, answerList, handleOpenWaitDialog, handleOpenCompleteDialog }) {
    const [completeButtonDisabled, setCompleteButtonDisabled] = useState(true);
    const [completeLoading, setCompleteLoading] = useState(false);
    const { showSuccessDialog, showFailureDialog } = useDialog();

    const confirmAirwayBill = async () => {
        setCompleteLoading(true);
        const allAnswersConfirmed = answerList.every(approval => approval.isConfirmed === true);
        const isIncludeAtLeastOneReject = answerList.some(approval => approval.isConfirmed === false);
        try {
            if (allAnswersConfirmed) {
                handleOpenCompleteDialog({ airwayBillNo, isApproved: true });
            }
            else if (isIncludeAtLeastOneReject) {
                handleOpenCompleteDialog({ airwayBillNo, isApproved: false });
            }
            else {
                handleOpenWaitDialog();
            }
            setCompleteLoading(false);
        }
        catch (err) {
            showFailureDialog(err?.response?.data?.errorDescription ? err?.response?.data?.errorDescription : "Beklenmeyen Bir Hata");
            setCompleteLoading(false);
        }
    }

    useEffect(() => {
        if (answerList.length > 0) {
            const allAnswersConfirmed = answerList.every(approval => approval.isConfirmed === true);
            const isIncludeAtLeastOneReject = answerList.some(approval => approval.isConfirmed === false);
            if (allAnswersConfirmed || isIncludeAtLeastOneReject) {
                setCompleteButtonDisabled(false);
            }
            else {
                setCompleteButtonDisabled(true);
            }
        }
    }, [answerList])

    return (
        <div className="flex flex-1 w-full items-center justify-between">
            <div className="flex items-center">
                <Typography
                    component={motion.span}
                    initial={{ x: -20 }}
                    animate={{ x: 0, transition: { delay: 0.2 } }}
                    delay={300}
                    className="text-16 md:text-24 mx-12 font-semibold"
                >
                    {title}
                </Typography>
            </div>
            <motion.div initial={{ opacity: 0, x: 20 }} animate={{ opacity: 1, x: 0, transition: { delay: 0.2 } }}>
                <Button
                    className="whitespace-nowrap"
                    disabled={completeLoading}
                    variant="contained"
                    style={{ backgroundColor: 'darkgreen', color: 'white' }}
                    onClick={() => { confirmAirwayBill() }}
                >
                    {completeLoading ?
                        <div><CircularProgress size={20} /> Yükleniyor</div> :
                        <div>
                            <span className="hidden sm:flex">Konşimentoyu Tamamla</span>
                            <span className="flex sm:hidden">Onay</span>
                        </div>}
                </Button>
            </motion.div>
        </div>
    )
}
