import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import APICall from 'app/api/APICall';

export default function BillAnswerWaitApprovalDialog({ open, handleClose }) {
    return (
        <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">{"Onay Bekleyen Cevaplar Var"}</DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    Onay bekleyen cevaplar mevcut. Lütfen tamamlamak için onay durumlarını değiştiriniz.
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} color="secondary">
                    Tamam
                </Button>
            </DialogActions>
        </Dialog>
    )
}
