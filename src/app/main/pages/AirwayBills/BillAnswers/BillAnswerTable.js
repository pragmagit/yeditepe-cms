import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import FuseScrollbars from '@fuse/core/FuseScrollbars';
import Typography from '@material-ui/core/Typography';
import { motion } from 'framer-motion';
import { Button } from '@material-ui/core';
import _ from 'lodash';
import { useHistory } from 'react-router';
import APICall from 'app/api/APICall';

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

const answerConverter = {
    "true": "Evet",
    "false": "Hayır"
}

const confirmConverter = {
    "true": "Onaylandı",
    "false": "Reddedildi"
}

export default function BillAnswerTable({ data, refetch, handleOpenPhotoDialog, handleOpenSnackbar }) {
    const classes = useStyles();
    const history = useHistory();

    const updateQuestionConfirmation = async (ApprovalId, confirmation) => {
        await APICall.SetApprovalConfirmation({ ApprovalId, confirmation }).then(() => {
            handleOpenSnackbar('Onay durumu ' + confirmConverter[confirmation?.toString()] + ' olarak değiştirildi.', 'info');
            refetch();
        }).catch(() => {
            handleOpenSnackbar('Bir hata oluştu. Lütfen tekrar deneyiniz.', 'danger');
        });
    }

    if (data.length === 0) {
        return (
            <motion.div
                initial={{ opacity: 0 }}
                animate={{ opacity: 1, transition: { delay: 0.1 } }}
                className="flex flex-1 items-center justify-center h-full"
            >
                <Typography color="textSecondary" variant="h5">
                    Konşimento cevapları bulunamadı!
                </Typography>
            </motion.div>
        );
    }

    return (
        <div className="w-full flex flex-col">
            <FuseScrollbars className="flex-grow overflow-x-auto">
                <Table stickyHeader className={classes.table} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell align="left">SORU</TableCell>
                            <TableCell align="left">CEVAP</TableCell>
                            <TableCell align="left">Onay Durumu</TableCell>
                            <TableCell align="center">Fotoğraf</TableCell>
                            <TableCell align="left" />
                            <TableCell align="left" />
                            <TableCell align="left" />
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            _.orderBy(data)
                                .map(airwaybillApproval => (
                                    <TableRow hover className="h-72" key={airwaybillApproval.id}>
                                        <TableCell align="left">
                                            {airwaybillApproval?.question?.questionText}{airwaybillApproval?.question?.questionTextEN ? "/" + airwaybillApproval?.question?.questionTextEN : null}
                                            <p style={{ color: '#a0a0a0' }}>{airwaybillApproval?.question?.questionHelpText}</p>
                                        </TableCell>
                                        <TableCell align="left">{answerConverter[airwaybillApproval?.answer] ? answerConverter[airwaybillApproval?.answer] : airwaybillApproval?.answer}</TableCell>
                                        <TableCell align="left"><strong style={{ color: airwaybillApproval?.isConfirmed ? 'green' : airwaybillApproval?.isConfirmed !== null ? 'red' : 'gray' }}>{airwaybillApproval?.isConfirmed !== null ? confirmConverter[airwaybillApproval?.isConfirmed] : "Onay Bekliyor"}</strong></TableCell>
                                        <TableCell align="center">
                                            {airwaybillApproval.answerPhoto ?
                                                <Button
                                                    color="secondary"
                                                    component="label"
                                                    onClick={() => { handleOpenPhotoDialog(airwaybillApproval.answerPhoto) }}
                                                >
                                                    Görüntüle
                                                </Button> : "Fotoğraf Yok"}
                                        </TableCell>
                                        <TableCell size="small" align="right">
                                            <Button
                                                variant="contained"
                                                component="label"
                                                onClick={() => { updateQuestionConfirmation(airwaybillApproval.id, true) }}
                                            >
                                                Onayla
                                            </Button>
                                        </TableCell>
                                        <TableCell align="left">
                                            <Button
                                                variant="contained"
                                                component="label"
                                                onClick={() => { updateQuestionConfirmation(airwaybillApproval.id, false) }}
                                            >
                                                Reddet
                                            </Button>
                                        </TableCell>
                                    </TableRow>
                                ))
                        }
                    </TableBody>
                </Table>
            </FuseScrollbars>
        </div>
    )
}