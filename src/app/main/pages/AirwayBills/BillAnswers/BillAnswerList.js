import React, { useEffect, useState } from 'react';
import FusePageCarded from '@fuse/core/FusePageCarded';
import BillAnswerHeader from './BillAnswerHeader';
import { useQuery } from 'react-query';
import APICall from 'app/api/APICall';
import FuseLoading from '@fuse/core/FuseLoading';
import BillAnswerTable from './BillAnswerTable';
import { useParams } from 'react-router';
import PhotoDialog from '../PhotoDialog';
import BillAnswerWaitApprovalDialog from './BillAnswerWaitApprovalDialog';
import SnackbarMessage from 'app/shared-components/SnackbarMessage';
import { truncate } from 'lodash';
import BillAnswerConfirmApprovalDialog from './BillAnswerConfirmApprovalDialog';


export default function BillAnswerList() {
    const { airwayBillNo } = useParams();
    const { isLoading, error, data, refetch } = useQuery('Bill Answer List', () => APICall.GetApprovalsForCMS(airwayBillNo), { cacheTime: 10000, refetchOnWindowFocus: false });
    const [answerList, setAnswerList] = useState([]);
    const [openPhotoDialog, setOpenPhotoDialog] = useState(false);
    const [dialogImage, setDialogImage] = useState(null);
    const [openWaitDialog, setOpenWaitDialog] = useState(false);
    const [snackbarText, setSnackbarText] = useState("");
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarSuccess, setSnackbarSuccess] = useState('');
    const [completeDialogOpen, setCompleteDialogOpen] = useState(false);
    const [completeDialogData, setCompleteDialogData] = useState(null);

    const handleOpenCompleteDialog = (data) => {
        setCompleteDialogData(data);
        setCompleteDialogOpen(true);
    }

    const handleCloseCompleteDialog = () => {
        setCompleteDialogOpen(false);
        setCompleteDialogData(null);
    }

    const handleOpenSnackbar = (text, success) => {
        setSnackbarText(text);
        setSnackbarSuccess(success);
        setSnackbarOpen(true);
    }

    const handleSnackbarClose = () => {
        setSnackbarOpen(false);
    }

    const handleOpenPhotoDialog = (data) => {
        setDialogImage(data);
        setOpenPhotoDialog(true);
    }

    const handleClosePhotoDialog = () => {
        setOpenPhotoDialog(false);
    }

    const handleOpenWaitDialog = () => {
        setOpenWaitDialog(true);
    }

    const handlCloseWaitDialog = () => {
        setOpenWaitDialog(false);
    }

    useEffect(() => {
        if (data) {
            setAnswerList(data?.data?.data);
        }
    }, [data])

    if (isLoading) {
        return <FuseLoading />
    }

    return (
        <div className="flex-grow overflow-x-auto">
            <FusePageCarded
                classes={{
                    content: 'flex',
                    contentCard: 'overflow-hidden',
                    header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
                }}
                header={<BillAnswerHeader title={airwayBillNo + " Numaralı Sevkiyat Onayı"} handleOpenCompleteDialog={handleOpenCompleteDialog} airwayBillNo={airwayBillNo} answerList={answerList} handleOpenWaitDialog={handleOpenWaitDialog} />}
                content={<BillAnswerTable data={answerList} refetch={refetch} handleOpenPhotoDialog={handleOpenPhotoDialog} handleOpenSnackbar={handleOpenSnackbar} />}
                innerScroll
            />
            <PhotoDialog open={openPhotoDialog} handleClose={handleClosePhotoDialog} image={dialogImage} />
            <BillAnswerWaitApprovalDialog open={openWaitDialog} handleClose={handlCloseWaitDialog} />
            <SnackbarMessage snackbarText={snackbarText} snackbarOpen={snackbarOpen} snackbarClose={handleSnackbarClose} snackbarSuccess={snackbarSuccess} />
            <BillAnswerConfirmApprovalDialog open={completeDialogOpen} handleClose={handleCloseCompleteDialog} data={completeDialogData} />
        </div>
    )
}
