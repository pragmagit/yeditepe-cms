import FuseLoading from '@fuse/core/FuseLoading';
import { useAuth } from 'app/main/contexts/AuthContext';
import React from 'react';
import { Redirect } from 'react-router';
import BillAnswerList from './BillAnswerList';

export default function BillAnswersMain() {
    const { isLoggedIn, remindProcess } = useAuth();

    if (remindProcess) {
        return <FuseLoading />
    }

    if (!isLoggedIn) {
        return <Redirect to="/" />
    }

    return (
        <BillAnswerList />
    )
}
