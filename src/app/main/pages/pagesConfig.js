import DashboardConfig from "../dashboard/DashboardConfig";
import AirwayBillsConfig from "./AirwayBills/AirwayBillsConfig";
import VehiclesConfig from "./IncomingVehicles/VehiclesConfig";
import LogsConfig from "./Logs/LogsConfig";
import QuestionConfig from "./Questions/QuestionConfig";
import ShipmentTypeConfig from "./ShipmentTypes/ShipmentTypeConfig";
import StaffConfig from "./Staff/StaffConfig";

const pagesConfigs = [
    DashboardConfig,
    AirwayBillsConfig,
    VehiclesConfig,
    QuestionConfig,
    ShipmentTypeConfig,
    StaffConfig,
    LogsConfig
];

export default pagesConfigs;
