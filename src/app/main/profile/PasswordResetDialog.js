import { Controller, useForm } from 'react-hook-form';
import { Button, CircularProgress } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import _ from '@lodash';
import APICall from 'app/api/APICall';
import { useEffect, useState } from 'react';
import { useDialog } from 'app/main/contexts/DialogContext';
import { Dialog, DialogTitle, DialogContent } from '@material-ui/core';

const schema = yup.object().shape({
    currentPassword: yup.string().required('Boş bırakılamaz.'),
    newPassword: yup.string().min(6, 'Parolanız en az 6 haneli olmalıdır.').required('Boş bırakılamaz.'),
    newPasswordAgain: yup.string().oneOf([yup.ref('newPassword'), null], 'Parolalar eşleşmiyor.')
});

export default function PasswordResetDialog({ open, handleClose }) {
    const { showSuccessDialog, showFailureDialog } = useDialog();
    const [disabledButton, setDisabledButton] = useState(false);

    const defaultValues = {
        currentPassword: '',
        newPassword: '',
        newPasswordAgain: ''
    }

    const { control, formState, handleSubmit, reset } = useForm({
        mode: 'onChange',
        defaultValues,
        resolver: yupResolver(schema)
    });

    const { isValid, dirtyFields, errors } = formState;

    const changePassword = async (data) => {
        try {
            if (data.newPassword === data.newPasswordAgain) {
                console.log(data);
            }
            else {
                showFailureDialog('Parolalar eşleşmiyor.');
            }
        }
        catch (err) {

        }
    }

    useEffect(() => {
        reset();
        setDisabledButton(false);
    }, [])

    return (
        <Dialog onClose={handleClose} aria-labelledby="answers-dialog-title" open={open} maxWidth="sm" fullWidth>
            <DialogTitle color="primary" id="answers-dialog-title">Şifre Değişikliği</DialogTitle>
            <DialogContent dividers>
                <form
                    name="newNoteForm"
                    noValidate
                    className="flex flex-col justify-center w-full"
                    onSubmit={handleSubmit(changePassword)}
                >
                    <Controller
                        name="currentPassword"
                        control={control}
                        render={({ field }) => (
                            <TextField
                                {...field}
                                className="mb-16"
                                style={{ marginBottom: 16 }}
                                label="Mevcut Parola"
                                type="password"
                                variant="outlined"
                                error={!!errors.currentPassword}
                                helperText={errors?.currentPassword?.message}
                                required
                                fullWidth
                            />
                        )}
                    />

                    <Controller
                        name="newPassword"
                        control={control}
                        render={({ field }) => (
                            <TextField
                                {...field}
                                className="mb-16"
                                style={{ marginBottom: 16 }}
                                label="Yeni Parola"
                                type="password"
                                variant="outlined"
                                error={!!errors.newPassword}
                                helperText={errors?.newPassword?.message}
                                required
                                fullWidth
                            />
                        )}
                    />

                    <Controller
                        name="newPasswordAgain"
                        control={control}
                        render={({ field }) => (
                            <TextField
                                {...field}
                                className="mb-16"
                                style={{ marginBottom: 16 }}
                                label="Yeni Parola Tekrar"
                                type="password"
                                variant="outlined"
                                error={!!errors.newPasswordAgain}
                                helperText={errors?.newPasswordAgain?.message}
                                required
                                fullWidth
                            />
                        )}
                    />

                    <Button
                        variant="contained"
                        color="primary"
                        disabled={_.isEmpty(dirtyFields) || !isValid || disabledButton}
                        className="w-full mx-auto mt-16"
                        aria-label="Change Password"
                        type="submit"
                    >
                        {disabledButton ? <div><CircularProgress size={20} /> Bekleyiniz</div> : "Şifreyi Değiştir"}
                    </Button>

                    <Button
                        variant="contained"
                        style={{ backgroundColor: 'red', color: 'white' }}
                        className="w-full mx-auto mt-16"
                        aria-label="CANCEL"
                        onClick={handleClose}
                    >
                        Vazgeç
                    </Button>
                </form>
            </DialogContent>
        </Dialog>
    )
}
