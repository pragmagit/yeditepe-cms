import { yupResolver } from '@hookform/resolvers/yup';
import { Button, CircularProgress } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { useDialog } from 'app/main/contexts/DialogContext';
import { useWindow } from 'app/main/contexts/WindowContext';
import clsx from 'clsx';
import { useEffect, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { useHistory } from 'react-router';
import * as yup from 'yup';
import { useAuth } from '../contexts/AuthContext';
import PasswordResetDialog from './PasswordResetDialog';
import { formatPhoneNumber } from '../../utils/Util';
import APICall from 'app/api/APICall';

const useStyles = makeStyles(theme => ({
    root: {},
    board: {
        cursor: 'pointer',
        transitionProperty: 'box-shadow border-color',
        transitionDuration: theme.transitions.duration.short,
        transitionTimingFunction: theme.transitions.easing.easeInOut
    },
    newBoard: {},
}));

const schema = yup.object().shape({
    nameSurname: yup.string().required('Boş bırakılamaz.'),
    email: yup.string().email('Emaili doğru formatta giriniz.').required('Boş bırakılamaz.')
});

export default function ProfileContent(props) {
    const classes = useStyles(props);
    const { user } = useAuth();
    const history = useHistory();
    const { showSuccessDialog, showFailureDialog } = useDialog();
    const { windowWidth } = useWindow();
    const [disabledButton, setDisabledButton] = useState(false);
    const [phoneNumber, setPhoneNumber] = useState("");

    const defaultValues = {
        nameSurname: user.nameSurname,
        email: user.email
    }

    const { control, formState, handleSubmit, reset } = useForm({
        mode: 'onChange',
        defaultValues,
        resolver: yupResolver(schema)
    });

    const { isValid, dirtyFields, errors } = formState;

    const editProfile = async (data) => {
        try {
            if (phoneNumber) {
                data.phoneNumber = phoneNumber;
                console.log(data);
            }
            else {
                showFailureDialog('Lütfen bir telefon numarası giriniz.');
            }
        }
        catch (err) {
            //Handle error
        }
    }

    const handlePhoneChange = ({ target: { value } }) => {
        setPhoneNumber(prevNumber => formatPhoneNumber(value, prevNumber));
    }

    const handlePasswordChange = async () => {
        try {
            APICall.ForgotPassword({ email: user?.email }).then(() => { showSuccessDialog("Şifre değişikliği bağlantısı mailinize iletildi.") }).catch(err => { showFailureDialog("Şifre değişikliği talebide hata oluştu. Tekrar deneyiniz.") });
        }
        catch (err) {
            showFailureDialog("Beklenmedik Hata");
        }
    }

    useEffect(() => {
        setPhoneNumber(formatPhoneNumber(user?.phoneNumber));
    }, [user])

    return (
        <div className={clsx(classes.root, 'flex flex-grow flex-shrink-0 flex-col items-center p-16 md:p-24')}>
            <div style={{ width: windowWidth > 550 ? 500 : '100%' }}>
                <form
                    name="profileForm"
                    noValidate
                    className="flex flex-col justify-center w-full"
                    onSubmit={handleSubmit(editProfile)}
                >
                    <Controller
                        name="nameSurname"
                        control={control}
                        render={({ field }) => (
                            <TextField
                                {...field}
                                className="mb-16"
                                style={{ marginBottom: 16 }}
                                label="İsim Soyisim"
                                type="text"
                                variant="outlined"
                                error={!!errors.nameSurname}
                                helperText={errors?.nameSurname?.message}
                                required
                                fullWidth
                                disabled
                            />
                        )}
                    />

                    <Controller
                        name="email"
                        control={control}
                        render={({ field }) => (
                            <TextField
                                {...field}
                                className="mb-16"
                                style={{ marginBottom: 16 }}
                                label="Email"
                                type="text"
                                variant="outlined"
                                error={!!errors.email}
                                helperText={errors?.email?.message}
                                required
                                fullWidth
                                disabled
                            />
                        )}
                    />

                    <TextField
                        className="mb-16"
                        style={{ marginBottom: 16 }}
                        label="Telefon Numarası"
                        type="phone"
                        placeholder="(xxx) xxx-xxxx"
                        variant="outlined"
                        value={phoneNumber}
                        onChange={handlePhoneChange}
                        required
                        fullWidth
                        disabled
                    />

                    <TextField
                        className="mb-16"
                        value={user.role}
                        style={{ marginBottom: 16 }}
                        label="Rol"
                        type="text"
                        variant="outlined"
                        disabled
                        fullWidth
                    />

                    <div className="flex flex-col sm:flex-row items-center justify-center sm:justify-between" />

                    <Button
                        variant="contained"
                        style={{ backgroundColor: '#a0a0a0', color: 'white' }}
                        className="w-full mx-auto mt-16"
                        aria-label="Change Password"
                        onClick={handlePasswordChange}
                    >
                        Şifre Değişikliği Talep Et
                    </Button>
                </form>
            </div>
        </div>
    )
}

/*

                    <Button
                        variant="contained"
                        color="primary"
                        disabled={!isValid || disabledButton}
                        className="w-full mx-auto mt-16"
                        aria-label="EDIT PROFILE"
                        type="submit"
                    >
                        {disabledButton ? <div><CircularProgress size={20} /> Güncelleniyor</div> : "Güncelle"}
                    </Button>
 */