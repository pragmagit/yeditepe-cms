import FuseLoading from '@fuse/core/FuseLoading';
import FusePageCarded from '@fuse/core/FusePageCarded';
import { useAuth } from 'app/main/contexts/AuthContext';
import React from 'react';
import { Redirect } from 'react-router';
import ProfileContent from './ProfileContent';
import ProfileHeader from './ProfileHeader';

export default function ProfileMain() {
    const { isLoggedIn, remindProcess } = useAuth();

    if (remindProcess) {
        return <FuseLoading />
    }

    if (!isLoggedIn) {
        return <Redirect to="/" />
    }

    return (
        <div className="flex-grow overflow-x-auto">
            <FusePageCarded
                classes={{
                    content: 'flex',
                    contentCard: 'overflow-hidden',
                    header: 'min-h-72 h-72 sm:h-136 sm:min-h-136'
                }}
                header={<ProfileHeader title="Profili Düzenle" />}
                content={<ProfileContent />}
                innerScroll
            />
        </div>
    )
}
