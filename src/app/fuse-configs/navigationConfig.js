import i18next from 'i18next';
import ar from './navigation-i18n/ar';
import en from './navigation-i18n/en';
import tr from './navigation-i18n/tr';

i18next.addResourceBundle('en', 'navigation', en);
i18next.addResourceBundle('tr', 'navigation', tr);
i18next.addResourceBundle('ar', 'navigation', ar);

const navigationConfig = [
	{
		id: 'applications',
		title: 'İşlemler',
		translate: 'İşlemler',
		type: 'group',
		icon: 'apps',
		children: [
			{
				id: 'dashboard',
				title: 'Ana Sayfa',
				type: 'item',
				icon: 'dashboard',
				url: '/dashboard'
			},
			{
				id: 'airwaybills',
				title: 'Sevkiyatlar',
				type: 'collapse',
				icon: 'local_airport',
				children: [
					{
						id: 'awaitingBills',
						title: 'Bekleyen Sevkiyatlar',
						type: 'item',
						url: '/pages/airwaybills/awaiting',
						exact: true
					},
					{
						id: 'awaitingApprovalBills',
						title: 'Onay Bekleyen Sevkiyatlar',
						type: 'item',
						url: '/pages/airwaybills/awaiting-approval',
						exact: true
					},
					{
						id: 'completedBills',
						title: 'Tamamlanan Sevkiyatlar',
						type: 'item',
						url: '/pages/airwaybills/completed',
						exact: true
					}
				]
			},
			{
				id: 'vehicles',
				title: 'Araçlar',
				type: 'collapse',
				icon: 'airport_shuttle',
				children: [
					{
						id: 'awatingVehicles',
						title: 'Onay Bekleyen Araçlar',
						type: 'item',
						url: '/pages/incoming-vehicles/awaitingApproval',
						exact: true
					},
					{
						id: 'completedVehicles',
						title: 'Tamamlanan Araçlar',
						type: 'item',
						url: '/pages/incoming-vehicles/completed',
						exact: true
					}
				]
			},
			{
				id: 'questions',
				title: 'Sorular',
				type: 'collapse',
				icon: 'view_list',
				children: [
					{
						id: 'questionBank',
						title: 'Soru Havuzu',
						type: 'item',
						url: '/pages/questions',
						exact: true
					},
					{
						id: 'questionTypes',
						title: 'Soru Eşleştirme',
						type: 'item',
						url: '/pages/shipment-types',
						exact: true
					}
				]
			},
			{
				id: 'staff',
				title: 'Personeller',
				type: 'item',
				icon: 'person',
				url: '/pages/staff'
			},
			{
				id: 'logs',
				title: 'Loglar',
				type: 'item',
				icon: 'info',
				url: '/pages/logs'
			},
			{
				id: 'logout',
				title: 'Çıkış Yap',
				type: 'item',
				icon: 'exit_to_app',
				url: '/logout'
			}
		]
	}
];

export default navigationConfig;
