import FuseUtils from '@fuse/utils';
import LoginConfig from 'app/main/login/LoginConfig';
import LogoutConfig from 'app/main/logout/LogoutConfig';
import pagesConfigs from 'app/main/pages/pagesConfig';
import ProfileConfig from 'app/main/profile/ProfileConfig';
import ResetPasswordConfig from 'app/main/reset-password/ResetPasswordConfig';
import { Redirect } from 'react-router-dom';

const routeConfigs = [
	...pagesConfigs,
	LoginConfig,
	LogoutConfig,
	ProfileConfig,
	ResetPasswordConfig
];

const routes = [
	// if you want to make whole app auth protected by default change defaultAuth for example:
	// ...FuseUtils.generateRoutesFromConfigs(routeConfigs, ['admin','staff','user']),
	// The individual route configs which has auth option won't be overridden.
	...FuseUtils.generateRoutesFromConfigs(routeConfigs),
	{
		path: '/',
		component: () => <Redirect to="/login" />
	},
	{
		component: () => <Redirect to="/" />
	}
];

export default routes;
