module.exports.AirwaybillStatus = {
    1: "AWAITING",
    2: "Under Control",
    3: "Awating Approval",
    4: "Completed",
    AWAITING: 1,
    UNDER_CONTROL: 2,
    AWAITING_APPROVAL: 3,
    COMPLETED: 4,
    "awaiting": 1,
    "under-control": 2,
    "awaiting-approval": 3,
    "completed": 4
}

module.exports.Datatype = {
    1: "String",
    2: "Number",
    3: "Boolean",
    4: "DateTime",
    5: "Multiple Choice",
    6: "Single Choice",
    7: "HES Code",
    8: "Date",
    9: "Decimal",
    10: "Airwaybill No",
    11: "Seal No",
    12: "Notes",
    13: "Photo",
    14: "Destination",
    15: "Piece"

}

module.exports.DatatypeReverse = {
    "String": 1,
    "Number": 2,
    "Boolean": 3,
    "DateTime": 4,
    "Multiple Choice": 5,
    "Single Choice": 6,
    "HES Code": 7,
    "Date": 8,
    "Decimal": 9,
    "Airwaybill No": 10,
    "Seal No": 11,
    "Notes": 12,
    "Photo": 13,
    "Destination": 14,
    "Piece": 15
}

module.exports.DatatypeTR = {
    1: "Yazı",
    2: "Sayı",
    3: "Evet/Hayır",
    4: "Tarih/Saat",
    5: "Çoktan Seçmeli",
    6: "Tek Seçimli",
    7: "HES Kodu",
    8: "Tarih",
    9: "Ondalık",
    10: "Konşimento Numarası",
    11: "Mühür Numarası",
    12: "Notlar",
    13: "Fotoğraf",
    14: "Varış Yeri",
    15: "Kap Adedi"
}

module.exports.DatatypeTRReverse = {
    "Yazı": 1,
    "Sayı": 2,
    "Evet/Hayır": 3,
    "Tarih/Saat": 4,
    "Çoktan Seçmeli": 5,
    "Tek Seçimli": 6,
    "HES Kodu": 7,
    "Tarih": 8,
    "Ondalık": 9,
    "Konşimento Numarası": 10,
    "Mühür Numarası": 11,
    "Notlar": 12,
    "Fotoğraf": 13,
    "Varış Yeri": 14,
    "Kap Adedi": 15
}

module.exports.IncomingVehicleStatus = {
    1: "Awaiting",
    2: "AwaitingApproval",
    3: "VehicleDeniedByHeadOffice",
    4: "VehicleDeniedByManifacturer",
    5: "Completed"
}

module.exports.IncomingVehicleStatusReverse = {
    "Awaiting": 1,
    "AwaitingApproval": 2,
    "VehicleDeniedByHeadOffice": 3,
    "VehicleDeniedByManifacturer": 4,
    "Completed": 5,
    "awaiting": 1,
    "awaitingApproval": 2,
    "vehicleDeniedByHeadOffice": 3,
    "vehicleDeniedByManifacturer": 4,
    "completed": 5
}

module.exports.IncomingVehicleStatusTR = {
    1: "Bekleyen",
    2: "Onay Bekleyen",
    3: "Ofis Tarafından Reddedildi",
    4: "Araç Üretici Tarafından Reddedildi",
    5: "Tamamlandı"
}

module.exports.WeightUnit = {
    1: "Kilogram"
}

module.exports.AirwaybillType = {
    1: "AirCargo"
}

module.exports.CargoCompany = {
    21: "UnknownCargoCompany"
}

module.exports.PhotoRequirementType = {
    1: "Cevap evet ise fotoğraf gerekli",
    2: "Cevap hayır ise fotoğraf gerekli",
    3: "Her cevapta fotoğraf çekilmeli",
    4: "Her cevapta fotoğraf çekilmemeli",
    5: "Cevap evet ise fotoğraf gerekli ama zorunlu değil",
    6: "Cevap hayır ise fotoğraf gerekli ama zorunlu değil",
    7: "Her cevapta fotoğraf gerekli ama zorunlu değil"
}

module.exports.PhotoRequirementTypeReverse = {
    "Cevap evet ise fotoğraf gerekli": 1,
    "Cevap hayır ise fotoğraf gerekli": 2,
    "Her cevapta fotoğraf çekilmeli": 3,
    "Her cevapta fotoğraf çekilmemeli": 4,
    "Cevap evet ise fotoğraf gerekli ama zorunlu değil": 5,
    "Cevap hayır ise fotoğraf gerekli ama zorunlu değil": 6,
    "Her cevapta fotoğraf gerekli ama zorunlu değil": 7
}

module.exports.PhotoRequirementTypeForOthers = {
    3: "Her cevapta fotoğraf çekilmeli",
    4: "Her cevapta fotoğraf çekilmemeli",
    7: "Her cevapta fotoğraf gerekli ama zorunlu değil"
}

module.exports.ConfirmationType = {
    1: "Cevap evet ise departman onayı istenecek",
    2: "Cevap hayır ise departman onayı istenecek",
    3: "Cevap belirtilen aralık dışındaysa merkez onayı istenecek",
    4: "Onay istenmiyor"
}

module.exports.ConfirmationTypeReverse = {
    "Cevap evet ise departman onayı istenecek": 1,
    "Cevap hayır ise departman onayı istenecek": 2,
    "Cevap belirtilen aralık dışındaysa merkez onayı istenecek": 3,
    "Onay istenmiyor": 4
}

module.exports.ConfirmationTypeForBoolean = {
    1: "Cevap evet ise departman onayı istenecek",
    2: "Cevap hayır ise departman onayı istenecek",
    4: "Onay istenmiyor"
}

module.exports.ConfirmationTypeForNumber = {
    3: "Cevap belirtilen aralık dışındaysa merkez onayı istenecek",
    4: "Onay istenmiyor"
}


module.exports.QuestionType = {
    0: "Ürün Kabul",
    1: "Araç Kabul"
}

module.exports.QuestionTypeReverse = {
    "Ürün Kabul": 0,
    "Araç Kabul": 1
}

module.exports.ShipmentType = {
    1: 'SWP',
    2: 'GC',
    3: 'FIS',
    4: 'PES',
    5: 'AVI',
    6: 'HR',
    7: 'LIV',
    8: 'DG',
    9: 'PER',
    10: 'FUAR',
    11: 'CHT',
    12: 'HDL',
    13: 'KAR'
}

module.exports.ShipmentTypeReverse = {
    'SWP': 1,
    'GC': 2,
    'FIS': 3,
    'PES': 4,
    'AVI': 5,
    'HR': 6,
    'LIV': 7,
    'DG': 8,
    'PER': 9,
    'FUAR': 10,
    'CHT': 11,
    'HDL': 12,
    'KAR': 13
}